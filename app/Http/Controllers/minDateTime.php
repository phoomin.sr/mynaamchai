<?php namespace App\Http\Controllers;

use Doctrine\Inflector\Rules\Pattern;

class minDateTime extends Controller {
    public $prefix;
    public $year;
    public $month;
    public $day;
    public const OR_RUN_NO = [
        'year', 'month', 'day'
    ];
    public const PTTRN_RUN_NO = [
        '%Y' => 'Y',
        '%y' => 'y',
        '%m' => 'm',
        '%d' => 'd'
    ];

    public function get_runno($runno)
    {
        for ($i = 0;$i < count($runno);$i++) {
            $key = self::OR_RUN_NO[$i];
            $this->$key = self::PTTRN_RUN_NO[$runno[$i]];
        }

        return $this;
    }
    public function genDateCode($digit = 0)
    {
        $pattrn = array();
        $pttDate = array();

        if ($this->prefix !== null) {
            array_push($pattrn, $this->prefix);
        }
        if ($this->year !== null) {
            array_push($pttDate, $this->year);
        }
        if ($this->month !== null) {
            array_push($pttDate, $this->month);
        }
        if ($this->day !== null) {
            array_push($pttDate, $this->day);
        }
        array_push($pattrn, implode('/', $pttDate));
        if ($digit !== 0) {
            array_push($pattrn, $digit);
        }

        return implode(',', $pattrn);
    }
}
