<?php namespace App\Http\Controllers;

use App\Models\option;
use App\Models\option_group_detail;
use App\Models\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    function product(Request $request)
    {
        $company_id = $request->session()->get('company');
        $select = <<<EOT
select p.*, c.name from product AS p
JOIN product_category AS c ON c.id = p.product_cat
where status <> 2 and company_id = $company_id
EOT;
        $res = DB::select($select);

        return view('viewAllProduct', [
            'title'=>'สินค้าคงเหลือ',
            'back' => 'drawer',
            'product'=>$res,
        ]);
    }
    function unitProduct(Request $request, $id)
    {
        $product = new product;
        $category = new StockController;
        $allcategory = $category->get_category_toList($request->session()->get('company'));
        $option = new OptionController();
        $alloption = $option->get_options($request->session()->get('company'));

        $info = $product->where('id', '=', $id)->get();

        return view('form.formUnitProduct', ['title'=>'สินค้าคงเหลือ', 'back' => 'product', 'option'=>$alloption, 'cat' => $allcategory, 'action'=>route('saveProduct'), 'info' => $info]);
    }
    function FormaddProduct(Request $request)
    {
        $category = new StockController;
        $allcategory = $category->get_category_toList($request->session()->get('company'));
        $option = new OptionController();
        $alloption = $option->get_options($request->session()->get('company'));
        return view('form.formProduct', ['title'=>'สินค้าคงเหลือ', 'back' => 'product', 'option'=>$alloption, 'cat' => $allcategory, 'action'=>route('saveProduct')]);
    }
    function saveProduct (Request $request)
    {
        $unit = array();
        $post = $request->all();
        $option = new option;
        $opt_id = $option->where('name', '=', 'หน่วย')->get();
        $optdt = new option_group_detail;
        $absname = $optdt->where('option_id', '=', $opt_id[0]->id)->get()->toArray();
        $company_id = $request->session()->get('company');

        foreach ($absname as $key => $value) {
            $unit[$value['name_option_dt']] = $value['abs_option_dt'];
        }

        for ($i = 0;$i < count($post['skus']);$i++) {
            $product = new product;
            $arrName = explode('-', $post['skus'][$i]);
            foreach ($unit as $key => $value) {
                if (in_array($value, $arrName)) {
                    $nn = $value;
                } else {
                    $nn = "";
                }
            }
            if (!($product->where('product_code', '=', $post['skus'][$i])->where('company_id', '=', $company_id)->get()->count() > 0)) {
                $product->product_cat = $post['product_type'];
                $product->product_code = $post['skus'][$i];
                $product->product_name = $post['product_name'][$i];
                $product->product_generation = $post['gen'];
                $product->product_unit = $nn;
                $product->product_location = $post['location'];
                $product->product_minstock = $post['min_stock'];
                $product->product_sale_price = $post['product_sale_price'][$i];
                $product->company_id = $company_id;
                $product->save();
            }
        }

        return response(['save' => true, 'msg' => 'บันทึกรายการ ' . $post['name'] . ' และ sku ทั้งหมดสำเร็จ', 'redirect_to' => route('product')]);
    }
    function generateSKUs($prefix, $option)
    {
        $array_sku = array();
        for ($i = 0;$i < count($option);$i++) {
            if (empty($array_sku)) {
                for ($ii = 0;$ii < count($option[$i]);$ii++) {
                    $start = $option[$i];
                    array_push($array_sku, $prefix . '-' . $start[$ii]);
                }
            } else {
                $buffer = array();
                for ($ii = 0;$ii < count($array_sku);$ii++) {
                    for ($x = 0;$x < count($option[$i]);$x++) {
                        $key = $array_sku[$ii] . '-' . $option[$i][$x];
                        $buffer[] = $key;
                    }
                }
                $array_sku = $buffer;
            }
        }
        return $array_sku;
    }
    function updateProduct(Request $request)
    {
        $post = $request->all();

        $updateInfo = [
            'product_cat' => $post['category_group'],
        ];

        if ($post['product_code'] !== "") {
            $updateInfo['product_code'] = $post['product_code'];
        }
        if ($post['product_name'] !== "") {
            $updateInfo['product_name'] = $post['product_name'];
        }
        if ($post['location'] !== "") {
            $updateInfo['product_location'] = $post['location'];
        }
        if ($post['product_minstock'] !== "") {
            $updateInfo['product_minstock'] = $post['product_minstock'];
        }

        DB::table('product')->where('id', '=', $post['id'])->update($updateInfo);

        return response(['save' => true, 'msg' => 'บันทึกการแก้ไข ' . $post['product_name'] . ' ทั้งหมดสำเร็จ', 'redirect_to' => route('product')]);
    }
    function deleteProduct(Request $request, $id)
    {
        DB::table('product')->where('id', '=', $id)->update(['status'=>2]);

        return redirect(route('product'));
    }
    function getExtra(Request $request)
    {
        $post = $request->all();
        $res = DB::table('product', 'pd')
        ->join('option_group_detail', 'option_group_detail.abs_option_dt', '=', 'pd.product_unit')
        ->where('pd.id', '=', $post['id'])
        ->get();

        return response(["response"=>true, 'extra' => $res]);
    }
}
