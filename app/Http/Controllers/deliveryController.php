<?php namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class deliveryController extends Controller {
    public function view(Request $request)
    {
        $company_id = $request->session()->get('company');

        $var = [
            'title' => 'ใบส่งของ',
            'back'=>'drawer',
            'add' => route('deliveryadd'),
        ];

        return view('deliveryview', $var);
    }
    public function add(Request $request)
    {
        $company_id = $request->session()->get('company');
        $var = [
            'title' => 'ข้อมูลใบส่งของ',
            'back'=>'deliveryview',
        ];

        return view('form.deliveryform', $var);
    }
}
