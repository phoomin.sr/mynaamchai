<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ConditionControl extends Controller
{
    function checkDuplicate(Request $request)
    {
        $post = $request->all();
        if (!isset($post['scope'])) {
            $res = DB::table($post['table'])->where($post['column'], '=', $post['value'])->get();
        }else if (!isset($post['table2'])) {
            $res = DB::table($post['table'])
            ->where($post['column'], '=', $post['value'])
            ->where('company_id', '=', $request->session()->get('company'))
            ->get();
        } else if (isset($post['table2'])) {
            $res = DB::table($post['table'])
            ->join($post['table2'], $post['table'].'.'.$post['first'], '=', $post['table2'].'.'.$post['second'])
            ->where($post['column'], '=', $post['value'])
            ->where('company_id', '=', $request->session()->get('company'))
            ->get();
        } else {
            
        }

        if (count($res) > 0) {
            return response([true]);
        } else {
            return response([false]);
        }
    }
}
