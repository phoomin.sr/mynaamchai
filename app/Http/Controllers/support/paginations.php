<?php namespace App\Http\Controllers\support;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class paginations{
    public $uri;
    public $page = 1;
    public $offset = 0;
    protected $maxrow = 10;
    public $row = 0;
    public $maxpage = 0;
    public $forward= 0;
    public $backward = 0;
    public $first = 1;
    public $last = 0;

    static function getInstants()
    {
        return new paginations;
    }
    static function setMax($max)
    {
        return paginations::getInstants()->maxrow = $max;
    }
    static function setRow($row)
    {
        return paginations::getInstants()->set_maxrow($row);
    }
    function set_maxrow($row)
    {
        if (is_array($row)) {
            $this->row = count($row);
        } else if ($row instanceof DB) {
            $this->row = $row;
        } else {
            $this->row = $row;
        }

        $this->maxpage()
        ->offset()
        ->prevpage()
        ->nextpage();

        return $this;
    }
    function page($page = 1)
    {
        $this->page = $page;

        $this->maxpage()
        ->offset()
        ->prevpage()
        ->nextpage();

        return $this;
    }
    function maxpage()
    {
        $this->last = $this->maxpage = ceil($this->row/$this->maxrow);
        return $this;
    }
    function offset()
    {
        $this->offset = $this->maxrow * ($this->page - 1);
        return $this;
    }
    function prevpage()
    {
        $this->forward = $this->page + 1 >= $this->maxpage ? $this->page:$this->page + 1;
        return $this;
    }
    function nextpage()
    {
        $this->backward = $this->page - 1 == 0 ? 1:$this->page - 1;
        return $this;
    }
    function maxrow()
    {
        return $this->maxrow;
    }
    function pagination_skip_row($length = 3)
    {
        $menu = [];

        for ($i = 1;$i <= $this->maxpage;$i++) {
            if ($i <= $this->page - $length) {
                $menu['morethan'] = '...';
            } else if ($i >= $this->page + $length) {
                $menu['lessthan'] = '...';
            } else {
                $menu['menulink'][] = $this->get_pageLink($i);
            }
        }
        $this->menu = $menu;
        return $this;
    }
    function get_pageLink($page)
    {
        $link = URL::current() . "?";
        $_GET['page'] = $page;

        $last = count($_GET) - 1;
        $i = 0;
        foreach ($_GET as $name => $value) {
            if ($last == $i) {
                $link .= "$name=$value";
            } else {
                $link .= "$name=$value&";
            }
            $i++;
        }
        return $link;
    }
}
