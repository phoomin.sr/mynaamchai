<?php namespace App\Http\Controllers\support;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;

class docno {
    static function getInstants()
    {
        return new docno;
    }
    function table($table){
        $this->table = $table;
        return $this;
    }
    function no($no){
        $this->no = $no;
        return $this;
    }
    function Generate_no($type, Request $request)
    {
        $this->company = $request->session()->get('company');
        $pattrn = $this->get_docpattrn($type);
        $part = explode(',', $pattrn[0]->pattrn);
        $part[1] = implode('', $this->getDate($part[1]));
        $this->pattrn = implode(',', $part);
        return $this->genKey($this->pattrn);
    }
    function Generate_no_bymanual($pattern)
    {
        return $this->genKey($pattern);
    }
    function get_docpattrn($type)
    {
        return DB::table('docno')->where('docname', '=', $type)->where('company_id', '=', $this->company)->get();
    }
    function getDate($datepattn, $delimeter = "/")
    {
        return explode($delimeter, $datepattn);
    }
    function genKey($pattrn)
    {
        $expl = explode(',', $pattrn);

        $prefix = $expl[0];
        $date = $this->getDate_byPattrn($expl[1]);
        $digits = $expl[2];

        $result = DB::table($this->table)->where($this->no, 'like', $prefix . $date ."%")->orderByDesc($this->no)->limit(1)->get()->toArray();

        if (empty($result)) {
            $no = str_pad(1, $digits, '0', STR_PAD_LEFT);
        } else {
            $number = $this->no;
            $no = (int)str_replace($prefix . $date, '', $result[0]->$number);

            $no++;
            $no = str_pad($no, $digits, '0', STR_PAD_LEFT);
        }
        return $prefix . $date . $no;
    }
    function getDate_byPattrn($sqlPattrn = "ym", $tothai = true)
    {
        $datetime = new DateTime('Asia/Bangkok');
        $date = "";

        if ($tothai) {
            $datetime->modify('+543 years');
        }

        $date = $datetime->format(str_replace("%", "", $sqlPattrn));
        return $date;
    }
}
