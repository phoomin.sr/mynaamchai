<?php namespace App\Http\Controllers\support;

use DateTime;

class datetimes {
    public const DAY_TH = [
        "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์", "อาทิตย์"
    ];
    public const MONTH_TH = [
        "01" => "มกราคม", "02" => "กุมภาพันธ์", "03"=>"มีนาคม", "04"=>"เมษายน", "05" => "พฤษภาคม", "06" => "มิถุนายน",
        "07"=>"กรกฎาคม", "08"=>"สิงหาคม", "09"=>"กันยายน", "10"=>"ตุลาคม", "11"=>"พฤษจิกายน", "12"=>"ธันวาคม"
    ];
    public const BCYEAR = 543;

    public static function get_month_no($month)
    {
        $months = array_flip(self::MONTH_TH);
        return $months[$month];
    }
    public static function formats($strdate, $format = "Y-m-d H:i:s")
    {
        $date = date($format, strtotime($strdate));
        return $date;
    }
}
