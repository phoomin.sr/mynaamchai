<?php namespace App\Http\Controllers;

use App\Models\category_tax;
use App\Models\docno;
use App\Models\product;
use App\Models\product_category;
use App\Models\stock_name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use App\Http\Controllers\support\datetimes;
use App\Models\add_dt_stock;
use App\Models\add_stock;
use App\Models\inv;
use App\Models\option;
use App\Models\stock;
use App\Http\Controllers\support\docno AS SupportDocNo;
use DateTime;

class StockController extends Controller
{
    public const COST_METHOD = [
        1 => 'เข้าก่อน ออกก่อน',
        2 => 'ถัวเฉลี่ย'
    ];
    public const RECIEVED_TYPE = [
        'manual' => 'รับเข้า(manual)',
        'begining_balance' => 'ยอดยกมา',
        'adjustStock' => 'ปรับสต๊อก',
    ];
    public const TRANS_RECIEVED_TYPE = [
        'manual' => '0',
        'begining_balance' => "1" ,
        'adjustStock' => "2"
    ];
    public const RUNNO_OPTION = [
        '%Y' => 'ปีเต็ม(4 หลัก)',
        '%y' => 'ปีย่อ (2 หลัก)',
        '%m' => 'เดือนย่อ (2 หลัก)',
        '%d' => 'วันที่',
    ];
    public const PRODUCT_TYPE = [
        'onsite' => '01',
        'outsource' => '02'
    ];
    public const WITHDRAW_TYPE = [
        '1' => 'เข้าก่อน-ออกก่อน',
        '2' => 'ถัวเฉลี่ย',
        '3' => 'เฉพาะเจาะจง'
    ];
    public const WITHDRAW_FOR = [
        'Sale' => 'เบิกส่งขาย',
        'Manualfac' => 'เบิกไปผลิต',
        'Stock' => 'ปรับสต๊อก'
    ];
    function incomemanual(Request $request)
    {
        $unit = OptionController::get_unit($request->session()->get('company'));
        $rec = array();

        $rec = DB::table('add_stock')
        ->select(['add_stock.*', 'product.product_name', 'add_dt_stock.unit_qty'])
        ->join('add_dt_stock', 'add_dt_stock.addId', '=', 'add_stock.id')
        ->join('inv', 'inv.id', '=', 'add_dt_stock.inv_id')
        ->join('product', 'product.id', '=', 'inv.ref_id')
        ->where('add_stock.company_id', '=', $request->session()->get('company'))
        ->orderByDesc('add_stock.no')
        ->get();

        return view('incomemanual', [
            'title' => 'รับสินค้า(Manual)',
            'back'=>'drawer',
            'add' => route('AddIncomeStockmn'),
            'recs' => $rec,
            'globalUnit' => $unit
        ]);
    }
    function addincomemanual(Request $request)
    {
        $stock_name = new stock_name();
        $stkname = $stock_name->where('company_id', '=', $request->session()->get('company'))->get();
        $unit = OptionController::get_unit($request->session()->get('company'));

        return view('form.incomemanual', [
            'title' => 'รับสินค้า(Manual)',
            'back'=>'IncomeStockmn',
            'recieved_type'=>self::RECIEVED_TYPE,
            'action' => route('saveincomemanual'),
            'count' => 5,
            'findProduct' => route('searchProduct'),
            'getExtra_product' => route('getExtra_product'),
            'globalUnit' => $unit,
            'stk' => $stkname
        ]);
    }
    function searchProduct (Request $request)
    {
        $f = $request->get('f');
        $product = new product();
        $res = $product->where(DB::raw('CONCAT(product_code, ":", product_name)'), 'like', '%'.$f.'%')
        ->where('status', '<>', '2')
        ->where('company_id', '=', $request->session()->get('company'))
        ->get();

        $result = [];
        foreach ($res AS $key => $item) {
            $result[] = [
                'value' => $item->id,
                'text' => $item->product_name
            ];
        }
        return response($result);
    }
    function saveincomemanual(Request $request)
    {
        $add = new add_stock();
        $post = $request->all();

        $pattrnno = SupportDocNo::getInstants()->table('add_stock')->no('no')->Generate_no('stock_trade', $request);
        $received_type = self::TRANS_RECIEVED_TYPE[$post['recieved_type']];
        $datetime = new DateTime(str_replace('/', '-', $post['recieved_date']));
        $received_date = $datetime->format('Y-m-d');

        $add->type = $received_type;
        $add->no = $pattrnno;
        $add->docdate = $received_date;
        $add->user_id = $request->session()->get('authId');
        $add->company_id = $request->session()->get('company');
        $add->save();

        for ($i = 0;$i < count($post['product_id']);$i++) {
            if ($post['product_id'][$i] !== NULL) {
                $inv_id = $this->checkinvId($post['product_id'][$i], true);
                $add_dtId = DB::table('add_dt_stock')->insertGetId([
                    'addid' => $add->id,
                    'inv_id' => $inv_id,
                    'unit_qty' => $post['quantity'][$i],
                    'unit_price' => $post['unit_price'][$i]
                ]);

                DB::table('stock')->insert([
                    'addId' => $add_dtId,
                    'invId' => $inv_id,
                    'qty' => $post['quantity'][$i],
                    'unitprice' => $post['unit_price'][$i],
                    'lot' => "0",
                    'location' => $post['stock'][$i],
                    'withdraw_id' => NULL,
                    'company_id' => $request->session()->get('company')
                ]);
            }
        }

        return response(['add_begin'=>true,'msg'=>'เพิ่มยอดยกมาสำเร็จ', 'redirect_to' => route("IncomeStockmn")]);
    }
    function view(Request $request)
    {
        $company_id = $request->session()->get('company');
        $list = array();

        $list = DB::table('product')
        ->join('inv', 'inv.ref_id', '=', 'product.id', 'left')
        ->join('stock', 'stock.invId', '=', 'inv.id', 'left')
        ->join('option_group_detail', 'option_group_detail.abs_option_dt', 'product.product_unit', 'left')
        ->where('product.status', '=', '1')
        ->where('product.company_id', '=', $company_id)
        ->orderByDesc('stock.qty')
        ->get();

        return view('stockview', [
            'title' => 'คลังสินค้า',
            'back' => 'drawer',
            'list' => $list
        ]);
    }
    function checkinvId($product_id, $add = "false")
    {
        // $product_id *= 1;
        $inv = new inv();
        $product_count = $inv->where('ref_id', '=', $product_id)->count();

        if ($product_count > 0) {
            $product_count = $inv->where('ref_id', '=', $product_id)->get();
            return $product_count[0]->id;
        } else {
            if ($add) {
                $invid = DB::table('inv')
                ->insertGetId([
                    'type' => self::PRODUCT_TYPE['onsite'],
                    'ref_id' => $product_id
                ]);

                return $invid;
            } else {
                return false;
            }
        }
    }
    function settingStock(Request $request)
    {
        $stock_name = new stock_name();
        $docno = new docno();
        $doc = $docno->where('department', '=', 'stock')->where('company_id', '=', $request->session()->get('company'))->get();

        $stkname = $stock_name->where('company_id', '=', $request->session()->get('company'))->get();

        $doctype = [];
        foreach ($doc as $key => $value) {
            $pattrn = explode(',', $doc[$key]['pattrn']);
            $runno_opt = explode('-', $pattrn[1]);
            $doctype[$doc[$key]['docname']]['prefix'] = $pattrn[0];
            $doctype[$doc[$key]['docname']]['digit'] = $pattrn[2];
            $doctype[$doc[$key]['docname']]['runno'] = $runno_opt;
        }

        return view('settingStock', [
            'title' => 'ตั้งค่า',
            'back' => 'drawer',
            'action' => route('stocksavesetting'),
            'docname' => $doctype,
            'runno' => self::RUNNO_OPTION,
            'stock_name' => $stkname
        ]);
    }
    function savesetting(Request $request)
    {
        $post = $request->all();
        $trade_code = implode(',', [$post['prefix_trade'], implode('-',$post['runno_pattrn_trade']), $post['trade_no_number']]);
        $other_code = implode(',', [$post['prefix_other'], implode('-',$post['runno_pattrn_other']), $post['other_no_number']]);
        // dd($trade_code, $other_code);
        // update run code
        // stock trade
        $docno = new docno();
        $dinfo = $docno->where('docname', '=', 'stock_trade')->get();

        if (count($dinfo) > 0) {
            $docno = docno::find($dinfo[0]->id);
        }

        $docno->department = "stock";
        $docno->docname = "stock_trade";
        $docno->pattrn = $trade_code;
        $docno->company_id = $request->session()->get('company');

        $docno->save();

        // stock order
        $docno = new docno();
        $dinfo = $docno->where('docname', '=', 'stock_other')->get();

        if (count($dinfo) > 0) {
            $docno = docno::find($dinfo[0]->id);
        }

        $docno->department = "stock";
        $docno->docname = "stock_other";
        $docno->pattrn = $other_code;
        $docno->company_id = $request->session()->get('company');

        $docno->save();

        // add stock name
        $stock_count = count($post['stock_name']);
        $stock_name = new stock_name();
        // DB::table('stockname')->delete();
        for ($i = 0;$i < $stock_count;$i++) {
            if ($post['stock_name'][$i] !== null) {
                $stock_name->name = $post['stock_name'][$i];
                $stock_name->company_id = $request->session()->get('company');
                $stock_name->save();
            }
        }

        if ($stock_name->id||$docno->id) {
            return response(['update'=>true, 'msg'=>'ตั้งค่าสำเร็จ']);
        } else {
            return response(['update'=>false, 'msg'=>'ผิดพลาด! ไม่สามารถตั้งค่าได้']);
        }
    }
    function stockgroup(Request $request)
    {
        $cattax = new category_tax();
        $cat = $cattax
        ->where('company_id', '=', $request->session()->get('company'))
        ->orderBy('lineage', 'asc')
        ->get();

        $prodarr = [];
        for($i = 0;$i < count($cat);$i++){
            $product = new product_category();
            $prodcat = $product
            ->where('id', '=', $cat[$i]->category_id)
            ->get();

            $prodarr[$cat[$i]->category_id] = [
                'name' => str_repeat('-', $cat[$i]->deep).' '.$prodcat[0]->name,
                'slug' => $prodcat[0]->slug,
                'description' => $prodcat[0]->description
            ];
        }
        return view('categoryStock', [
            'title' => 'ประเภทสินค้าคงเหลือ',
            'back'=>'drawer',
            'category' => $prodarr
            ]);
    }
    function updateCatStock(Request $request, $id)
    {
        $tax = new category_tax();
        $product = new product_category();

        $nowcat = $tax->where('category_id', '=', $id)->get();
        $res = $product->where('id', '=', $id)->get();
        $res[0]['parent'] = $nowcat[0]->parent;
        $cat = $this->get_category_toList($request->session()->get('company'));

        return view('form.categoryStock', [
            'title' => 'ประเภทสินค้าคงเหลือ',
            'back'=>'group',
            'action' => route('updateCategoryStock'),
            'category' => $cat,
            'cost_method' => self::COST_METHOD,
            'info' => $res[0],
            'id' => $id
            ]);
    }
    function get_category_toList($company_id)
    {
        $tax = new category_tax();
        $product_cat = new product_category();
        $rtax = $tax
        ->where('company_id', '=', $company_id)
        ->orderBy('lineage', 'asc')
        ->get();

        $cat[0] = "ไม่มี";
        for ($i = 0;$i < count($rtax);$i++) {
            $res = $product_cat
            ->where('id', '=', $rtax[$i]->id)
            ->get();
            $cat[$res[0]->id] = str_repeat('-', $rtax[$i]->deep).' '.$res[0]->name;
        }

        return $cat;
    }
    function addCatstock(Request $request)
    {
        $cat = $this->get_category_toList($request->session()->get('company'));
        return view('form.categoryStock', [
            'title' => 'เพิ่มสินค้าคงเหลือ',
            'back'=>'group',
            'action' => route('postCatstock'),
            'category' => $cat,
            'cost_method' => self::COST_METHOD
            ]);
    }
    function saveCatstock(Request $request)
    {
        $post = $request->all();
        $category_tax = new category_tax();
        $product_cat = new product_category();

        $dup = $product_cat->where('name', '=', $post['category_name'])
        ->get();

        if (count($dup) > 0) {
            return response(['save'=>false, 'msg'=>'รหัสสินค้านี้มีแล้ว กรุณาเปลี่ยนแล้วลองใหม่อีกครั้ง']);
        }

        if (is_null($post['description'])) {
            $post['description'] = $post['category_name'];
        }
        if ($post['group_main'] == 0) {
            $parent = 0;
            $deep = 0;
            $lineage = "";
        } else {
            $find = $category_tax->where('id', '=', $post['group_main'])->get();
            $lineage = $find[0]->lineage."-";
            $parent = $post['group_main'];
            $deep = count(explode('-', $find[0]->lineage));
        }

        $productCatId = DB::table('product_category')
        ->insertGetId([
            'acc_method' => $post['cost_method'],
            'slug' => $post['slug'],
            'name' => $post['category_name'],
            'description' => $post['description']
        ]);

        DB::table('category_tax')
        ->insert([
            'category_id'=>$productCatId,
            'parent' => $parent,
            'deep' => $deep,
            'lineage' => $lineage.$productCatId,
            'company_id' => $request->session()->get('company')
        ]);

        return response(['save'=>true, 'msg'=>'เพิ่มกลุ่ม'.$post['category_name'].'สำเร็จ', 'redirect_to'=>route('group')]);
    }
    function updateCategoryStock (Request $request)
    {
        $post = $request->all();
        $category_tax = new category_tax();

        if (is_null($post['description'])) {
            $post['description'] = $post['category_name'];
        }
        if ($post['group_main'] == 0) {
            $parent = 0;
            $deep = 0;
            $lineage = "";
        } else {
            $find = $category_tax->where('id', '=', $post['group_main'])->get();
            $lineage = $find[0]->lineage."-";
            $parent = $post['group_main'];
            $deep = count(explode('-', $find[0]->lineage));
        }

        DB::table('product_category')
        ->where('id', '=', $post['id'])
        ->update([
            'acc_method' => $post['cost_method'],
            'slug' => $post['slug'],
            'name' => $post['category_name'],
            'description' => $post['description']
        ]);

        DB::table('category_tax')
        ->where('id', '=', $post['id'])
        ->update([
            'category_id'=>$post['id'],
            'parent' => $parent,
            'deep' => $deep,
            'lineage' => $lineage.$post['id'],
            'company_id' => $request->session()->get('company')
        ]);

        return response(['save'=>true, 'msg'=>'แก้ไขกลุ่ม'.$post['category_name'].'สำเร็จ', 'redirect_to'=>route('group')]);
    }
    function deleteCatStock($id)
    {
        $product = new product();
        $count = $product->where('product_cat', '=', $id)->get()->count();

        if ($count > 0) {
            return response(['delete'=>false, 'msg'=>'ไม่สามารถลบรายการได้ขณะที่ใช้งานกลุ่ม']);
        } else {
            $tax = new category_tax;
            $tax->where('category_id', '=', $id)->delete();
            $cat = new product_category;
            $cat->where('id', '=', $id)->delete();

            return response(['delete'=>true, 'msg'=>'ลบรายการสำเร็จ', 'redirect_to'=>route('group')]);
        }
    }
    function widthdraw_view(Request $request)
    {
        $infos = array();
        // $this->company_id = $request->session()->get('company');

        // $infos = DB::table('withdraws')
        // ->select(DB::raw("withdraws.doc_no, group_concat(withdraw_dt.product) AS programs, withdraws.type, withdraws.doc_date"))
        // ->join('withdraw_dt', 'withdraw_dt.withdraw_id', 'withdraws.id')
        // ->where('company_id', '=', $this->company_id)
        // ->groupBy('withdraw_dt.withdraw_id')
        // ->get();

        return view('withdraw', [
            'title' => 'ประเภทสินค้าคงเหลือ',
            'back'=>'drawer',
            'record' => $infos,
            'withdraw_for' => self::WITHDRAW_FOR
        ]);
    }
    function addWithdrawmn(Request $request, $id=null)
    {
        $infos = array();

        return view('form.withdraw', [
            'title' => 'เบิกสินค้า',
            'back' => 'widthdraw_manual_view',
            'infos' => $infos,
            'withdraw_type' => self::WITHDRAW_TYPE,
            'withdraw_for' => self::WITHDRAW_FOR
        ]);
    }
    function savewithdraw (Request $request) {
        $this->company_id = $request->session()->get('company');

        $post = $request->all();
        $now = new DateTime();
        $no = SupportDocNo::getInstants()->table('withdraws')->no('doc_no')->Generate_no_bymanual("WD,ym,4");

        // create withdraw head with table withdraws
        $withdraw_id = DB::table('withdraws')->insertGetId([
            'type' => $post['wd_for'],
            'doc_no' => $no,
            'doc_date' => $now->format("Y-m-d"),
            'ref_id' => $post['refId'],
            'company_id' => $this->company_id,
            'update_at' => $now->format("Y-m-d H:i:s"),
            'create_at' => $now->format("Y-m-d H:i:s")
        ]);

        for ($i = 0;$i < count($post['product']);$i++) {
            if ($post['product'][$i] !== null) {
                DB::table("withdraw_dt")->insert([
                    'product_id' => $post['product'][$i],
                    'product' => $post['product_search'][$i],
                    'unit_qty'=> $post['unit_qty'][$i],
                    'withdraw_id' => $withdraw_id
                ]);
            }
        }

        // if ($post['wd_type'] == 1) {
        //     $this->create_mat_list('FiFo')
        //     ->matching($post)
        //     ->withdraw();
        // }

        return response(['save' => true, 'msg' => 'ทำการบันทึกใบเบิกรหัส ' . $no . ' เรียบร้อยแล้ว', 'redirect_to' => route('widthdraw_manual_view')]);
    }
}
