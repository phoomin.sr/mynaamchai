<?php namespace App\Http\Controllers;

use App\Models\product;

class accountingController {
    function setting () 
    {
        $product = new product();
        $allProduct = $product->get();
        return view('accsetting', [
            'title' => 'ตั้งค่า',
            'back'=>'drawer',
            'stockAction' => route('startingStock'),
            'product' => $allProduct
        ]);
    }
}