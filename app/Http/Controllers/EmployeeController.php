<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\support\datetimes;
use App\Models\employee_category;
use App\Models\employees;
use App\Models\User;
use App\Models\user_has_roles;
use App\Models\usermeta;
use DateTime;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class EmployeeController extends Controller
{
    public const PERMISSION = [
        '755' => 'สิทธิ์ทั้งหมด',
        '754' => 'อนุมัติ',
        '600' => 'อ่าน เพิ่ม ลบ แก้ไข',
        '544' => 'อ่าน และแก้ไข',
        '500' => 'อ่านเท่านั้น',
        '000' => 'ไม่มีสิทธิ์เข้าถึง'
    ];

    function employee(Request $request)
    {
        $employee_category = new employee_category();
        $employee = new employees();
        $company = $request->session()->get('company');

        $allemp = $employee->get_all_employee($company);
        $position = $employee_category->get_all_position($company, true);

        return view('form.employees', ['title'=>'พนักงาน', 'back'=>'drawer','employee'=>$allemp, 'position'=>$position]);
    }
    function formemployee(Request $request)
    {
        $empcat = new employee_category();
        $company = $request->session()->get('company');
        $position = $empcat->get_all_position($company);

        return view('form.formEmployee', [
            'title'=>'พนักงาน',
            'route'=>'addemployee',
            'back'=>'employee',
            'position'=>$position,
            'action' => 'add',
            'findAddress' => route('searchAddress')
        ]);
    }
    function catEmployee (Request $request)
    {
        $emp = new employee_category;
        $company = $request->session()->get('company');
        $position = $emp->get_all_position($company);

        return view('form.categoryEmployee', ['title'=>'กลุ่มพนักงาน','back'=>'drawer','position'=>$position]);
    }
    function editEmployee(Request $request, $id)
    {
        $company = $request->session()->get('company');

        $emp = new employees();
        $dbuser = new User();
        $empcat = new employee_category();

        $position = $empcat->get_all_position($company, true);
        $employee = $emp->get_employee_byId($id, true);

        if (!is_null($employee['info']->user_id)) {
            $user = $dbuser->get_user_byId($employee['info']->user_id);
        }


        return view('form.formEmployee', [
            'title'=>'พนักงาน',
            'back'=>'employee',
            'route' => 'updateEmployee',
            'action' => 'update',
            'position'=>$position,
            'info' => $employee['info'],
            'user' => $user['uinfo'],
            'user_meta' => $employee['meta'],
            'findAddress' => route('searchAddress')
        ]);
    }
    function updateEnployee(Request $request)
    {
        DB::table('employees')
        ->where('id', '=', $request->get('employee_id'))
        ->update([
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname'),
            'position' => $request->get('position'),
        ]);
        $post = $request->all();
        $this->UpdateUserMeta($request->get('employee_id'), $post);

        return response(['emp'=>true, 'msg'=>'แก้ไขรายการสำเร็จ', 'redirect_to'=>route('employee')]);
    }
    function editcatEmployee($id)
    {
        $emp = new employee_category;
        $menu = MenuController::menu;
        $info = $emp->where('id', '=', $id)->get()[0];
        $infomenu = json_decode($info['permission'], true);
        // dd($info);

        return view('form.categoryFormEmployee', [
                'title' => 'แก้ไขกลุ่มพนักงาน',
                'back'=>'catEmployee',
                'info'=>$info,
                'infomenu' => $infomenu,
                'menu'=>$menu,
                'permission'=>self::PERMISSION,
                'id'=>$id,
                'route'=>route('updatecatEmployee')
            ]);
    }
    function updatecatEmployee(Request $request)
    {
        $post = $request->all();
        $n = count($post['route_name']);
        $menu = array();

        for ($i = 0;$i < $n;$i++) {
            $menu[$post['route_name'][$i]] = $post['permission'][$i];
        }

        $permission = json_encode($menu);
        $lastid = DB::table('employee_category')
        ->where('id', '=', $post['where_id'])
        ->update([
            'position_name' => $post['position_name'],
            'slug' => $post['position_group'],
            'permission' => $permission
        ]);

        if ($lastid > 0) {
            return response(['catemp' => true, 'msg' => 'Update '.$post['position_name'].' permission Complete.', 'redirect_to' => route('catEmployee')]);
        } else {
            return response(['catemp' => false, 'msg' => '']);
        }
    }
    function addCatEmployee ()
    {
        $menu = MenuController::menu;
        $var = [
            'title'=>'กลุ่มพนักงาน',
            'back'=>'catEmployee',
            'permission'=>self::PERMISSION,
            'menu'=>$menu,
            'route'=>route('saveEmployee')
        ];
        return view('form.categoryFormEmployee', $var);
    }
    function saveCatEmployee (Request $request)
    {
        $post = $request->all();
        $n = count($post['route_name']);
        $menu = array();

        for ($i = 0;$i < $n;$i++) {
            $menu[$post['route_name'][$i]] = $post['permission'][$i];
        }

        $permission = json_encode($menu);
        $lastid = DB::table('employee_category')->insertGetId([
            'position_name' => $post['position_name'],
            'slug' => $post['position_group'],
            'permission' => $permission,
            'company_id' => $request->session()->get('company')
        ]);

        if ($lastid > 0) {
            return response(['catemp' => true, 'msg' => 'Create new permission Complete.', 'redirect_to' => route('catEmployee')]);
        } else {
            return response(['catemp' => false, 'msg' => '']);
        }
    }
    function saveEmployee(Request $request)
    {
        $emp = new employees;
        $user = new User;

        if (isset($request->email)) {
            $user->name = $request->input('username');
            $user->email = $request->input('email');
            $user->password = md5($request->input('password').'_msc');
            $user->grand = userController::LEVEL_USER[$request->input('grand')];
            $user->company_id = $request->session()->get('company');

            $user->save();
        }


        $emp->firstname = $request->input('firstname');
        $emp->lastname = $request->input('lastname');
        $emp->position = $request->input('position');
        $emp->user_id = $user->id;
        $emp->company_id = $request->session()->get('company');
        $emp->save();

        $post = $request->all();
        $this->UpdateUserMeta($emp->id, $post);

        if ($emp->id > 0) {
            return response(['emp' => true, 'msg' => 'Create new user Complete.', 'redirect_to' => route('employee')]);
        } else {
            return response(['emp' => false, 'msg' => '']);
        }
    }
    function UpdateUserMeta($id, $post)
    {
        $phone = str_replace('-', '', $post['phone']);
        $date = new DateTime(str_replace("/", "-", $post['bithdate']));

        $meta = [
            "bithdate" => $date->format("Y-m-d"),
            "phone" => trim($phone),
            "salary" => $post['salary'],
            "houseNo" => $post['houseNo'],
            "address" =>  $post['address']
        ];

        DB::table('employee_meta')->where('employee_id', '=', $id)->delete();

        foreach ($meta as $meta_key => $meta_value) {
            DB::table('employee_meta')->insert([
                'employee_id' => $id,
                "meta_key" => $meta_key,
                "meta_value" => $meta_value
            ]);
        }
    }
    function deleteEmployee(Request $request, $id)
    {
        DB::table('employees')
        ->where('id', '=', $id)
        ->delete();

        return response(['delete'=>true]);
    }
    function deleteCatEmployee(Request $request, $id)
    {
        $count = DB::table('employees')->where('position', '=', $id)->get()->count();

        if ($count > 0) {
            return response(['delete'=>false, 'msg'=>'ไม่สามารถลบกลุ่มที่กำลังถูกใช้งานอยู่ได้', 'redirect_to'=>route('catEmployee')]);
        } else {
            DB::table('employee_category')
            ->where('id', '=', $id)
            ->delete();
            return response(['delete'=>true, 'msg'=>'ดำเนินการลบรายการเสร็จสมบูรณ์', 'redirect_to'=>route('catEmployee')]);
        }
    }
}
