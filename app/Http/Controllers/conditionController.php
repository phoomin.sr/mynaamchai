<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class conditionController extends Controller {
    function checkDuplicate(Request $request)
    {
        $post = $request->all();

        $res = DB::table($post['table'])
        ->where($post['column'], $post['expression'], $post['value'])
        ->get();

        if (count($res) > 0) {
            return response(['condition_status' => true]);
        } else {
            return response(['condition_status' => false]);
        }
    }
    function search(Request $request) {
        $post = $request->all();
        $this->company = $request->session()->get('company');
        $func = "search_" . strtolower($post['type']);
        $result = call_user_func_array([$this, $func], [$request, $post['f']]);

        return response($result);
    }
    function get_extra_item(Request $request){
        $post = $request->all();
        $id = $post['id'];
        $this->company = $request->session()->get('company');
        $func = "get_extra_" . strtolower($post['type']);

        $result = call_user_func_array([$this, $func], [$request, $id]);

        return response($result);
    }
    function get_extra_sales(Request $request, $id = null) {
        if ($id !== null) {
            $res['info'] = DB::table('invoices')->where('id', '=', $id)->get();
            $res['info']['customer_info'] = CustomerController::get_customer_info($res['info'][0]->customer_id);
            $res['infos'] = DB::table('invoice_dt') 
            ->join('product', 'product.id', 'invoice_dt.product_id')
            ->join('option_group_detail', 'option_group_detail.abs_option_dt', 'product.product_unit')
            ->where('invoice_id', '=', $id)
            ->get();
        }

        return $res;
    }
    function search_sale(Request $request, $find = ""){
        if ($find !== "") {
            $res = DB::table('invoices')
            ->where('docno', 'like', "%" . $find . "%")
            ->where('company_id', '=', $this->company)
            ->get();

            $result = array();
            foreach($res as $key => $value){
                $result[] = [
                    "value" => $value,
                    "type" => "sales",
                    "text" => $value->docno
                ];
            }

            return $result;
        }
    }
    function search_manualfac(Request $request, $find = ""){
        if ($find !== "") {

        }
    }
    function search_stock(Request $request, $find = ""){
        if ($find !== "") {

        }
    }
}
