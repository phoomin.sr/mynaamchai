<?php namespace App\Http\Controllers;

use App\Http\Controllers\support\paginations;
use App\Models\customer;
use App\Models\customer_ct;
use App\Models\customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class CustomerController extends Controller{
    function searchCustomer(Request $request)
    {
        $find = "%".$request['f']."%";
        $company = $request->session()->get('company');
        $cus = new customers();

        $res = $cus
        ->where('status', '=', 1)
        ->where('company_id', '=', $company)
        ->where('customer_name', 'like', $find)
        ->orWhere('customer_no', 'like', $find)
        ->get();

        $company = new CompanyController;

        $result = array();
        foreach ($res AS $key => $item) {
            $item->customer_name = $company->add_prefix($item->business_type, $item->customer_name);
            $result[] = [
                'value' => json_encode($item),
                'text' => $item->customer_no . '::-' . $item->customer_name
            ];
        }

        return response($result);
    }
    public static function get_customer_info($id) {
        return DB::table('customers')->where('id', '=', $id)->get();
    }
    public function view(Request $request)
    {
        $company = $request->session()->get('company');
        $default = [
            'province' => $request->get('province'),
            'amphur' => $request->get('amphur'),
            'district' => $request->get('district'),
            'customer_code' => $request->get('customer_code'),
            'saletacker_id' => $request->get('saletacker_id')
        ];
        $province = DB::table('province')->pluck('province_id', 'province_name');
        $amphur = DB::table('amphur')->pluck('amphur_id', 'amphur_name');
        $district = DB::table('district')->pluck('district_id', 'district_name');
        $customers = DB::table('customers')
        ->where('company_id', '=', $company);

        if ($request->get('customer_code') !== null) {
            $customer_code = $request->get('customer_code');
            $find = str_replace(' ', '%', $customer_code);
            $customers->where(function ($query) use ($find) {
                $query->orWhere('customer_no', 'like', "%$find%");
                $query->orWhere('customer_name', 'like', "%$find%");
                $query->orWhere('customer_taxid', 'like', "%$find%");
            });
        }

        if ($request->get('saletacker_id') !== null) {
            $saletacker_id = $request->get('saletacker_id');
            $find = str_replace(' ', '%', $saletacker_id);
            $customers->where('saletaker', 'like', "%$find%");
        }

        if ($request->get('province') !== null) {
            $find = trim($request->get('province'));
            $customers->where('customer_address', 'like', "%$find%");
        }
        if ($request->get('amphur') !== null) {
            $find = trim($request->get('amphur'));
            $customers->where('customer_address', 'like', "%$find%");
        }
        if ($request->get('district') !== null) {
            $find = trim($request->get('district'));
            $customers->where('customer_address', 'like', "%$find%");
        }

        $customers->where('status', '<>', 2);

        $page = paginations::setRow($customers->get()->count())->page($request->get('page', 1));

        $customer = $customers->limit($page->maxrow())->offset($page->offset)->get();

        $var = [
            'title' => 'ระบบลูกค้า',
            'back'=>'drawer',
            'add' => route('customeradd'),
            'customers_list' => $customer,
            'pagination' => $page->pagination_skip_row(),
            'province' => $province,
            'amphur' => $amphur,
            'district' => $district,
            'default' => $default
        ];
        return view('customerview', $var);
    }
    public function add(Request $request)
    {
        $var = [
            'title' => 'ข้อมูลลูกค้า',
            'back'=>'customerview',
            'add' => route('customeradd'),
            'company_prefix' => CompanyController::BUSINESS_TYPE,
        ];
        return view('form.customerform', $var);
    }
    private function generate_customer_no() {
        /**
         * find docno at department is sale
         * and get pattern
         *
         * return string generate by sale docno pattern
         */
    }
    public function save (Request $request)
    {
        $post = $request->all();
        $customer = new customers;

        /**
         * add customer
         */
        if ($post['customer_no'] == null) {
            $customer->customer_no = $this->generate_customer_no();
        } else {
            $customer->customer_no = $post['customer_no'];
        }
        if ($post['sale_taker'] !== null) {
            $customer->saletaker = $post['sale_taker'];
        }
        if ($post['customer_taxid'] !== null) {
            $customer->customer_taxid = $post['customer_taxid'];
        }
        if ($post['branch_no'] !== null) {
            $customer->branch_no = $post['branch_no'];
        } else {
            $customer->branch_no = '00000';
        }

        $customer->business_type = $post['company_prefix'];
        $customer->customer_name = $post['customer_name'];
        $customer->customer_address_no = $post['customer_address_no'];
        $customer->customer_address = $post['customer_address'];
        $customer->phone_no = $post['customer_tel'];
        $customer->customer_fax = $post['customer_fax'];
        $customer->email = $post['customer_email'];
        $customer->customer_credit_term = $post['customer_credit_term'];
        $customer->company_id = $request->session()->get('company');

        if (isset($post['customer_id'])) {
            $customer->where('id', '=', $post['customer_id'])
            ->update([
                'saletaker' => $post['sale_taker'],
                'customer_no' => $post['customer_no'],
                'customer_taxid' => $post['customer_taxid'],
                'branch_no' => $post['branch_no'],
                'business_type' => $post['company_prefix'],
                'customer_name' => $post['customer_name'],
                'customer_address_no' => $post['customer_address_no'],
                'customer_address' => $post['customer_address'],
                'phone_no' => $post['customer_tel'],
                'customer_fax' => $post['customer_fax'],
                'email' => $post['customer_email']
            ]);
        } else {
            $customer->save();
        }

        /**
         * add contact_customer
         */

        for ($ii = 0;$ii < count($post['ct_name']);$ii++) {
            if ($post['ct_name'][$ii] !== null) {
                $ct = new customer_ct;
                $ct->name = $post['ct_name'][$ii];
                $ct->phone_no = $post['ct_tel'][$ii];
                $ct->email = $post['ct_email'][$ii];
                $ct->remark = $post['ct_remark'][$ii];
                $ct->customer_id = $customer->id;

                if (isset($post['contact_id'])) {
                    $ct->where('id', '=', $post['contact_id'][$ii])
                    ->update([
                        'name' => $post['ct_name'][$ii],
                        'phone_no' => $post['ct_tel'][$ii],
                        'email' => $post['ct_email'][$ii],
                        'remark' => $post['ct_remark'][$ii]
                    ]);
                } else {
                    $ct->save();
                }
            }
        }

        return response(['save_customer' => true, 'msg' => 'customer added.', 'redirect_to' => route('customerview')]);
    }
    public function edit(Request $request, $id)
    {
        $customer = new customers;
        $cust = $customer->where('id', '=', $id)->get()[0];
        $contact = new customer_ct;
        $ct = $contact->where('customer_id', '=', $id)->get();
        $view = [
            'title' => 'ข้อมูลลูกค้า',
            'back'=>'customerview',
            'add' => route('customeradd'),
            'company_prefix' => CompanyController::BUSINESS_TYPE,
            'data' => $cust
        ];

        if (!empty($ct)) {
            array_merge($view, ['contact' => $ct]);
        }

        return view('form.customerform', $view);
    }
    public function delete(Request $request, $id)
    {
        $customer = new customers;
        $ct = new customer_ct;

        $customer->where('id', '=', $id)->update([
            'status' => 2 //update status 2 for delete
        ]);

        return redirect(route('customerview'));
    }
}
