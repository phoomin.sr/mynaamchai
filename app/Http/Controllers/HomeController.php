<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    function index ()
    {
        $company = DB::table('company')->get();
        return view('signin', ['title' => 'Sign In', 'company' => $company]);
    }
    function RegisterWithcreateCompany()
    {
        $bns_type = CompanyController::BUSINESS_TYPE;
        return view('addCompany', ['title' => 'Register', 'type' => 'shotform', 'bns' => $bns_type]);
    }
}
