<?php namespace App\Http\Controllers;

use App\Exports\InvoiceExport;
use App\Http\Controllers\support\datetimes;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use App\Exports\customerExport;

class ReportController extends Controller
{
    public $execpt = [
        'invoice' => 13,
        'customer' => 2
    ];
    function invoice(Request $request)
    {
        $date = new DateTime();
        if (isset($_GET['firstday'])) {
            $newdate = new DateTime($_GET['firstday']);
            $firstday = date_format($newdate, 'Y-m-d');
        } else {
            $firstday = date_modify($date, 'first day of this month')->format('Y-m-d');
        }

        if (isset($_GET['lastday'])) {
            $newdate = new DateTime($_GET['lastday']);
            $lastday = date_format($newdate, 'Y-m-d');
        } else {
            $lastday = date_modify($date, 'last day of this month')->format('Y-m-d');
        }

        $date_default = [$firstday, $lastday];

        $data = DB::table('invoices')
        ->join('customers', 'customers.id', '=', 'invoices.customer_id')
        ->where('invoices.status', '<>', '13')
        ->whereBetween('invoices.docdate', [$firstday, $lastday])
        ->select('invoices.id', 'invoices.docdate', 'invoices.docno', 'customers.customer_name', 'invoices.sale_id', 'novat', 'vattype')
        ->get();

        $result = array();

        for ($i = 0;$i < count($data);$i++) {
            $dt = DB::table('invoice_dt')
            ->join('product', 'product.id', '=', 'invoice_dt.product_id')
            ->join('option_group_detail', 'option_group_detail.abs_option_dt', '=', 'product.product_unit')
            ->where('invoice_dt.invoice_id', '=', $data[$i]->id)
            ->select('product.product_name', 'invoice_dt.name', 'option_group_detail.name_option_dt', 'unit_qty', 'unit_price')
            ->get();


            for ($ii = 0;$ii < count($dt);$ii++) {
                $included = ($dt[$ii]->unit_qty * $dt[$ii]->unit_price);
                $vat = ($included * 1.07) - $included;
                $nonvat = $included - $vat;
                $result[implode(';', [$data[$i]->id, $data[$i]->docdate, $data[$i]->docno, $data[$i]->customer_name, $data[$i]->sale_id, $data[$i]->novat, $data[$i]->vattype])][] = [
                    $dt[$ii]->name,
                    $dt[$ii]->name_option_dt,
                    $dt[$ii]->unit_qty,
                    $dt[$ii]->unit_price,
                    $nonvat,
                    $vat,
                    $included
                ];
            }
        }

        return view('invoiceFilter', [
            'title'=>'รายงานใบขาย',
            'back' => 'drawer',
            'data' => $result,
            'default' => $date_default
        ]);
    }
    function customer(Request $request)
    {
        $company_id = $request->session()->get('company');
        $province = DB::table('province')->pluck('province_id', 'province_name');
        $amphur = DB::table('amphur')->pluck('amphur_id', 'amphur_name');
        $district = DB::table('district')->pluck('district_id', 'district_name');

        $result = DB::table('customers');
        $default = ['customer_code' => $request->get('customer_code'), 'saletacker_id' => $request->get('saletacker_id')];
        $default['province'] = $request->get('province');
        $default['amphur'] = $request->get('amphur');
        $default['district'] = $request->get('district');

        if (isset($_GET['customer_code'])) {
            $find = str_replace(' ', '%', $_GET['customer_code']);
            $result->orWhere('customer_no', 'like', "%$find%");
            $result->orWhere('customer_name', 'like', "%$find%");
            $result->orWhere('customer_taxid', 'like', "%$find%");
        }

        if (isset($_GET['saletacker_id'])) {
            $result->where('saletaker', '=', $_GET['saletacker_id']);
        }

        if ($default['province'] !== null) {
            $result->where('customer_address', 'like', '%'.$default['province'].'%');
        }
        if ($default['amphur'] !== null) {
            $result->where('customer_address', 'like', '%'.$default['amphur'].'%');
        }
        if ($default['district'] !== null) {
            $result->where('customer_address', 'like', '%'.$default['district'].'%');
        }

        $result->where('company_id', '=', $company_id)->where('status', '<>', $this->execpt['customer']);
        $result = $result->get();
        $data = array();
        for ($i = 0;$i < count($result);$i++) {
            $data[] = [
                $result[$i]->customer_no,
                $result[$i]->customer_taxid,
                CompanyController::add_prefix($result[$i]->business_type, $result[$i]->customer_name),
                $result[$i]->customer_address_no . ' ' .  $result[$i]->customer_address,
                $result[$i]->saletaker == null ? 'ยังไม่กำหนด':$result[$i]->saletaker
            ];
        }

        return view('customerFilter', [
            'title' => 'รายงานรายชื่อลูกค้า',
            'back' => 'drawer',
            'data' => $data,
            'default' => $default,
            'province' => $province,
            'amphur' => $amphur,
            'district' => $district
        ]);
    }
    function get_customer_byWhere($customercode = null, $saleID = null)
    {
        $result = DB::table('customers');

        if (isset($customercode)) {
            $find = str_replace(' ', '%', $customercode);
            $result->orWhere('customer_no', 'like', "%$find%");
            $result->orWhere('customer_name', 'like', "%$find%");
            // $result->raw("where concat(customer_no, '-', customer_name) like '%$find%'");
        }

        if (isset($saleID)) {
            $result->where('saletaker', '=', $saleID);
        }

        $result->where('company_id', '=', $this->company)
        ->where('status', '<>', $this->execpt['customer']);
        $result = $result->get();
        $data = array();
        for ($i = 0;$i < count($result);$i++) {
            $data[] = [
                $result[$i]->customer_no,
                $result[$i]->customer_taxid,
                CompanyController::add_prefix($result[$i]->business_type, $result[$i]->customer_name, $result[$i]->branch_no),
                $result[$i]->customer_address_no . ' ' .  $result[$i]->customer_address,
                $result[$i]->saletaker == null ? 'ยังไม่กำหนด':$result[$i]->saletaker
            ];
        }

        return $data;
    }
    function getinvoices_bydate($datefrom, $dateto)
    {
        $result = array();
        $data = DB::table('invoices')
        ->join('customers', 'customers.id', '=', 'invoices.customer_id')
        ->whereBetween('invoices.docdate', [date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $datefrom))), date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $dateto)))])
        ->where('invoices.status', '<>', '13')
        ->where('company_id', '=', $this->company)
        ->select('invoices.id', 'invoices.docdate', 'invoices.docno', 'customers.customer_name', 'invoices.sale_id', 'novat', 'vattype')
        ->get();

        for ($i = 0;$i < count($data);$i++) {
            $dt = DB::table('invoice_dt')
            ->join('product', 'product.id', '=', 'invoice_dt.product_id')
            ->join('option_group_detail', 'option_group_detail.abs_option_dt', '=', 'product.product_unit')
            ->where('invoice_dt.invoice_id', '=', $data[$i]->id)
            ->select('product.product_name', 'invoice_dt.name', 'option_group_detail.name_option_dt', 'unit_qty', 'unit_price')
            ->get();

            for ($ii = 0;$ii < count($dt);$ii++) {
                $included = ($dt[$ii]->unit_qty * $dt[$ii]->unit_price);
                $vat = ($included * 1.07) - $included;
                $nonvat = $included - $vat;
                $result[implode(';', [$data[$i]->id, $data[$i]->docdate, $data[$i]->docno, $data[$i]->customer_name, $data[$i]->sale_id, $data[$i]->novat, $data[$i]->vattype])][] = [
                    $dt[$ii]->name,
                    $dt[$ii]->name_option_dt,
                    $dt[$ii]->unit_qty,
                    $dt[$ii]->unit_price,
                    $nonvat,
                    $vat,
                    $included
                ];
            }
        }

        return $result;
    }
    function export($type, Request $request)
    {
        $get = $request->all();
        $this->company = $request->session()->get('company');

        if ($type == "invoice") {
            $data = $this->getinvoices_bydate($get['datefrom'], $get['dateto']);
            if (empty($data)) {
                return ['error' => true, 'msg' => 'ไม่พบข้อมูล.'];
            }
            return Excel::download(new InvoiceExport($data),
            'invoice_' . str_replace('/', '_', $get['datefrom']) . '-' . str_replace('/', '_', $get['dateto']) . '.xlsx');
        } else if ($type == "customer") {
            $customercode = isset($get['customer_code']) ? $get['customer_code']:null;
            $saleID = isset($get['saletacker_id']) ? $get['saletacker_id']:null;

            $data = $this->get_customer_byWhere($customercode, $saleID);
            if (empty($data)) {
                return ['error' => true, 'msg' => 'ไม่พบข้อมูล.'];
            }
            $now = date_create()->format('d_m_y');
            return Excel::download(new CustomerExport($data), 'customer_report_' . $now . '.xlsx');
        }
    }
}
