<?php namespace App\Http\Controllers;

use App\Models\company;
use App\Models\employee_category;
use App\Models\employees;
use Illuminate\Http\Request;

class workspace extends Controller
{
    function drawer(Request $request)
    {
        $emp = new employees();
        $catEmp = new employee_category();
        if ($request->session()->get('grand') == 0) {
            $einfo = $emp->where('user_id', '=', $request->session()->get('authId'))->get();
            $ecinfo = $catEmp->where('id', '=', $einfo[0]->position)->get();
            $menu = MenuController::getMenulistBypermisson($ecinfo[0]->permission);
        } else {
            $menu = MenuController::menu;
        }
        $company = new company();
        $comp = $company->where('id', '=', $request->session()->get('company'))->get();
        // $user_id = $request->session()->get('authId');
        // $emp = new employees();
        // $empCat = new employee_category();
        // $einfo = $emp->where('user_id', '=', $user_id)->get();
        // $position = $empCat->where('id', '=', $einfo[0]->position)->get();

        // $permission = json_decode($position[0]->permission, true);

        return view('appdrawer', ['title' => 'application', 'menu'=>$menu, 'appName'=>$comp[0]->business_name]);
    }
}
