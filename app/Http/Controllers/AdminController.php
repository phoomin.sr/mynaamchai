<?php namespace App\Http\Controllers;

class AdminController extends Controller
{
    function __construct($request)
    {
        if (session('authId') == "NULL") {
            return redirect(route('login'));
        }
    }
}