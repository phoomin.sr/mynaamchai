<?php namespace App\Http\Controllers;

use App\Exports\InvoiceExport;
use App\Models\invoice_dt;
use App\Models\invoice_mt;
use App\Models\invoices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel as FacadesExcel;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class salecontroller extends Controller {
    public const doctype = [
        'sale_no_credit' => 'ใบขาย(สด)',
        'sale_credit' => 'ใบขาย(เชื่อ)',
    ];
    public const date_op = [
        '%Y' => 'ปีเต็ม(4 หลัก)',
        '%y' => 'ปีย่อ(2 หลัก)',
        '%m' => 'เดือนย่อ(2 หลัก)',
        '%d' => 'วันที่(2 หลัก)',
    ];
    public const payment_type = [
        '1' => 'เครดิต',
        '2' => 'เงินโอน',
        '3' => 'เงินสด'
    ];
    function view (Request $request)
    {
        $company = $request->session()->get('company');

        $invoice = DB::table('invoices')
        ->select(['invoices.*', 'customers.customer_name'])
        ->join('customers', 'invoices.customer_id', '=', 'customers.id')
        ->where('invoices.status', '<>', 13)
        ->where('invoices.company_id', '=', $company)
        ->orderBy('invoices.created_at', 'desc')
        ->get();

        return view('viewsale', [
            'title' => 'ระบบขาย',
            'back'=>'drawer',
            'add' => route('addsale'),
            'view' => $invoice,
            'vattype' => invoiceController::VAT_TYPE
        ]);
    }
    function print (Request $request, $id)
    {
        $company = $request->session()->get('company');
        $info = DB::table('invoices')
        ->select(['invoices.*', 'customers.customer_name', 'customers.business_type',
        DB::raw('concat(customers.customer_address_no, "", customers.customer_address) as customer_address'),
        'customers.customer_taxid'])
        ->join('customers', 'customers.id', '=', 'invoices.customer_id')
        ->where('invoices.id', '=', $id)
        ->get();

        $info[0]->customer_name = CompanyController::add_prefix($info[0]->business_type, $info[0]->customer_name);

        if ($info[0]->vattype !== 'vat_zero' || $info[0]->vattype !== 'non_vat'){
            $vatres = $info[0]->novat * 7 / 100;
        } else {
            $vatres = 0;
        }

        $date = date_create($info[0]->duedate);
        $duedate = date_format($date, 'd/m/Y');
        // $infos = DB::table('invoice_dt')
        // ->join('product', 'product.id', 'invoice_dt.product_id')
        // ->join('option_group_detail', 'option_group_detail.abs_option_dt', 'product.product_unit')
        // ->where('invoice_id', '=', $id)->get()->toArray();

        $infos = DB::table('invoice_dt')
        ->join('product', 'product.id', 'invoice_dt.product_id')
        ->join('option_group_detail', 'option_group_detail.abs_option_dt', 'product.product_unit')
        ->where('invoice_id', '=', $id)->get()->toArray();

        $page = array_chunk($infos, 3);

        return view('form.printsale', [
            'info' => $info,
            'page' => $page,
            'duedate' => $duedate,
            'vatres' => $vatres,
            'satang' => currencyController::set_currency(number_format($info[0]->novat + $vatres, 2))
        ]);
    }
    function add (Request $request)
    {
        $select = <<<EOT
select dt.id, name_option_dt as name from option_group
join option_group_detail as dt on dt.option_id = option_group.id
where option_group.name = 'หน่วย'
EOT;
        $resUnit = DB::select($select);

        return view('form.addsale', [
            'title' => 'สร้างใบขาย',
            'back' => 'saleview',
            'findCustomer' => route('searchCustomer'),
            'findProduct' => route('searchProduct'),
            'findAddress' => route('searchAddress'),
            'getExtra_product' => route('getExtra_product'),
            'vattype' => invoiceController::VAT_TYPE,
            'payment_type' => self::payment_type,
            'list' => 3,
            'unit' => $resUnit
        ]);
    }
    function edit (Request $request, $id)
    {
        // dd($request, $id);

        $select = <<<EOT
select dt.id, name_option_dt as name from option_group
join option_group_detail as dt on dt.option_id = option_group.id
where option_group.name = 'หน่วย'
EOT;
        $resUnit = DB::select($select);

        $ivinfo = DB::table('invoices')
        ->select(['invoices.*', 'customers.customer_name', DB::raw('concat(customer_address_no, " ", customer_address) as customer_address')])
        ->join('customers', 'customers.id', 'invoices.customer_id')
        ->where('invoices.id', '=', $id)
        ->get();

        $ivinfos = DB::table('invoice_dt')
        ->join('product', 'product.id', 'invoice_dt.product_id')
        ->join('option_group_detail', 'option_group_detail.abs_option_dt', 'product.product_unit')
        ->where('invoice_id', '=', $id)
        ->get();

        return view('form.updatesale', [
            'title' => 'แก้ไขใบขาย',
            'back' => 'saleview',
            'findCustomer' => route('searchCustomer'),
            'findProduct' => route('searchProduct'),
            'findAddress' => route('searchAddress'),
            'getExtra_product' => route('getExtra_product'),
            'vattype' => invoiceController::VAT_TYPE,
            'payment_type' => self::payment_type,
            'info' => $ivinfo,
            'infos' => $ivinfos,
            'unit' => $resUnit
        ]);
    }
    function save (Request $request)
    {
        $post = $request->all();
        if ($post['customer_id'] !== "") {
            $customer_id = $post['customer_id'];
        }

        $dc = date_create(str_replace('/', '-', $post['doc_date']));
        $de = date_create(str_replace('/', '-', $post['due_date']));

        // invoices
        $invoice  = new invoices;

        $invoice->docno = $post['docno'];
        $invoice->sale_id = $post['sale'];
        $invoice->vattype = $post['vattype'];
        $invoice->novat = $post['sub_total'];
        $invoice->docdate = date_format($dc, "Y-m-d");
        $invoice->duedate = date_format($de, "Y-m-d");
        $invoice->discount = $post['discount_baht'] > 0 ? $post['discount_baht']:0;
        $invoice->customer_id = $customer_id;
        $invoice->status = 1;
        $invoice->company_id = $request->session()->get('company');
        $invoice->save();

        // invoice_dt
        for ($i = 0;$i < count($post['product_id']);$i++) {
            if ($post['product_id'][$i] > 0) {
                $dt = new invoice_dt;
                $dt->invoice_id = $invoice->id;
                $dt->product_id = $post['product_id'][$i];
                $dt->name = $post['product'][$i];
                $dt->unit_id = NULL;
                $dt->unit_qty = $post['qty'][$i];
                $dt->unit_price = $post['price'][$i];
                $dt->unit_discount = 0;
                $dt->save();
            }
        }

        if (isset($invoice->id)) {
            return response(['save_invoice' => true, 'msg' => 'saved invoice.', 'redirect_to' => route('saleview')]);
        } else {
            return response(['save_invoice' => false]);
        }
    }
    function update(Request $request)
    {
        $post = $request->all();
        if ($post['customer_id'] !== "") {
            $customer_id = $post['customer_id'];
        }

        $dc = date_create(str_replace('/', '-', $post['doc_date']));
        $de = date_create(str_replace('/', '-', $post['due_date']));

        DB::table('invoices')->where('id', '=', $post['id'])
        ->update([
            'docno' => $post['docno'],
            'sale_id' => $post['sale'],
            'vattype' => $post['vattype'],
            'novat' => $post['sub_total'],
            'duedate' => $de,
            'docdate' => $dc,
            'discount' => $post['discount_baht'],
            'customer_id' => $customer_id,
            'status' => 1,
            'company_id' => $request->session()->get('company')
        ]);

        // DB::table('invoice_dt')->where('invoice_id', '=', $post['id'])->delete();
        for ($i = 0;$i < count($post['product_id']);$i++) {
            if ($post['product_id'][$i] > 0) {
                if (isset($post['invoice_dt_id'][$i])) {
                    $invoice_dt = new invoice_dt;
                    $invoice_dt
                    ->where("id", "=", $post['invoice_dt_id'][$i])
                    ->update([
                        "product_id" => $post['product_id'][$i],
                        "name" => $post['product'][$i],
                        "unit_id" => NULL,
                        "unit_qty" => $post['qty'][$i],
                        "unit_price" => $post['price'][$i],
                        "unit_discount" => 0
                    ]);
                } else {
                    $invoice_dt = new invoice_dt;
                    $invoice_dt->invoice_id = $post['id'];
                    $invoice_dt->product_id = $post['product_id'][$i];
                    $invoice_dt->name = $post['product'][$i];
                    $invoice_dt->unit_id = NULL;
                    $invoice_dt->unit_qty = $post['qty'][$i];
                    $invoice_dt->unit_price = $post['price'][$i];
                    $invoice_dt->unit_discount = 0;
                    $invoice_dt->save();
                }
            }
        }

        return response(['save_invoice' => true, 'msg' => 'saved invoice.', 'redirect_to' => route('saleview')]);
    }
    function delete(Request $request, $id)
    {
        // DB::table('invoice_dt')->where('invoice_id', '=', $id)->delete();
        DB::table('invoices')->where('id', '=', $id)->update(['status' => 13]);

        // DB::table('invoice_mt')->where('invoice_id', '=', $id)->delete();

        return redirect(route('saleview'));
    }
    function setting(Request $request)
    {
        return view('settingsale', [
            'title' => 'ตั้งค่าใบขาย',
            'back' => 'saleview',
            'doctype' => self::doctype,
            'dateop' => self::date_op
        ]);
    }
}
