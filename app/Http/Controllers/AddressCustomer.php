<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddressCustomer extends Controller {
    function searchFullAddress (Request $request) {
        $find = "%" . $request['f'] . "%";

        $res = DB::table('district')
        ->select(DB::raw('concat(if(province.PROVINCE_ID = 1, "แขวง ", "ตำบล ") , district.DISTRICT_NAME, if(province.PROVINCE_ID = 1, "เขต ", "อำเภอ "), amphur.AMPHUR_NAME, if(province.PROVINCE_ID = 1, "", "จังหวัด ") , province.PROVINCE_NAME, amphur.POSTCODE) as name, concat(district.DISTRICT_ID, ",", amphur.AMPHUR_ID, ",", province.PROVINCE_ID) as id'))
        ->join('amphur', 'amphur.AMPHUR_ID', 'district.AMPHUR_ID')
        ->join('province', 'province.PROVINCE_ID', 'district.PROVINCE_ID')
        ->where(DB::raw('concat(district.DISTRICT_NAME, " ", amphur.AMPHUR_NAME, " ", province.PROVINCE_NAME, " ", amphur.POSTCODE)'), 'like', $find)
        ->limit(10)
        ->get();

        $result = [];

        foreach ($res AS $key => $item) {
            $result[] = [
                'value' => $item->id,
                'text' => $item->name
            ];
        }

        return response($result);
    }
}
