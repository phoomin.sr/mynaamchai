<?php namespace App\Http\Controllers;

use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class MenuController extends Controller
{
    public const menu =
    [
        'Master' => [
            'กลุ่มคลังสินค้า' => [
                'icon' => 'fas fa-braille',
                'route_name' => 'group'
            ],
            'กลุ่มตัวเลือก(เพิ่มเติม)' => [
                'icon' => 'fas fa-stream',
                'route_name' => 'showAllOption'
            ],
            'สินค้าคงเหลือ' => [
                'icon' => 'fas fa-box-open',
                'route_name' => 'product'
            ],
            'ตั้งค่าระบบคลัง' => [
                'icon' => 'fas fa-cog',
                'route_name' => 'setupStock'
            ]
        ],
        'บัญชี' => [
            'ตั้งค่าระบบบัญชี' => [
                'icon' => 'fas fa-cog',
                'route_name' => 'Accsetting'
            ],
        ],
        "ระบบขาย" => [
            'ระบบขาย' => [
                'icon' => 'fas fa-cash-register',
                'route_name' => 'saleview'
            ],
            'ใบกำกับการขนส่งน้ำมัน(หจก.กังหัน)' => [
                "icon" => 'fas fa-truck-loading',
                "route_name" => "deliveryview"
            ],
            'ลูกค้า' => [
                'icon' => 'fas fa-user-tie',
                'route_name' => 'customerview'
            ],
            'ตั้งค่าระบบขาย' => [
                'icon' => 'fas fa-cog',
                'route_name' => 'settingsale'
            ],
        ],
        "ระบบจัดซื้อ" => [
            'ใบสั่งซื้อ' => [
                'icon' => 'fas fa-store-alt',
                'route_name' => 'poview'
            ],
            'ผู้ผลิต/คู่ค้า' => [
                'icon' => 'fas fa-parachute-box',
                'route_name' => 'supplychainview'
            ],
            'ตั้งค่าระบบจัดซื้อ' => [
                'icon' => 'fas fa-cog',
                'route_name' => 'supplychainsetting'
            ],
        ],
        'คลังสินค้า' => [
            'รายการสต็อก' => [
                'icon' => 'fas fa-boxes',
                'route_name' => 'stockview'
            ],
            'รับสินค้า(Manual)' => [
                'icon' => 'fas fa-truck',
                'route_name' => 'IncomeStockmn'
            ],
            'รับสินค้า(PO)' => [
                'icon' => 'fas fa-truck-moving',
                'route_name' => 'stockSetting'
            ],
            'เบิกสินค้า' => [
                'icon' => 'fas fa-cubes',
                'route_name' => 'widthdraw_manual_view'
            ],
            'ตั้งค่า warehouse' => [
                'icon' => 'fas fa-cog',
                'route_name' => 'stockSetting'
            ],
        ],
        'ทรัพยากรมนุษย์' => [
            'ตำแหน่งงาน' => [
                'icon' => 'fas fa-user-friends',
                'route_name' => 'catEmployee'
            ],
            'พนักงาน' => [
                'icon' => 'fas fa-users',
                'route_name' => 'employee'
            ]
        ],
        'รายงาน' => [
            'รายงานฝ่ายขาย' => [
                'icon' => 'fas fa-file-invoice',
                'route_name' => 'invoiceReport'
            ],
            'รายงานลูกค้า' => [
                'icon' => 'fas fa-portrait',
                'route_name' => 'customerReport'
            ]
        ]
    ];
    static function getMenulistBypermisson($menu)
    {
        $arrMenu = json_decode($menu, true);
        $userMenu = array();
        foreach (self::menu as $key => $items) {
            $initems = [];
            foreach ($items as $k => $item) {
                if (isset($arrMenu[$item['route_name']]) && $arrMenu[$item['route_name']] !== "000") {
                    $initems[$k] = $item;
                }
            }
            if (!empty($initems)) {
                $userMenu[$key] = $initems;
            }
        }
        return $userMenu;
    }
}
