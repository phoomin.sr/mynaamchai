<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\usermeta;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    CONST HASH = "_msc";
    CONST LEVEL_USER = [
        '99' => true,
        '0' => false
    ];
    function user(Request $request)
    {
        $emp = DB::table('employees')
        ->where('company_id', '=', $request->session()->get('company'))
        ->get();

        $Allposition = DB::table('employee_category')
        ->where('company_id', '=', $request->session()->get('company'))
        ->get();

        $n = count($Allposition);
        if ($n > 0) {
            for ($i = 0;$i < $n;$i++) {
                $position[$Allposition[$i]->id] = $Allposition[$i]->position_name;
            }
        } else {
            $position = [];
        }

        return view('form.employees', ['title'=>'ผู้ใช้', 'back'=>'drawer','employee'=>$emp, 'position'=>$position]);
    }
    function create_account($company_id, Request $request)
    {
        $post = $request->all();
        $user = new User;

        $user->name = $post['username'];
        $user->email = $post['email'];
        $user->password = UserController::hash($post['password']);
        $user->grand = self::LEVEL_USER[$post['grand']];
        $user->company_id = $company_id->id;
        $user->save();

        return $user->id;
    }
    static function hash($password)
    {
        return md5($password.self::HASH);
    }
    function autoLogin($userid, $compId = null)
    {
        $user = new User();

        $this->auth = $user->where('id', '=', $userid)->get();

        Session::put('authId', $this->auth[0]->id);
        Session::put('grand', $this->auth[0]->grand);
        Session::put('name', $this->auth[0]->name);
        Session::put('company', $compId);

        return route('drawer');
    }
    function signin (Request $request)
    {
        $user = new User();

        $this->auth = $user->where('name', '=', $request->input('username'))
        ->where('password', '=', UserController::hash($request->input('password')))
        ->where('company_id', '=', $request->input('company_id'))
        ->get();

        if (isset($this->auth[0])) {
            Session::put('authId', $this->auth[0]->id);
            Session::put('grand', $this->auth[0]->grand);
            Session::put('name', $this->auth[0]->name);
            Session::put('company', $this->auth[0]->company_id);

            return response([
                'login' => true,
                'msg' => 'เช้าสู่ระบบสำเร็จ. ระบบกำลังเตรียมความพร้อมอีก 5 วินาทีจะนำเข้าสู่ระบบ',
                'redirect_to' => route('drawer')
            ]);
        } else {
            return response([
                'login' => false,
                'msg' => 'ชื่อหรือรหัสผ่านไม่ถูกต้อง. กรุณาลองใหม่อีกครั้ง'
            ]);
        }
    }
    function signout(Request $request)
    {
        $request->session()->forget('name');
        return redirect('/');
    }
    function profile(Request $request)
    {
        $user = new User;

        $userId = $request->session()->get('authId');
        $uinfo = $user
        ->join('employees', 'employees.id', '=','users.id')
        ->where('users.id', '=', $userId)->get();

        dd($uinfo);
    }
    function destroy()
    {
        DB::table('users')->delete();
        DB::table('company')->delete();
        DB::table('usermeta')->delete();

        return redirect('/');
    }
}
