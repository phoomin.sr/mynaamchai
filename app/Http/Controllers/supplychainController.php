<?php namespace App\Http\Controllers;

class supplychainController extends Controller {
    function view()
    {
        return view('supplier', [
            'title' => 'ผู้ผลิต/คู่ค้า',
            'back' => 'drawer',
        ]);
    }
    function add ()
    {
        return view('form.supplier', [
            'title' => 'เพิ่มผู้ผลิต/คู่ค้า',
            'back' => 'supplychainview',
            'action' => route('supplychainsave'),
            'business_type' => CompanyController::BUSINESS_TYPE
        ]);
    }
}
