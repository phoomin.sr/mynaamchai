<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class invoiceController extends Controller {
    public const VAT_TYPE = [
        "vat" => "VAT 7%",
        "include_vat" => "Include VAT 7%",
        "vat_zero" => "VAT 0%",
        "non_vat" => "NON VAT"
    ];
}