<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pubcharse_orderController extends Controller
{
    function view (Request $request)
    {
        return view('poview', [
            'title' => 'ใบสั่งซื้อ',
            'back' => 'drawer',
        ]);
    }
    function add (Request $request)
    {
        return view('form.poform', [
            'title' => '+ ใบสั่งซื้อ',
            'back' => 'poview',
        ]);
    }
}
