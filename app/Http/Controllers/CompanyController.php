<?php namespace App\Http\Controllers;

use App\Models\company;
use App\Models\User;
use App\Models\usermeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
    public CONST BUSINESS_TYPE = [
        1 => "กิจการเจ้าของคนเดียว (Sold Proprietorship)",
        2 => "ห้างหุ้นส่วนจำกัด (Partnership)",
        5 => "ห้างหุ้นส่วน (Partnership)",
        3 => "บริษัทจำกัด (Limited Company)",
        4 => "บริษัทมหาชนจำกัด (Public Limited Company)"
    ];
    public CONST ANS_BUSINESS_TYPE = [
        1 => "{ name }",
        2 => "ห้างหุ้นส่วน { name } จำกัด",
        3 => "บริษัท { name } จำกัด",
        4 => "บริษัท { name } จำกัด (มหาชน)",
        5 => "ห้างหุ้นส่วน { name } จำกัด",
    ];
    final static function add_prefix($business_type, $business_name, $branch_id = '00000')
    {
        $prefix = CompanyController::ANS_BUSINESS_TYPE[$business_type];
        $company_name = str_replace('{ name }', $business_name, $prefix);

        $branch = $branch_id == '00000' ? ' (สำนักงานใหญ่)':" ($branch_id)";
        return $company_name . $branch;
    }
    function saveCompany (Request $request)
    {
        // แยกการเพิ่ม user กับเพิ่ม company ออกจากกัน โดย shotform จะเรียก save company ก่อน
        $post = $request->all();
        $company = new company();

        if ($company->where('business_name', '=', $post['business_name'])->get()->count() > 0) {
            return response(['addCompany' => false,
                'msg' => $request->business_name.'มีอยู่ในระบบแล้ว สามารถเข้าสู่ระบบได้จากหน้า Index ครับ'
            ]);
        }

        $company->business_name = $post['business_name'];
        $company->business_type = $post['business_type'];
        $company->wh_tax = $post['wh_tax'];
        $company->trade_no = $post['business_trade_no'];
        if ($post['wh_tax'] == 1) {
            $company->tax_id = $post['business_trade_no'];
        }
        $company->save();

        $user = new UserController;
        $userId = $user->create_account($company, $request);
        $response = $user->autoLogin($userId, $company->id);

        return response([
            'addCompany' => true,
            'msg' => 'เราได้เพิ่มคุณเข้าระบบแล้วระบบกำลังนำทางคุณไปยัง'.$request->business_name,
            'redirect_to' => $response
        ]);
    }
}
