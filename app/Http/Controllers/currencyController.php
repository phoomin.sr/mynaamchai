<?php namespace App\Http\Controllers;

use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class currencyController extends Controller
{
    const STR_NUMBER = [
        'ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'เอ็ด', 'ยี่สิบ'
    ];
    const DOT = "จุด";
    const STR_COLUMN = [
        '6' => 'ล้าน', '5' => 'แสน', '4'=>'หมื่น', '3' => 'พัน', '2'=>'ร้อย', '1'=>'สิบ', '0'=>''
    ];
    public static function set_currency($num)
    {
        $currency = new currencyController;
        $currency->number = $num;
        return $currency->determinate()->baht;
    }
    final function determinate()
    {
        $afdot = explode('.', str_replace(',', '', $this->number));
        $integers = $afdot[0];
        $afterDots = $afdot[1];
        // $n = count($this->integers);
        $this->integers = str_split($integers);
        $this->afterDots = str_split($afterDots);

        $this->merge_string();

        return $this;
    }
    final function get_string()
    {
        return $this->currency_str;
    }
    final function merge_string()
    {
        $baht = "";
        $integers = $this->get_integers();
        $afterDots = $this->get_afterDots();

        $baht .= $integers;
        if ($afterDots !== "") {
            $baht .= $integers . self::DOT . $afterDots . "สตางค์";
        } else {
            $baht .= "บาทถ้วน";
        }

        return $this->baht = $baht;
    }
    function get_integers()
    {
        $str_interger = array();
        $prepare = array_chunk(array_reverse($this->integers), 6);
        $rcolumn = array_reverse(self::STR_COLUMN);
        for ($i = 0;$i < count($prepare);$i++) {
            foreach ($prepare[$i] as $column => $number) {
                if ($number > 0) {
                    if ($column == 1) {
                        if ($number == 1) {
                            $str_interger[$i][] = $rcolumn[$column];
                        } else if ($number == 2) {
                            $str_interger[$i][] = self::STR_NUMBER[11] . $rcolumn[$column];
                        } else {
                            $str_interger[$i][] = self::STR_NUMBER[$number] . $rcolumn[$column];
                        }

                        if ($prepare[$i][$column - 1] == 1) {
                            $str_interger[$i][$column - 1] = self::STR_NUMBER[10];
                        }
                    } else {
                        $str_interger[$i][] = self::STR_NUMBER[$number] . $rcolumn[$column];
                    }
                }
            }
        }

        for ($i = 0;$i < count($str_interger);$i++) {
            $str_interger[$i] = implode('', array_reverse($str_interger[$i]));
        }

        $str = implode('ล้าน', array_reverse($str_interger));

        $this->str_integers = $str;
        return $this->str_integers;
    }
    function get_afterDots()
    {
        $n = count($this->afterDots);
        $str_afterDots = "";
        for ($i = 0;$i < $n;$i++) {
            if ($this->afterDots[$i] > 0) {
                $str_afterDots = self::STR_NUMBER[$this->afterDots[$i]];
            }
        }

        $this->str_afterDots = $str_afterDots;
        return $this->str_afterDots;
    }
}
