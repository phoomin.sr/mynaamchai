<?php namespace App\Http\Controllers;

use App\Models\option;
use App\Models\option_group_detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OptionController extends Controller
{
    static function get_unit($company_id)
    {
        $unit = "หน่วย";
        $option = new option();
        $unit_id = $option->where('name', '=', $unit)
        ->where('company_id', '=', $company_id)
        ->get();

        $optiondt = new option_group_detail();
        $option_dt = $optiondt->where('option_id', '=', $unit_id[0]->id)->get();

        foreach ($option_dt as $key => $value) {
            $res[$value->id] = $value->name_option_dt;
        }

        return $res;
    }
    function get_options($company_id)
    {
        $option = new option();
        $res = $option->where('company_id', '=', $company_id)->get();

        if (count($res) == 0) {
            return array();
        } else {
            for ($i = 0;$i < count($res);$i++) {
                $option_dt = new option_group_detail;
                $dtres = $option_dt
                ->where('option_id', '=', $res[$i]->id)
                ->get();
                $opt = [];

                for($ii = 0;$ii < count($dtres);$ii++){
                    $opt[$dtres[$ii]->abs_option_dt] = $dtres[$ii]->name_option_dt;
                }

                $result[] = [
                    'option_id' => $res[$i]->id,
                    'option_name' => $res[$i]->name,
                    'option_inner' => $opt
                ];
            }
            return $result;
        }
    }
    function get_option_value(Request $request)
    {
        $option = new option_group_detail();
        $opt = $option->where('option_id', '=', $request->get('option_id'))->get();
        $result = [];

        foreach($opt as $items)
        {
            $result[$items->abs_option_dt] = $items->name_option_dt;
        }

        return $result;
    }
    function option (Request $request)
    {
        $list = $this->get_options($request->session()->get('company'));
        return view('option', ['title' => 'ตัวเลือก', 'back' => 'drawer', 'option' => $list]);
    }
    function addoption()
    {
        return view('form.option', ['title' => 'ตัวเลือก', 'back' => 'showAllOption', 'action' => route('saveoption')]);
    }
    function updateformoption($id, Request $request)
    {
        $option = new option();
        $option_dt = new option_group_detail();
        $opt = $option->where('id', '=', $id)->get();
        $opt_dt = $option_dt->where('option_id', '=', $id)->get();

        return view('form.option', [
            'title' => 'ตัวเลือก',
            'back' => 'showAllOption',
            'action' => route('updateoption'),
            'info' => $opt,
            'infos' => $opt_dt,
            'id' => $id
            ]);
    }
    function saveoption(Request $request)
    {
        $post = $request->all();
        $company_id = $request->session()->get('company');
        $option = new option();

        $res = $option->where('name', '=', $post['group_main'])->where('company_id', '=', $company_id)->get();

        if (count($res) > 0) {
            return response(['save'=>false,'msg'=>'กลุ่มซ้ำ กรุณาเปลี่ยนชื่อกลุ่มแล้วลองใหม่อีกครั้ง.']);
        }

        $lastId = DB::table('option_group')->insertGetId([
            'name' => $post['group_main'],
            'company_id' => $request->session()->get('company'),
        ]);

        for($i = 0;$i < count($post['abs_option_name']);$i++){
            if ($post['abs_option_name'][$i] !== null) {
                DB::table('option_group_detail')->insert([
                    'option_id' => $lastId,
                    'abs_option_dt' => $post['abs_option_name'][$i],
                    'name_option_dt' => $post['option_name'][$i],
                ]);
            }
        }

        return response([
                'save'=>true,
                'msg'=>'ทำการเพิ่มตัวเลือก และรายละเอียดของตัวเลือกเรียบร้อย',
                'redirect_to'=>route('showAllOption')
            ]);
    }
    function updateoption (Request $request)
    {
        $post = $request->all();

        DB::table('option_group')
        ->where('id', '=', $post['option_id'])
        ->update(['name' => $post['group_main']]);

        DB::table('option_group_detail')
        ->where('option_id', '=', $post['option_id'])
        ->delete();

        // insert new group detail
        for ($i = 0;$i < count($post['abs_option_name']);$i++) {
            if ($post['abs_option_name'][$i] !== null) {
                DB::table('option_group_detail')
                            ->insert([
                                'option_id' => $post['option_id'],
                                'abs_option_dt' => $post['abs_option_name'][$i],
                                'name_option_dt' => $post['option_name'][$i]
                            ]);
            }
        }

        return response([
            'save'=>true,
            'msg'=>'ทำการอัพเดทตัวเลือก และรายละเอียดของตัวเลือกเรียบร้อย',
            'redirect_to'=>route('showAllOption')
        ]);
    }
    function deleteOption($id)
    {
        DB::table('option_group')->where('id', '=', $id)->delete();
        DB::table('option_group_detail')->where('option_id', '=', $id)->delete();

        return redirect(route('showAllOption'));
    }
}
