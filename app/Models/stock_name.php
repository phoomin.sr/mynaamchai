<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class stock_name extends Model {
    protected $table = "stockname";
    protected $fillable = [
        "name", "company_id"
    ];
}