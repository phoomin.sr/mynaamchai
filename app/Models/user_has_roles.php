<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_has_roles extends Model
{
    use HasFactory;
    protected $table = 'user_has_role';
    protected $fillable =[
        'user_id',
        'mapper'
    ];
}
