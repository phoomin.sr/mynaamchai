<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class stock extends Model {
    protected $table = "stock";
    protected $fillable = [
        'addId',
        'invId',
        'unit_id',
        'pack_id',
        'qty',
        'unitprice',
        'lot',
        'location',
        'withdraw_id',
        'business_id'
    ];
}
