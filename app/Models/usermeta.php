<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class usermeta extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'meta_key',
        'meta_value'
    ];
}
