<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class employees extends Model
{
    use HasFactory;
    protected $table = 'employees';
    protected $fillable =[
        'firstname',
        'lastname',
        'position',
        'user_id',
        'company_id'
    ];

    function get_all_employee($company)
    {
        $emp = DB::table('employees')
        ->where('company_id', '=', $company)
        ->get();

        return $emp;
    }
    function get_employee_byId($empId, $with_meta = false)
    {
        $emp_meta = new employee_meta();
        $response['info'] = DB::table('employees')
        ->where('id', '=', $empId)
        ->get()[0];

        if ($with_meta) {
            $response['meta'] = $emp_meta->get_user_meta_byId($empId);
        }

        return $response;
    }
}
