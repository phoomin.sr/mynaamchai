<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class employee_category extends Model
{
    use HasFactory;
    protected $table = 'employee_category';
    protected $fillable =[
        'position_name',
        'slug',
        'permission',
        'company_id'
    ];

    public function get_all_position($company, $with_key = false)
    {
        $Allposition = DB::table('employee_category')
        ->where('company_id', '=', $company);

        if ($with_key) {
            $position = $Allposition->pluck('position_name', 'id');
        } else {
            $position = $Allposition->get();
        }

        return $position;
    }
}
