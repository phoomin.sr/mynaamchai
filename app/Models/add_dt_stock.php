<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class add_dt_stock extends Model {
    protected $table = "add_dt_stock";
    protected $fillable = [
        'addId',
        'inv_id',
        'unit_id',
        'pack',
        'unit_price'
    ];
}
