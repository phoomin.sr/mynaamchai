<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customer_ct extends Model
{
    use HasFactory;
    protected $table = 'customer_ct';
    protected $fillable =[
        'name',
        'phone_no',
        'email',
        'remark',
        'customer_id'
    ];
    public $timestamps = true;
}
