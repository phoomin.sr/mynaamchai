<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class invoice_dt extends Model
{
    protected $table = "invoice_dt";
    protected $fillable = [
        'invoice_id',
        'product_id',
        'name',
        'unit',
        'unit_qty',
        'unit_price',
        'unit_discount'
    ];
    public $timestamps = false;
}
