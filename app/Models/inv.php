<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class inv extends Model {
    protected $table = 'inv';
    protected $fillable = [
        'type', "ref_id"
    ];
}
