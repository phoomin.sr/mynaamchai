<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    use HasFactory;
    protected $table = 'company';
    protected $fillable =[
        'business_name',
        'image_id',
        'business_type',
        'wh_tax',
        'trade_no',
        'tax_id',
        'isBranch',
        'branch_code',
        'official_tel',
        'phonenumber',
        'fax',
        'website',
    ];
}
