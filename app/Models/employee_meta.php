<?php
    namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class employee_meta extends Model
{
    use HasFactory;
    protected $table = 'employee_meta';
    protected $fillable =[
        'employee_id',
        'meta_key',
        'meta_value'
    ];

    function get_user_meta_byId($user_id)
    {
        $meta = DB::table('employee_meta')
        ->where('employee_id', '=', $user_id)
        ->pluck('meta_value', 'meta_key');

        return $meta;
    }
}
