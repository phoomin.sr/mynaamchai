<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'product';
    protected $fillable =[
        'product_cat',
        'product_code',
        'product_name',
        'product_generation',
        'product_unit',
        'product_location',
        'product_minstock',
        'product_sale_price',
        'status',
        'company_id'
    ];
}
