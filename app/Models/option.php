<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class option extends Model
{
    protected $table = 'option_group';
    protected $fillable = [
        'name',
        'company_id'
    ];
}
