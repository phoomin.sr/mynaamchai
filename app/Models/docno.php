<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class docno extends Model{
    protected $table = "docno";
    protected $fillable = [
        "department",
        "docname",
        "pattrn",
        "company_id",
    ];
}