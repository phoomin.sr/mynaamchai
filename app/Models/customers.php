<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class customers extends Model{
    protected $table = "customers";
    protected $fillable = [
        'customer_no',
        'business_type',
        'customer_name',
        'saletaker',
        'customer_taxiv',
        'customer_group',
        'customer_address_no',
        'customer_address',
        'email',
        'phone_no',
        'payment_method',
        'credit_pay',
        'placeBill_method',
        'isBranch',
        'branch_no',
        'default_vat_type',
        'company_id',
        'saletaker_id',
        'customer_fax',
        'customer_credit_term'
    ];
}
