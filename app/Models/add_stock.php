<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class add_stock extends Model{
    protected $table = "add_stock";
    protected $fillable = [
        'type',
        'no',
        'docdate',
        'user_id',
        'company_id'
    ];
}
