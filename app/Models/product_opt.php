<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class product_opt extends Model
{
    protected $table = 'product_opt';
    protected $fillable =[
        'product_id',
        'option_id',
        'option_slug'
    ];
}