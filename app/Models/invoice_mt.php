<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class invoice_mt extends Model 
{
    protected $table = 'invoice_mt';
    protected $fillable = [
        'invoice_id',
        'meta_key',
        'meta_value'
    ];
    public $timestamps = false;
}