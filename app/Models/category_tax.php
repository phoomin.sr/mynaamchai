<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category_tax extends Model
{
    use HasFactory;
    protected $table = 'category_tax';
    protected $fillable =[
        'parent',
        'deep',
        'lineage',
        'company_id'
    ];
}
