<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class invoices extends Model {
    public $vat_type = [
        'vat', 'include vat', 'vat 0%', 'non-vat'
    ];
    public $op_status = [
        'created' => 'ฉบับร่าง',
        'approved' => 'ตรวจสอบแล้ว',
        'delived' => 'อยู่ระหว่างจัดส่ง',
        'billing' => 'รอชำระเงิน',
        'completed' => 'ชำระเงินแล้ว',
        'cancel' => 'ยกเลิก'
    ];
    protected $table = "invoices";
    protected $fillable = [
        'docno',
        'sale_id',
        'novat',
        'vattype',
        'discount',
        'duedate',
        'docdate',
        'customer_id',
        'status',
        'company_id'
    ];
}
