<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class option_group_detail extends Model
{
    use HasFactory;
    protected $table = 'option_group_detail';
    protected $fillable =[
        'option_id',
        'abs_option_dt',
        'name_option_dt',
    ];
}
