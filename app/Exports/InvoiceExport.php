<?php

namespace App\Exports;

use Illuminate\Contracts\View\View as ViewView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class InvoiceExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): ViewView
    {
        return view('exports.invoices', [
            'data' => $this->data
        ]);
    }
}
