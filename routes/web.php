<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/signout', [App\Http\Controllers\UserController::class, 'signout'])->name('signout');
Route::get('/destroy', [App\Http\Controllers\UserController::class, 'destroy']);
Route::post('/checkDuplicate', [App\Http\Controllers\ConditionControl::class, 'checkDuplicate'])->name('checkDuplicate');
Route::post('/searchWithdraw', [App\Http\Controllers\conditionController::class, 'search'])->name('search');
Route::post('/get_extra_item', [App\Http\Controllers\conditionController::class, 'get_extra_item'])->name('get_extra_item');
Route::prefix('user')->group(function () {
    Route::get('/setting', [App\Http\Controllers\UserController::class, 'setting'])->name('setting');
    Route::get('/profile', [App\Http\Controllers\UserController::class, 'profile'])->name('profile');
});
Route::get('/shotform-company', [App\Http\Controllers\HomeController::class, 'RegisterWithcreateCompany']);
Route::prefix('request')->group(function () {
    Route::post('/saveCompany', [App\Http\Controllers\CompanyController::class, 'saveCompany'])->name('savecompany');
    Route::post('/signin', [App\Http\Controllers\UserController::class, 'signin'])->name('login');
});
Route::prefix('workspace')->group(function () {
    Route::get('/', [App\Http\Controllers\workspace::class, 'drawer'])->name('drawer');
    Route::prefix('supplychain')->group(function () {
        Route::get('/view', [\App\Http\Controllers\supplychainController::class, 'view'])->name('supplychainview');
        Route::get('/add', [\App\Http\Controllers\supplychainController::class, 'add'])->name('addsupplier');
        Route::get('/save', [\App\Http\Controllers\supplychainController::class, 'view'])->name('supplychainsave');
        Route::get('/setting', [\App\Http\Controllers\supplychainController::class, 'setting'])->name('supplychainsetting');
    });
    Route::prefix('/pubcharse')->group(function () {
        Route::get('/view', [\App\Http\Controllers\pubcharse_orderController::class, 'view'])->name('poview');
        Route::get('/add', [\App\Http\Controllers\pubcharse_orderController::class, 'add'])->name('poadd');
        Route::post('/save', [\App\Http\Controllers\pubcharse_orderController::class, 'save'])->name('posave');
    });
    Route::prefix('sale')->group(function () {
        Route::get('/view', [\App\Http\Controllers\salecontroller::class, 'view'])->name('saleview');
        Route::get('/add', [\App\Http\Controllers\salecontroller::class, 'add'])->name('addsale');
        Route::get('/edit/{id}', [\App\Http\Controllers\salecontroller::class, 'edit'])->name('editsale');
        Route::post('/save', [\App\Http\Controllers\salecontroller::class, 'save'])->name('savesale');
        Route::post('/update', [\App\Http\Controllers\salecontroller::class, 'update'])->name('updatesale');
        Route::get('/delete{id}', [\App\Http\Controllers\salecontroller::class, 'delete'])->name('deletesale');
        Route::get('/print/{id}', [\App\Http\Controllers\salecontroller::class, 'print'])->name('printsale');
        Route::get('/setting', [\App\Http\Controllers\salecontroller::class, 'setting'])->name('settingsale');
    });
    Route::prefix('delivery')->group(function () {
        Route::get('/view', [\App\Http\Controllers\deliveryController::class, 'view'])->name('deliveryview');
        Route::get('/add', [\App\Http\Controllers\deliveryController::class, 'add'])->name('deliveryadd');
        Route::post('/save', [\App\Http\Controllers\deliveryController::class, 'save'])->name('deliverysave');
    });
    Route::prefix('customer')->group(function () {
        Route::get('/view', [\App\Http\Controllers\CustomerController::class, 'view'])->name('customerview');
        Route::get('/add', [\App\Http\Controllers\CustomerController::class, 'add'])->name('customeradd');
        Route::post('/save', [\App\Http\Controllers\CustomerController::class, 'save'])->name('customersave');
        Route::get('/edit/{id}', [\App\Http\Controllers\CustomerController::class, 'edit'])->name('customeredit');
        Route::get('/delete/{id}', [\App\Http\Controllers\CustomerController::class, 'delete'])->name('customerdelete');
    });
    Route::prefix('stock')->group(function () {
        /**
         * stock setting
         */
        Route::get('/view', [\App\Http\Controllers\StockController::class, 'view'])->name('stockview');
        Route::get('/incomestockmn', [\App\Http\Controllers\StockController::class, 'incomemanual'])->name('IncomeStockmn');
        Route::get('/addincomestockmn', [\App\Http\Controllers\StockController::class, 'addincomemanual'])->name('AddIncomeStockmn');
        Route::post('/saveincomemanual', [\App\Http\Controllers\StockController::class, 'saveincomemanual'])->name('saveincomemanual');
        Route::get('/incomestockmn/{$id}', [\App\Http\Controllers\StockController::class, 'incomemanual'])->name('IncomeStockmnid');
        Route::get('/updatestockmn', [\App\Http\Controllers\StockController::class, 'updatemanual'])->name('updateStockmn');
        Route::get('/deletestockmn', [\App\Http\Controllers\StockController::class, 'deletemanual'])->name('deleteStockmn');
        /**
         * widthdraw stock
         */
        Route::get('/withdrawstockmn', [\App\Http\Controllers\StockController::class, 'widthdraw_view'])->name('widthdraw_manual_view');
        Route::get('/addWithdrawmn', [\App\Http\Controllers\StockController::class, 'addWithdrawmn'])->name('formWidthdrawmn');
        Route::post('/savewithdraw', [\App\Http\Controllers\StockController::class, 'savewithdraw'])->name('savewithdraw');
        Route::get('/setting', [\App\Http\Controllers\stockController::class, 'setting'])->name('stockSetting');
        Route::post('/startingStock', [\App\Http\Controllers\stockController::class, 'startingStock'])->name('startingStock');
    });
    Route::prefix('acc')->group(function () {
        /**
         * stock setting
         */
        Route::get('/setting', [\App\Http\Controllers\accountingController::class, 'setting'])->name('Accsetting');
        Route::post('/setting', [\App\Http\Controllers\accountingController::class, 'update'])->name('updateaccSetting');
    });
    Route::prefix('master')->group(function () {
        /**
         * Stock Group
         */
        Route::get('/stockGroup', [App\Http\Controllers\StockController::class, 'stockgroup'])->name('group');
        Route::get('/addCatStock', [App\Http\Controllers\StockController::class, 'addCatstock'])->name('addCatstock');
        Route::post('/searchProduct', [\App\Http\Controllers\StockController::class, 'searchProduct'])->name('searchProduct');
        Route::post('/searchCustomer', [\App\Http\Controllers\CustomerController::class, 'searchCustomer'])->name('searchCustomer');
        Route::post('/searchAddress', [\App\Http\Controllers\AddressCustomer::class, 'searchFullAddress'])->name('searchAddress');
        Route::post('/extraProduct', [\App\Http\Controllers\ProductController::class, 'getExtra'])->name('getExtra_product');
        Route::get('/updateCatStock/{id}', [App\Http\Controllers\StockController::class, 'updateCatStock'])->name('updateCatStock');
        Route::post('/updateCatStock', [App\Http\Controllers\StockController::class, 'updateCategoryStock'])->name('updateCategoryStock');
        Route::get('/deleteCatStock/{id}', [App\Http\Controllers\StockController::class, 'deleteCatStock'])->name('deleteCatStock');
        Route::post('/addCatstock', [App\Http\Controllers\StockController::class, 'saveCatstock'])->name('postCatstock');
        Route::get('/stockSetting', [\App\Http\Controllers\StockController::class, 'settingStock'])->name('stockSetting');
        Route::post('/save', [\App\Http\Controllers\StockController::class, 'savesetting'])->name('stocksavesetting');
        /**
         * category type
         */
        Route::get('/option', [App\Http\Controllers\OptionController::class, 'option'])->name('showAllOption');
        Route::post('/get_option', [App\Http\Controllers\OptionController::class, 'get_option_value'])->name('get_options');
        Route::get('/addoption', [App\Http\Controllers\OptionController::class, 'addoption'])->name('addoption');
        Route::get('/updateoption/{id}', [App\Http\Controllers\OptionController::class, 'updateformoption'])->name('updateformoption');
        Route::post('/saveoption', [App\Http\Controllers\OptionController::class, 'saveoption'])->name('saveoption');
        Route::post('/updateoption', [App\Http\Controllers\OptionController::class, 'updateoption'])->name('updateoption');
        Route::get('/deleteOption/{id}', [App\Http\Controllers\OptionController::class, 'deleteOption'])->name('deleteoption');
        /**
         * Stock Product
         */
        Route::get('/stockProduct', [App\Http\Controllers\ProductController::class, 'product'])->name('product');
        Route::get('/formProduct', [App\Http\Controllers\ProductController::class, 'FormaddProduct'])->name('addProduct');
        Route::get('/formProduct/{id}', [App\Http\Controllers\ProductController::class, 'unitProduct'])->name('editProduct');
        Route::post('/updateProduct', [App\Http\Controllers\ProductController::class, 'updateProduct'])->name('updateProduct');
        Route::get('/deleteProduct/{id}', [App\Http\Controllers\ProductController::class, 'DeleteProduct'])->name('deleteProduct');
        Route::post('/saveProduct', [App\Http\Controllers\ProductController::class, 'saveProduct'])->name('saveProduct');
        /**
         * Stock Setup
         */
        Route::get('/settingStock', [App\Http\Controllers\StockController::class, 'showSetting'])->name('setupStock');
    });
    Route::prefix('employee')->group(function () {
        /**
         * Category Employee
         */
        Route::get('/catEmployee', [App\Http\Controllers\EmployeeController::class, 'catEmployee'])->name('catEmployee');
        Route::get('/addCatEmployee', [App\Http\Controllers\EmployeeController::class, 'addcatEmployee'])->name('addcatEmployee');
        Route::get('/editCatEmployee/{id}', [App\Http\Controllers\EmployeeController::class, 'editcatEmployee'])->name('editcatEmployee');
        Route::post('/editCatEmployee', [App\Http\Controllers\EmployeeController::class, 'updatecatEmployee'])->name('updatecatEmployee');
        Route::post('/catEmployee', [App\Http\Controllers\EmployeeController::class, 'saveCatEmployee'])->name('saveEmployee');
        Route::get('/deleteCatEmployee/{id}', [\App\Http\Controllers\EmployeeController::class, 'deleteCatEmployee'])->name('deleteCatEmployee');
        /**
         * Employee
         */
        Route::get('/employee', [App\Http\Controllers\EmployeeController::class, 'employee'])->name('employee');
        Route::get('/formemployee', [App\Http\Controllers\EmployeeController::class, 'formemployee'])->name('formemployee');
        Route::get('/formemployee/{id}', [App\Http\Controllers\EmployeeController::class, 'editEmployee'])->name('editEmployee');
        Route::post('/employee', [App\Http\Controllers\EmployeeController::class, 'saveEmployee'])->name('addemployee');
        Route::post('/updateEmployee', [\App\Http\Controllers\EmployeeController::class, 'updateEnployee'])->name('updateEmployee');
        Route::get('/deleteEmployee/{id}', [App\Http\Controllers\EmployeeController::class, 'deleteEmployee'])->name('deleteEmployee');
    });
    Route::prefix('report')->group(function () {
        Route::get('/customers', [App\Http\Controllers\ReportController::class, 'customer'])->name('customerReport');
        Route::get('/invoices', [App\Http\Controllers\ReportController::class, 'invoice'])->name('invoiceReport');
        Route::get('/export/{type}', [\App\Http\Controllers\ReportController::class, 'export'])->name('invoiceExport');
    });
});
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});
