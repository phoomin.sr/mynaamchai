<?php

namespace Database\Factories;

use App\Models\usermeta;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsermetaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = usermeta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
