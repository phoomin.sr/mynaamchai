<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            /**
             * notifications table note
             *
             * type
             * 1 => message
             * 2 => link
             * 3 => picture
             * 4 => location
             * 5 => authenticate
             * 6 => bank
             *
             * status
             * 1 => create
             * 2 => read
             *
             */
            $table->id();
            $table->integer('type');
            $table->unsignedBigInteger('from');
            $table->string('header');
            $table->unsignedBigInteger('to');
            $table->integer('status');
            $table->integer('company_id');
            $table->timestamps();

            $table->foreign('from')->references('id')->on('employees');
            $table->foreign('to')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
