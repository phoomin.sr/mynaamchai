<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('customer_no')->nullable();
            $table->string('customer_trade_no')->nullable();
            $table->tinyInteger('business_type');
            $table->string('customer_name');
            $table->string('customer_taxid')->nullable();
            $table->unsignedBigInteger('customer_group')->nullable();
            $table->text('customer_address_no');
            $table->text('customer_address');
            $table->string('email')->nullable();
            $table->string('phone_no');
            $table->tinyInteger('payment_method')->nullable();
            $table->integer('credit_day')->nullable();
            $table->tinyinteger('placeBill_method')->nullable();
            $table->tinyInteger('isBranch')->default(1);
            $table->string('branch_no')->default('00000');
            $table->tinyInteger('default_vat_type')->default(1);
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('saletacker_id')->nullable();
            $table->timestamps();

            $table->foreign('customer_group')->references('id')->on('customer_group');
            $table->foreign('company_id')->references('id')->on('company');
            $table->foreign('saletacker_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
