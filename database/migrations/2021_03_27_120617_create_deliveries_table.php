<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->id();
            $table->dateTime('docdate');
            $table->dateTime('duedate');
            $table->unsignedBigInteger('ref_invoice_id');
            $table->decimal('novat', 10, 2);
            $table->tinyInteger('vattype');
            $table->unsignedBigInteger('company_id');
            $table->timestamps();

            $table->foreign('ref_invoice_id')->references('id')->on('invoices');
            $table->foreign('company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
