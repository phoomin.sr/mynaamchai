<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddDetailStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_dt_stock', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('addId');
            $table->unsignedBigInteger('inv_id');
            $table->string('unit_id');
            $table->integer('pack');
            $table->decimal('unit_price', 10, 2);

            $table->foreign('addId')->references('id')->on('add_stock');
            $table->foreign('inv_id')->references('id')->on('inv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_detail_stock');
    }
}
