<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('addId');
            $table->unsignedBigInteger('invId');
            $table->integer('unit_id');
            $table->integer('pack_id');
            $table->decimal('qty', 10, 2);
            $table->decimal('unitprice', 10, 2);
            $table->string('lot')->nullable(true);
            $table->string('location')->nullable(true);
            $table->unsignedBigInteger('withdraw_id')->nullable(true);
            $table->unsignedBigInteger('business_id');

            $table->foreign('addId')->references('id')->on('add_dt_stock');
            $table->foreign('invId')->references('id')->on('inv');
            $table->foreign('withdraw_id')->references('id')->on('withdraw');
            $table->foreign('business_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
