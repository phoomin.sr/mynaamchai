<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryDtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_dt', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invoice_dt_id');
            $table->integer('unit_qty');
            $table->decimal('unit_price', 10, 2);
            $table->decimal('discount', 10, 2);

            $table->foreign('invoice_dt_id')->references('id')->on('invoice_dt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_dt');
    }
}
