<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('business_name');
            $table->integer('image_id')->nullable();
            $table->integer('business_type');
            $table->integer('wh_tax');
            $table->string('trade_no');
            $table->string('tax_id')->nullable();
            $table->tinyInteger('isBranch')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('official_tel')->nullable();
            $table->string('phonenumber')->nullable();
            $table->string('fax')->nullable();
            $table->string('website')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
