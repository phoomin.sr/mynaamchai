function subcal(price, qty, subtotal) {
    outline = price.val() * qty.val();
    subtotal.val(outline);
}
function cal(vat = 7) {
    subtotal = $('#sub_total').val();
    vat_type = $('#vattype').val();

    discount = $('#discount_baht').val();
    gtt = subtotal - discount;

    switch (vat_type) {
        case 'vat':
            vat = vat / 100
            Onlyvat = gtt * vat;
            gtt += Onlyvat;
            break;
        case 'include_vat':
            // vat = gtt * 7 / 107
            Onlyvat = gtt * 7 / 107;
            $('#sub_total').val((gtt -= Onlyvat).toFixed(2));
            gtt += Onlyvat;
            break;
        default:
            vat = 0;
            Onlyvat = 0;
            break;
    }

    $('#vatres').val(Onlyvat.toFixed(2));
    $('#sum_vat').val(gtt.toFixed(2));
    $('#gtt').val(gtt.toFixed(2));
}
