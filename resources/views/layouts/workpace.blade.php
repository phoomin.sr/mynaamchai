<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script> --}}

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script> --}}
    <link href="{{ asset('fontawesome-free-5.15.1-web/css/all.css') }}" rel="stylesheet">
    <script src="{{ asset('/fontawesome-free-5.15.1-web/js/all.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/xcash/bootstrap-autocomplete@v2.3.7/dist/latest/bootstrap-autocomplete.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <link rel="stylesheet" href="{{ asset('datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('datepicker/css/bootstrap-datepicker.standalone.css') }}">
    <link rel="stylesheet" href="{{ asset('datepicker/css/bootstrap-datepicker3.css') }}">
    <script src="{{ asset('datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('datepicker/locales/bootstrap-datepicker.th.min.js') }}"></script>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.rtl.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.rtl.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.rtl.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.rtl.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/i18n/defaults-th_TH.min.js" integrity="sha512-4+67DNUFdJSmwICorHH5XI9W9M0RYk2tICC5rz/0bG+y3aa+CoRk9yGDfm7xFZz2eAbPbUKnlIu3e8fZM5vRYw==" crossorigin="anonymous"></script>
    @yield('style')
</head>
<body class="bg-light min-vh-100">
<div class="container bg-white mt-2">
    <header>
        <div class="row pt-3">
            <div class="col-2 text-center">
                <a href="{{ route($back) }}">
                    <h1><i class="fas fa-chevron-left"></i></h1>
                </a>
            </div>
            <div class="col">
                <h1>{{ $title }}</h1>
            </div>
        </div>
    </header>
    <hr class="divider">
    <main class="py-4">
        @yield('content')
        <div class="row">
            <div class="col">
                @yield('pagination')
            </div>
        </div>
    </main>
</div>
    <script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    $(document).ready(function () {
        $.valHooks.input = {
            set: function (name, value) {
                if ($(name).attr('type') == "hidden") {
                    console.log($(name).attr('id'));
                    if ($('#txt_' + $(name).attr('id')).length > 0) {
                        var txt = $('#txt_' + $(name).attr('id'));
                        value = parseFloat(value.replace(/,/, ''));
                        $(name).attr('value', value);
                        txt.attr('value', numberWithCommas(value));
                    }
                }
            }
        };
        function numberWithCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }

            return x1 + x2;
        }
        $('[name^="txt_"]').keyup(function () {
            id = $(this).attr('id');
            value = $(this).val().replace(/,/, '');
            console.log(id.replace('txt_', ''));
            $('#' + id.replace('txt_', '')).val(value);
            $(this).val(numberWithCommas(value));
            $('#' + id.replace('txt_', '')).trigger('change');
        });
        $(function () {
            $('select').selectpicker();
            $('input[type=text]').attr('autocomplete', 'off');
        });
        var submit = $('[type=submit]');

        $('a.delete, button.delete').click(function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            alertify.confirm("เมื่อลบรายการแล้วจะไม่สามารถกู้คืนได้ ยืนยันการลบรายการ",
            function(){
                alertify.success('ลบรายการสำเร็จ');
                window.location.replace(url);
            },
            function(){
                alertify.error('ยกเลิก');
            }).set({'title':'ลบรายการ'});
        });
        $('[name=clear_filter]').click(function () {
            cUrl = window.location.origin + window.location.pathname;
            window.location.replace(cUrl);
        });
        $('form').submit(function (e) {
            e.preventDefault();
            status = window['validate_'+$(this).attr('name')]();
            if (status == 'true') {
                var data = $(this).serializeArray();
                $.ajaxSetup({
                    method: 'POST',
                    header: {
                        'Content-Type': "text/html; charset=UTF-8"
                    },
                    url: $(this).attr('action')
                });

                window['ajax_'+$(this).attr('name')](data);
            } else {
                reponse_error_require_form(status)
            }
        });
    });
        function reponse_error_require_form(status)
        {
            alert = status.split(',');
            for (let i = 0;i < alert.length;i++) {
                $('label[for="'+alert[i]+'"]').after("<span class='text-danger'>* require</span>");
            }
        }
        function valid_error(ele_name, status, error = "ซ้ำ", success = "สามาสรถใช้งานได้")
        {
            label_text = $('label[for="'+ele_name+'"]').text();
            if (status) {
                if ($('label[for="'+ele_name+'"]').next().is('SPAN')) {
                    $('label[for="'+ele_name+'"]').next().remove();
                }
                $('label[for="'+ele_name+'"]').after('<span class="text-success" valid="true">'+ label_text + success +'</span>');
            } else {
                if ($('label[for="'+ele_name+'"]').next().is('SPAN')) {
                    $('label[for="'+ele_name+'"]').next().remove();
                }
                $('label[for="'+ele_name+'"]').after('<span class="text-danger" valid="false">'+ label_text + error +'</span>');
            }
        }
        function formsubmit_lock(status)
        {
            if (status == 'true') {
                $('[type=submit]').attr('disabled', false).removeClass('disabled');
            } else {
                $('[type=submit]').attr('disabled', true).addClass('disabled');
            }
        }
        function send(data, res) {
            wait_process(true);
            $.ajax({
                'data': data
            }).done(function(response) {
                res(response);
                wait_process(false);
            });
        }
        async function sending(data, url, method = "POST") {
            // Default options are marked with *
            fetchHead = {
                    method: method, // *GET, POST, PUT, DELETE, etc.
                    mode: 'cors', // no-cors, *cors, same-origin
                    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                    credentials: 'same-origin', // include, *same-origin, omit
                    headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    redirect: 'follow', // manual, *follow, error
                    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                };
            if (method !== "GET") {
                data._token = $('input[type=hidden][name=_token]').val()
                fetchHead.body = JSON.stringify(data) // body data type must match "Content-Type" header
            } else {
                $(data).each(function (key, value) {
                    url += key + "=" + value
                });
            }

            const response = await fetch(url, fetchHead);
            return response.json(); // parses JSON response into native JavaScript objects
        }
        async function fetchGet(data, url) {
            return sending(data, url, 'GET');
        }
        function select_option_byval(select, mergedata = {}){
            ChooseFind = {class:'.', id:'#'}
            data = {
                ChooseFind: {
                    class: '.',
                    id:'#'
                },
                prefix: 'sel',
                findFrom: 'class'
            };

            for(const [key, value] of Object.entries(mergedata)){
                data[key] = value;
            }

            if (data.default !== undefined) {
                $('div[class^="' + data.prefix + '-"]').hide();
                $('div' + data.ChooseFind[data.findFrom] + data.prefix + '-' + data.default).show();
            }

            select.change(function () {
                $('div[class^="' + data.prefix + '-"]').hide();
                $('div' + data.ChooseFind[data.findFrom] + data.prefix + '-' + $(this).val()).show();
            });
        }
        function wait_process(event) {
            if (event) {
                var wait_button = '<button class="btn btn-primary wait_process" type="button" disabled>'
                            + '<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>'
                            + 'Loading...'
                            + '</button>';

                $('input[type=submit]').after(wait_button);
                $('input[type=submit]').addClass('d-none');
            } else {
                $('input[type=submit]').removeClass('d-none');
                $('button.wait_process').remove();
            }
        }
        function dateElem(ele, option = {}) {
            var options = Object.assign({
                clearBtn: true,
                language: "th",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            }, option);

            ele.datepicker(options);

            if (ele.val().length > 0) {
                date = new Date(ele.val());
                ele.datepicker('setDate', date);
            } else {
                date = new Date();
                ele.datepicker('setDate', date);
            }
        }
        function addDate(from, countto)
        {
            var date_from = from.split('/');
            var now = new Date(date_from[2], date_from[1] - 1, date_from[0]);
            now.setDate(now.getDate() + countto)

            return now;
        }
        function searchbox(txtEl, url, onComplete = function (env, item) { return; }, externalData = function (data) { return data; }, callback = function (env, item) { return; })
        {
            txtEl.autoComplete({
                resover: 'custom',
                autoFill: true,
                events:{
                    search: function (qry, callback) {
                        data = {
                                '_token': $('input[type=hidden][name=_token]').val(),
                                'f': qry
                            };
                        data = externalData(data);
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: data
                        }).done(function (res) {
                            callback(res);
                        });
                    },
                }
            });

            txtEl.on('autocomplete.select', function (env, item) {
                onComplete(env, item);
            });
        }
    </script>
    @yield('script')
</body>
</html>
