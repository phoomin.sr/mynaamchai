<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script> --}}

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script> --}}
    <link href="{{ asset('fontawesome-free-5.15.1-web/css/all.css') }}" rel="stylesheet">
    <script src="{{ asset('/fontawesome-free-5.15.1-web/js/all.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.rtl.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.rtl.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.rtl.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.rtl.min.css"/>
    @yield('style')
    <style>
        body {
            background: rgb(2,0,36);
            background: -moz-linear-gradient(332deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%);
            background: -webkit-linear-gradient(332deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%);
            background: linear-gradient(332deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#020024",endColorstr="#00d4ff",GradientType=1);
            background-repeat: no-repeat;
            height: 100%;
        }
        a {
            text-decoration: none;
        }
    </style>
</head>
<body class="bg-light min-vh-100">
    @if (Session::has('name'))
        <nav class="navbar navbar-expand-sm fixed-top bg-dark navbar-dark shadow">
            <!-- Brand -->
            <a class="navbar-brand" href="#">{{ $appName ?? '' }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropleft">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            <i class="fas fa-envelope"></i> Inbox
                        </a>
                        <div class="dropdown-menu" style="min-width: 20rem">
                            <div class="p-2">
                                <span class="font-weight-bold">notification</span>
                                <div class="bg-light p-1" style="min-height: 2rem">
                                    <div class="rounded bg-white p-2">
                                        <div class="row">
                                            <div class="align-middle col-1"><i class="fas fa-comment-dots text-primary"></i></div>
                                            <div class="align-middle col">
                                                <span class="font-weight-bold">min</span>
                                            </div>
                                            <div class="col-3">
                                                <button type="button" class="btn btn-outline-light btn-sm"><i class="fas fa-times text-danger"></i></button>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <span><small>message from systems.</small></span>
                                        <div class="dropdown-divider"></div>
                                        <div class="text-right">
                                            {{-- button group --}}
                                            <div class="btn-group btn-group-sm" role="group">
                                                <button type="button" class="btn btn-outline-light"><i class="fas fa-eye text-warning"></i></button>
                                                {{-- <i class="fas fa-eye-slash"></i> --}}
                                                <button type="button" class="btn btn-outline-light"><i class="far fa-window-restore text-primary"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="p-2">
                                <span class="font-weight-bold">message</span>
                                <div class="bg-light p-1" style="min-height: 2rem">
                                    <div class="rounded bg-white p-2">
                                        <div class="row">
                                            <div class="align-middle col-1"><i class="fas fa-comment-dots text-primary"></i></div>
                                            <div class="align-middle col">
                                                <span class="font-weight-bold">min</span>
                                            </div>
                                            <div class="col-3">
                                                <button type="button" class="btn btn-outline-light btn-sm"><i class="fas fa-times text-danger"></i></button>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <span><small>message from systems.</small></span>
                                        <div class="dropdown-divider"></div>
                                        <div class="text-right">
                                            {{-- button group --}}
                                            <div class="btn-group btn-group-sm" role="group">
                                                <button type="button" class="btn btn-outline-light"><i class="fas fa-eye text-warning"></i></button>
                                                {{-- <i class="fas fa-eye-slash"></i> --}}
                                                <button type="button" class="btn btn-outline-light"><i class="far fa-window-restore text-primary"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropleft">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        <i class="fas fa-user"></i> {{ Session::get('name') }}
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('profile') }}"><i class="fas fa-user"></i> Profile</a>
                            <a class="dropdown-item" href="{{ route('setting') }}"><i class="fas fa-user-cog"></i> Setting</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('signout') }}"><i class="fas fa-sign-out-alt"></i> sign out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    @endif
    <main class="py-4 mt-5">
        @yield('content')
    </main>
    @if (Session::has('name'))
        <div class="bott-navigator d-block d-sm-none">
            <ul class="nav nav-pills nav-fill bg-dark text-white fixed-bottom">
                <li class="nav-item">
                    <a class="nav-link" href="#">Back</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Favorite</a>
                </li>
            </ul>
        </div>
    @endif
    <script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $(document).ready(function () {
        var submit = $('[type=submit]');
        $('form').submit(function (e) {
            e.preventDefault();
            status = window['validate_' + $(this).attr('name')]();
            if (status == "true") {
                var data = $(this).serializeArray();
                $.ajaxSetup({
                    method: 'POST',
                    header: {
                        'Content-Type': "text/html; charset=UTF-8"
                    },
                    url: $(this).attr('action')
                });

                window['ajax_'+$(this).attr('name')](data);
            } else {
                reponse_error_require_form(status);
            }
            });
        });
        function send(data, res) {
            wait_process(true);
            $.ajax({
                'data': data
            }).done(function(response) {
                res(response);
                wait_process(false);
            });
        }
        function wait_process(status) {
            if (status) {
                var wait_button = '<button class="btn btn-primary wait_process" type="button" disabled>'
                            + '<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>'
                            + 'Loading...'
                            + '</button>';

                $('input[type=submit]').after(wait_button);
                $('input[type=submit]').addClass('d-none');
            } else {
                $('button.wait_process').remove();
                $('input[type=submit]').removeClass('d-none');
            }
        }
        function reponse_error_require_form(status)
        {
            for (let i = 0;i < status.length;i++) {
                if (status[i]['msg'] !== '') {
                    msg = status[i]['msg'];
                } else {
                    msg = '';
                }
                $('label[name="'+status[i]['name']+'"]').after("<span>*"+msg+"</span>");
            }
        }
    </script>
    @yield('script')
</body>
</html>
