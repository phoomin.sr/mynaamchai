@extends('layouts.workpace')

@section('contents')
    <form action="{{ route('posave') }}" name="po" method="post">
        <div class="row">
            <div class="col">
                <table class="table table-responsive-sm table-striped">
                    <tr class="table-warning">
                        <th>#</th>
                        <th>รายการ</th>
                        <th>หน่วย</th>
                        <th>จำนวน</th>
                        <th>ราคา</th>
                        <th>รวมราคา</th>
                    </tr>
                </table>
            </div>
        </div>
    </form>
@endsection
