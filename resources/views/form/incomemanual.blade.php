@extends('layouts.workpace')

@section('content')
    <div class="container">
        <form action="{{ $action }}" name="manual_add" method="post">
            @csrf
            <div class="text-right">
                <div class="row mb-2">
                    <div class="col-5">
                        <label for="recieved_date" class="form-label">วันที่</label>
                    </div>
                    <div class="col">
                        <div class="input-group date">
                            <input name="recieved_date" type="text" class="form-control" placeholder="วันที่รับ">
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-5">
                        <label for="recieved_type" class="form-label">ประเภทการรับ</label>
                    </div>
                    <div class="col">
                        <select name="recieved_type" id="recieved_type" class="form-control">
                            @foreach ($recieved_type as $key => $item)
                                <option value="{{ $key }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row text-left mb-3">
                    <div class="col">
                        <h3>รายการ</h3>
                        <table class="table table-sm table-striped table-hover">
                            <thead class="text-center">
                                <tr>
                                    <th>สินค้าคงเหลือ</th>
                                    <th>หน่วย</th>
                                    <th width='10%'>จำนวน</th>
                                    <th width='10%'>ราคา</th>
                                    <th width='10%'>ราคา/หน่วย</th>
                                    <th width='20%'>ที่เก็บ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for ($i = 0; $i < $count; $i++)
                                    <tr>
                                        <td>
                                            <input type="search" name="product[]" id="product" class="form-control" autocomplete="off">
                                            <input type="hidden" name="product_id[]">
                                        </td>
                                        <td>
                                            <input type="text" name="unit[]" id="unit_{{ $i }}" class="form-control" readonly>
                                            <input type="hidden" name="unit_id" value="">
                                        </td>
                                        <td>
                                            <input type="text" name="txt_quantity[]" id="txt_quantity_{{ $i }}" class="form-control">
                                            <input type="hidden" name="quantity[]" id="quantity_{{ $i }}" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" name="txt_price[]" id="txt_price_{{ $i }}" class="form-control">
                                            <input type="hidden" name="price[]" id="price_{{ $i }}" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" name="txt_unit_price[]" id="txt_unit_price_{{ $i }}" class="form-control" readonly>
                                            <input type="hidden" name="unit_price[]" id="unit_price_{{ $i }}" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <select name="stock[]" id="stock" class="form-control">
                                                @foreach ($stk ?? [] as $key => $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                @endfor
                                <tr>
                                    <td colspan="6">
                                        <input id="addBut" type="button" value="+ เพิ่ม" class="btn btn-block btn-warning">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="text-right">
                    <input type="submit" value="บันทึก" class="btn btn-block btn-primary">
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        function validate_manual_add() {
            return true;
        }
        function ajax_manual_add(data){
            send(data, function (res) {
                if (res.add_begin) {
                    alertify.notify(res.msg, 'success', 3, function(){
                        window.location.replace(res.redirect_to);
                    });
                } else {
                    alertify.notify(response.msg, 'error', 5);
                }
            });
        }
        $('[name="product[]"]').autoComplete({
            resover: 'custom',
            autoFill: true,
            events:{
                search: function (qry, callback) {
                    $.ajax({
                        method: 'POST',
                        url: '{{ route("searchProduct") }}',
                        data: {
                            '_token': $('input[type=hidden][name=_token]').val(),
                            'f': qry
                        }
                    }).done(function (res) {
                        callback(res);
                    });
                }
            }
        });
        $(document).ready(function () {
            dateElem($('input[name="recieved_date"]'));
            $('[name="product[]"]').on('autocomplete.select', function (event, item) {
                let i = $('[name="product[]"]').index($(this));
                $('[name="product_id[]"]').eq(i).val(item.value);

                $.ajax({
                    method: 'POST',
                    url: '{{ route("getExtra_product") }}',
                    data: {
                        '_token': $('input[type=hidden][name=_token]').val(),
                        'id': item['value']
                    }
                }).done(function (res) {
                    $('[name="unit[]"]').eq(i).val(res.extra[0].name_option_dt);
                    $('[name="price[]"]').eq(i).val(res.extra[0].product_sale_price);
                });
            });
            $('#addBut').click(function () {
                cloneEle = $('table tbody tr').first().clone();
                cloneEle.find('input').val('');

                $('table tbody tr').last().before(cloneEle);
            });
            $('[name="quantity[]"]').change(function () {
                let i = $('[name="quantity[]"]').index($(this));
                if ($('[name="price[]"]').eq(i).val() !== "") {
                   unit_price = $(this).val() / $('[name="price[]"]').eq(i).val();
                   $('[name="unit_price[]"]').eq(i).val(unit_price.toFixed(2));
                }
            });
            $('[name="price[]"]').change(function () {
                let i = $('[name="price[]"]').index($(this));
                if ($('[name="quantity[]"]').eq(i).val() !== "") {
                    unit_price = $(this).val() / $('[name="quantity[]"]').eq(i).val();
                   $('[name="unit_price[]"]').eq(i).val(unit_price.toFixed(2));
                }
            });
        });
    </script>
@endsection
