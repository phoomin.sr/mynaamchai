@extends('layouts.workpace')

@section('content')
<div class="container">
    <form action="{{ $route }}" name="employee">
        @csrf
        <div class="row mb-5">
            <div class="col">
                <label for="position_group" class="form-label">รหัสกลุ่ม</label>
                <input type="text" name="position_group" id="position_group" class="form-control" value="{{ $info['slug'] ?? '' }}">
            </div>
            <div class="col">
                <label for="position_name">ชื่อตำแหน่ง</label>
                <input type="text" name="position_name" id="position_name" class="form-control" value="{{ $info['position_name'] ?? '' }}">
            </div>
        </div>
        <div class="row">
            <h3 class="mb-3">สิทธิ์การเข้าถึง</h3>
            <div class="table">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อ/กลุ่ม</th>
                            <th>สิทธิ์การเข้าถึง</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($menu as $key => $items)
                                <tr class="table-active">
                                    <td colspan="3">{{ $key }}</td>
                                </tr>
                            @foreach ($items as $name => $item)
                            {{-- {{dd($item, $permission, $info['menu'][$item['route_name']])}} --}}
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>
                                        {{ $name }}
                                        <input type="hidden" name="route_name[]" value="{{ $item['route_name'] }}">
                                    </td>
                                    <td>
                                        <select name="permission[]" class="form-control">
                                            @foreach ($permission as $key => $permission_item)
                                                <option value="{{ $key }}"
                                                @isset ($infomenu)
                                                    @if (isset($infomenu[$item['route_name']]))
                                                        @if ($infomenu[$item['route_name']] == $key)
                                                            selected
                                                        @endif
                                                    @endif
                                                @endisset
                                                >{{ $permission_item }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-right">
            @if (isset($id))
                <input type="hidden" name="where_id" value="{{ $id }}">
            @endif
            <input type="submit" value="บันทึก" class="btn btn-primary">
            <input type="reset" value="ยกเลิก" class="btn btn-danger">
        </div>
    </form>
</div>
@endsection
@section('script')
    <script>
        function validate_employee () {
            status = true;
            unit_status = [];
            unit_status.push($('#position_group').val().length > 0);
            unit_status.push($('#position_name').val().length > 0);

            for (let i = 0;i < unit_status.length;i++) {
                if (!unit_status[i]) {
                    status = false;
                }
            }

            return status;
        }
        function ajax_employee(data) {
            send(data, function (res) {
            if (res.catemp) {
                alertify.notify(res.msg, 'success', 5, function(){
                    window.location.replace(res.redirect_to);
                 });
            } else {
                alertify.notify(res.msg, 'error', 5);
            }
        });
        }
    </script>
@endsection
