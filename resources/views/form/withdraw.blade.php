@extends('layouts.workpace')

@section('content')
    <div class="conatiner">
        <form name="withdraw_form" action="{{ route('savewithdraw') }}" method="post">
            @csrf
            <div class="row mb-3">
                <div class="col">
                    <label for="wd-type" class="form-label">ประเภทการเบิก</label>
                    <select name="withdraw_type" id="wd-type" class="form-control">
                        @foreach ($withdraw_type as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <label for="wd-for" class="form-label">จุดประสงค์การเบิก</label>
                    <select name="wd_for" id="wd-for" class="form-control">
                        @foreach ($withdraw_for as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-2">
                    <label for="doc-no" class="form-label">เลขที่เอกสาร</label>
                    <input type="search" name="doc_no" id="doc-no" class="form-control" placeholder="ค้นหาเลขที่เอกสาร">
                    <input type="hidden" name='refId' id='refId' value="">
                </div>
                <div class="col-2">
                    <label for="datetime-doc" class="form-label">วันที่เอกสาร</label>
                    <input type="text" name="datetime_doc" id="datetime-doc" class="form-control">
                </div>
            </div>

            {{-- customer panal --}}
            <div class="sel-1">
                <div class="row mb-3">
                    <div class="col">
                        <label for="customer-code" class="form-label">รหัสลูกค้า</label>
                        <input type="search" name="customer_code" id="customer-code" class="form-control">
                    </div>
                    <div class="col">
                        <label for="customer-name" class="form-label">ชื่อลูกค้า</label>
                        <input type="search" name="customer_name" id="customer-name" class="form-control">
                    </div>
                    <div class="col">
                        <label for="customer-tax-id" class="form-label">รหัสประจำตัวผู้เสียภาษี</label>
                        <input type="text" name="customer_tax_id" id="customer-tax-id" class="form-control">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="customer-address" class="form-label">ที่อยู่ลูกค้า</label>
                        <textarea name="customer_address" id="customer-address" class="form-control"></textarea>
                        <input type="hidden" name="customer_id" id="customer-id">
                    </div>
                </div>
            </div>

            {{-- supplier panal --}}
            <div class="sel-2">
                <div class="row mb-3">
                    <div class="col">
                        <label for="supplier-code" class="form-label">รหัสคู่ค้า</label>
                        <input type="search" name="supplier_code" id="supplier-code" class="form-control">
                    </div>
                    <div class="col">
                        <label for="supplier-name" class="form-label">ชื่อคู่ค้า</label>
                        <input type="search" name="supplier_name" id="supplier-name" class="form-control">
                    </div>
                    <div class="col">
                        <label for="supplier-tax-id" class="form-label">รหัสประจำตัวผู้เสียภาษี</label>
                        <input type="text" name="supplier_tax_id" id="supplier-tax-id" class="form-control">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="supplier-address" class="form-label">ที่อยู่คู่ค้า</label>
                        <textarea name="supplier_address" id="supplier-address" class="form-control"></textarea>
                        <input type="hidden" name="supplier_id" id="supplier-id">
                    </div>
                </div>
            </div>

            <p class="h2">รายการเบิก</p>
            <div class="row">
                <div class="col">
                    <table class="table table-responsive-sm table-striped">
                        <tr class="table-warning text-center">
                            <th>สินค้า</th>
                            <th>หน่วย</th>
                            <th>จำนวน</th>
                        </tr>
                        <tr>
                            @for ($i = 0; $i < max(10, count($infos)); $i++)
                                <tr>
                                    <td>
                                        <input type="search" class="form-control" name="product_search[]" id="product_search_{{ $i }}">
                                        <input type="hidden" name="product[]">
                                    </td>
                                    {{-- <td>
                                        <input type="text" class="form-control" name="search-job" id="search-job" placeholder="ค้นหาใบงาน">
                                        <input type="hidden" name="job">
                                    </td> --}}
                                    <td>
                                        <input type="text" class="form-control" name="unit_pack[]" id="unit_pack" readonly>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="unit_qty[]" id="unit-qty">
                                    </td>
                                </tr>
                            @endfor
                        </tr>
                    </table>
                </div>
            </div>
            <input type="submit" value="บันทึก" class="btn btn-block btn-primary">
        </form>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            dateElem($('#datetime-doc'));
            searchbox($('#doc-no'), '{{ route("search") }}', function(env, item) {
                sending({
                    id: item.value.id,
                    type: item.type
                }, '{{ route("get_extra_item") }}')
                .then(function (response) {
                    console.log(response);
                    customer_info = response.info.customer_info;
                    infos = response.infos;
                    info = response.info;

                    $('#refId').val(info[0]['id']);
                    $('#customer-code').val(customer_info[0]['customer_no']);
                    $('#customer-name').val(customer_info[0]['customer_name']);
                    $('#customer-tax-id').val(customer_info[0]['customer_taxid']);
                    $('#customer-id').val(customer_info[0]['id']);
                    $('#customer-tax-id').val(customer_info[0]['customer_taxid']);
                    $('#customer-address').val(customer_info[0]['customer_address']);

                    for (let i = 0;i < infos.length;i++) {
                        $('#product_search_' + i).val(infos[i]['name']);
                        $('[name="product[]"]').eq(i).val(infos[i]['product_id']);
                        $('[name="unit_pack[]"]').eq(i).val(infos[i]['name_option_dt']);
                        $('[name="unit_qty[]"]').eq(i).val(infos[i]['unit_qty']);
                    }
                })
                .catch(function (err) {
                    console.log(err);
                });
            }, function (data) {
                data.type = $('#wd-for').val();
                 return data;
            });
            searchbox($('#customer-code'), '{{ route("searchCustomer") }}', function (env, item) {
                let info = JSON.parse(item.value);
                $('#customer-code').val(info.customer_no);
                $('#customer-name').val(info.customer_name);
                $('#customer-tax-id').val(info.customer_taxid);
                $('#customer-address').val(info.customer_address_no + ' ' + info.customer_address);
                $('#customer-id').val(info.id);
            });
            searchbox($('#customer-name'), '{{ route("searchCustomer") }}', function (env, item) {
                let info = JSON.parse(item.value);
                $('#customer-code').val(info.customer_no);
                $('#customer-name').val(info.customer_name);
                $('#customer-tax-id').val(info.customer_taxid);
                $('#customer-address').val(info.customer_address_no + ' ' + info.customer_address);
                $('#customer-id').val(info.id);
            });
            searchbox($('[name=product_search]'), '{{ route("searchProduct") }}', function (env, item) {
                currentId = $('[name=product_search]').index($(env.currentTarget));
                $('[name=product]').eq(currentId).val(item.value);
                sending({
                    id: item.value
                }, '{{ route("getExtra_product") }}')
                .then(res => {
                    let Extra = res.extra;
                    $('[name=unit_pack]').eq(currentId).val(Extra.name_option_dt);
                })
                .catch(err => {
                    console.log(err);
                });
            });
            select_option_byval($('#wd-for'), {
                default: "1"
            });
        });

        function validate_withdraw_form() {
            return true;
        }
        function ajax_withdraw_form(data) {
            send(data, function(response) {
                if (response.save) {
                    alertify.notify(response.msg, 'success', 3, function(){
                        window.location.replace(response.redirect_to);
                    });
                } else {
                    alertify.notify(response.msg, 'error', 3, function(){
                        window.location.replace(response.redirect_to);
                    });
                }
            });
        }
    </script>
@endsection
