@extends('layouts.workpace')

@section('content')
<div class="container">
    <div class="w-100 mb-3">
        <div class="mb-2">
            <input type="search" name="search" id="search" class="form-control">
        </div>
        <div class="row">
            <div class="col-3">
                <a class="btn btn-outline-info" href="{{ route('group') }}">clear filter</a>
            </div>
        </div>
    </div>

    <div class="row">
            <a href="{{ route('formemployee') }}" class="btn btn-warning btn-block">+ เพิ่ม</a>
    </div>
    <table class="table table-hover">
        <thead class='text-center'>
            <tr>
                <td style="width: 5%">#</td>
                <td style="width: 5%">แก้ไข</td>
                <td style="width: 5%">ตำแหน่ง</td>
                <td style="width: 25%">ชื่อ-นามสกุล</td>
                <td style="width: 15%">ลบ</td>
                <td style="width: 15%">วันที่สร้าง</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($employee as $key => $item)
                <tr class="text-center">
                    <td>{{ $key + 1 }}</td>
                    <td><a href="{{ route('editEmployee', ['id'=>$item->id]) }}"><i class="fas fa-edit"></i></a></td>
                    <td>{{ $position[$item->position] }}</td>
                    <td>{{ $item->firstname . '  ' . $item->lastname }}</td>
                    <td><button class="btn btn-danger delete_user delete" data-url="{{ route('deleteEmployee', ['id'=>$item->id]) }}"><i class="fas fa-trash"></i></button></td>
                    <td>{{ $item->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.delete_user').click(function () {
                $.ajax({
                    method:'GET',
                    url: $(this).data('url')
                }).done(function (response) {
                    if (response.delete) {
                        window.location.reload();
                    } else {
                        alertify.notify(response.msg, 'error', 5);
                    }
                });
            });
        });
    </script>
@endsection
