@extends('layouts.app_print')

@section('style')
    <style>
        @media screen {
            body {
                size: 21.8cm 14.8cm;
                font-size: 14px;
                font-family: sans-serif;
                color: black;
                background-color: slategrey;
                padding: 0.5cm 2cm;
            }
            div.paper {
                padding: 1cm 0.5cm 0.5cm 1.5cm;
            }
            div {
                position: relative;
            }
            .header {
                display: block;
            }
        }
        @media print {
            @page {
                size: 21.8cm 14.8cm;
                margin: 1.2cm 0.5cm 0.5cm 1.5cm;
            }

            body {
                font-size: 14pt;
                font-family: serif;
                color: black;
            }

            .header {
                display: none;
            }
            table tr td {
                vertical-align: text-top;
            }
        }
    </style>
@endsection

@section('content')
@for($x = 0;$x < count($page);$x++)
    <div style="page-break-after: always" class="bg-white paper">
        <div style="padding-top: 1.7cm;padding-left: 20cm;">{{ $info[0]->docno }}</div>
        <div style="padding-left: 20cm;line-height:0.7cm">{{ $duedate }}</div>
        <div style="padding-top: 1.4cm">
            <div style="padding-left: 2.2cm">{{ $info[0]->customer_name }}</div>
            <div style="padding-left: 2.2cm">{{ $info[0]->customer_address }}</div>
        </div>
        <div>
            <div style="padding-top: 1.8m; padding-left: 3cm;width:11.3cm;display:inline-block;">{{ $info[0]->customer_taxid }}</div>
            <div style="padding-top: 1.8cm; padding-left: 12cm;width:width:11.3cm;display:inline-block;">{{ $info[0]->sale_id }}</div>
        </div>
        <div style="padding-top:1cm;padding-left:2cm;min-height:5cm">
            <table style="width: 23.5cm; vertical-align:text-top">
                @for ($i = 0; $i < count($page[$x]); $i++)
                    <tr style="line-height: 1cm">
                        <td style="width: 1.1cm;text-align:center">{{ $i + 1 }}</td>
                        <td style="width: 8cm" class="text-break">{{ $page[$x][$i]->name }}</td>
                        <td style="width: 2.9cm">{{ $page[$x][$i]->name_option_dt }}</td>
                        <td style="width: 2cm">{{ number_format($page[$x][$i]->unit_qty) }}</td>
                        <td style="width: 2.8cm; text-align:right">{{ number_format($page[$x][$i]->unit_price) }}</td>
                        <td style="width: 2.9cm; text-align:right">{{ number_format($page[$x][$i]->unit_discount) }}</td>
                        <td style="width: 2.9cm; text-align:right">{{ number_format(($page[$x][$i]->unit_qty * $page[$x][$i]->unit_price)) }}</td>
                    </tr>
                @endfor
            </table>
        </div>
                @if ($x !== count($page) - 1)
                    <div style="display:inline-block;padding-top: 1cm; padding-left:2.2cm; width:10cm"></div>
                    <div style="display:inline-block;padding-top: 1cm; padding-left: 13cm"></div>
                    <div style="padding-left: 17cm"></div>
                    <div style="padding-left: 17cm"></div>
                @else
                    <div style="display:inline-block;padding-top: 1cm; padding-left:2.2cm; width:18cm">{{ $satang }}</div>
                    <div style="display:inline-block;padding-top: 1cm; padding-left: 4.5cm;text-align:right"><div style="width: 3cm">{{ number_format($info[0]->novat) }}</div></div>
                    <div style="display:inline-block;padding-top: 0.1cm; padding-left:2.2cm; width:11cm"></div>
                    <div style="display:inline-block;padding-top: 0.1cm; padding-left: 11.5cm;text-align:right"><div style="width: 3cm">{{ number_format($vatres) }}</div></div>
                    <div style="display:inline-block;padding-top: 0.1cm; padding-left:2.2cm; width:11cm"></div>
                    <div style="display:inline-block;padding-top: 0.1cm; padding-left: 11.5cm;text-align:right"><div style="width: 3cm">{{ number_format($info[0]->novat + $vatres) }}</div></div>
                @endif
    </div>
@endfor
@endsection
