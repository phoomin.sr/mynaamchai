@extends('layouts.workpace')

@section('content')
    <div class='container'>
        <form name='customer' action="{{ route('customersave') }}" method="post">
            @csrf
            @isset($data)
                <input type="hidden" name="customer_id" value="{{ $data->id }}">
            @endisset
            <div class="row">
                <div class="col"></div>
                <div class="col-3">
                    <label for="customer_no" class="form-label">พนักงานขายผู้รับผิดชอบ</label>
                    <input type="text" name="sale_taker" id="sale_taker" placeholder="" class="form-control"
                        @isset($data)
                            value="{{ $data->saletaker }}"
                        @endisset
                        required
                    >
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="company_prefix" class="form-label">ประเภทธุรกิจ <span class="text-danger">*</span></label>
                    <select name="company_prefix" id="company_prefix" class="form-control" required>
                        @foreach ($company_prefix as $key => $value)
                            <option value="{{ $key }}"
                                @isset($data)
                                    @if ($key == $data->business_type)
                                        selected
                                    @endif
                                @endisset
                            >{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <label for="customer_no" class="form-label">รหัสลูกค้า <span class="text-danger">*</span></label>
                    <input type="text" name="customer_no" id="customer_no" placeholder="รหัสลูกค้า *ไม่จำเป็น" class="form-control"
                        @isset($data)
                            value="{{ $data->customer_no }}"
                        @endisset
                        required
                    >
                </div>
                <div class="col">
                    <label for="customer_name" class="form-label">ชื่อลูกค้า <span class="text-danger">*</span></label>
                    <input type="text" name="customer_name" id="customer_name" placeholder="ชื่อลูกค้าไม่ต้องมีคำนำหน้า" class="form-control"
                        @isset($data)
                            value="{{ $data->customer_name }}"
                        @endisset
                        required
                    >
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <label for="customer_taxid" class="form-label">รหัสประจำตัวผู้เสียภาษี<span class="text-danger">*</span></label>
                    <input type="text" name="customer_taxid" id="customer_taxid" placeholder="รหัสประจำตัวผู้เสียภาษี" class="form-control"
                    @isset($data)
                        value="{{ $data->customer_taxid }}"
                    @endisset
                    required>
                </div>
                <div class="col">
                    <label for="customer_tel" class="form-label">เบอร์โทรศัพท์</label>
                    <input type="text" name="customer_tel" id="customer_tel" class="form-control"
                        @isset($data)
                            value="{{ $data->phone_no }}"
                        @endisset
                    >
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="customer_fax" class="form-label">เบอร์โทรสาร</label>
                    <input type="text" name="customer_fax" id="customer_fax" class="form-control"
                        @isset($data)
                            value="{{ $data->customer_fax }}"
                        @endisset
                    >
                </div>
                <div class="col">
                    <label for="customer_email" class="form-label">อีเมล์</label>
                    <input type="text" name="customer_email" id="customer_email" class="form-control"
                        @isset($data)
                            value="{{ $data->email }}"
                        @endisset
                    >
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label for="customer_address_no" class="form-label">ที่อยู่เลขที่ <span class="text-danger">*</span></label>
                    <input type="text" name="customer_address_no" id="customer_address_no" placeholder="บ้านเลขที่ หมู่ ซอย ถนน ไม่ต้องกรอก ตำบล อำเภอ จังหวัด" class="form-control"
                        @isset($data)
                            value="{{ $data->customer_address_no }}"
                        @endisset
                        required
                    >
                </div>
                <div class="col">
                    <label for="customer_type" class="form-label">สาขา</label>
                    <div class="input-group">
                    <div class="input-group-text">
                        <input class="form-check-input mt-0" name="isBranch" type="checkbox" value="" aria-label="Checkbox for following text input"
                            @isset($data)
                                @if ($data->isBranch == 1)
                                    checked
                                @endif
                            @endisset
                        >
                    </div>
                    <input type="text" name="branch_no" class="form-control" aria-label="Text input with checkbox" placeholder="สำนักงานใหญ่"
                        @isset($data)
                            value="{{ $data->branch_no }}"
                        @endisset
                    readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="customer_address" class="form-label">ที่อยู่ <span class="text-danger">*</span></label>
                    <input type="search" name="customer_address" id="customer_address" placeholder="ค้นหา ตำบล อำเภอ จังหวัด ตามฐานข้อมูลไปรษณีย์ไทย" class="form-control"
                        @isset($data)
                            value="{{ $data->customer_address }}"
                        @endisset
                        autocomplete="off" required>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <label for="customer_type" class="form-label">ประเภทผู้จำหน่าย</label>
                    <select name="customer_type" id="customer_type" class="form-control">
                        <option value="1">ประจำ</option>
                        <option value="2">ชั่วคราว</option>
                    </select>
                </div>
                <div class="col">
                    <label for="customer_type" class="form-label">วิธีการขนส่ง</label>
                    <input type="text" name="delivery_method" id="delivery_method" class="form-control"
                        @isset($data)
                            value="{{ $data->delivery_method }}"
                        @endisset
                    >
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="customer_credit_term" class="form-label">เครดิตเทอม</label>
                    <input type="number" name="customer_credit_term" id="customer_credit_term" class="form-control"
                        @isset($data)
                            value="{{ $data->credit_day }}"
                        @endisset
                    >
                </div>
                <div class="col">
                    <label for="customer_payment_method" class="form-label">เงื่อนไขการชำระเงิน</label>
                    <input type="text" name="customer_credit_cond" id="customer_credit_cond" class="form-control"
                        @isset($data)
                            value="{{ $data->placeBill_method }}"
                        @endisset
                    >
                </div>
                <div class="col">
                    <label for="customer_credit_amount" class="form-label">วงเงินที่อนุมัติ</label>
                    <input type="number" name="customer_credit_amount" id="customer_credit_amount" class="form-control"
                        @isset($data)
                            value="{{ $data->customer_credit_amount }}"
                        @endisset
                    >
                </div>
            </div>

            <hr>

            <div class="user-contract">
                <div class="row mb-3">
                    <div class="col">
                        <h3>ข้อมูลผู้ติดต่อ</h3>
                    </div>
                    <div class="col-3">
                        <button class="btn btn-block btn-warning contact-input-add-but" style="height: 100%"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
                @if (isset($contact))
                    @for ($i = 0; $i < count($contact); $i++)
                        <div class="row mb-2 contact-input">
                            @isset($contact)
                                <input type="hidden" name="contact_id[]" value="{{ $contact[$i]->id }}">
                            @endisset
                            <div class="col">
                                <label for="ct_name" class="form-label">
                                    ชื่อผู้ติดต่อ
                                </label>
                                <input type="text" name="ct_name[]" id="ct_name" class="form-control"
                                    @isset($contact)
                                        value="{{ $contact[$i]->name }}"
                                    @endisset
                                >
                            </div>
                            <div class="col">
                                <label for="ct_email" class="form-label">
                                    อีเมลผู้ติดต่อ
                                </label>
                                <input type="email" name="ct_email[]" id="ct_email" class="form-control"
                                    @isset($contact)
                                        value="{{ $contact[$i]->email }}"
                                    @endisset
                                >
                            </div>
                            <div class="col">
                                <label for="ct_tel" class="form-label">
                                    เบอร์โทรผู้ติดต่อ
                                </label>
                                <input type="text" name="ct_tel[]" id="ct_tel" class="form-control"
                                    value="{{ $contact[$i]->phone_no }}"
                                >
                            </div>
                        </div>
                        <div class="row contact-input mb-3">
                            <div class="col">
                                <label for="ct_remark" class="form-label">หมายเหตุ</label>
                                <textarea name="ct_remark[]" id="ct_remark" class="form-control">@isset($contact){{ $contact[$i]->remark }}@endisset</textarea>
                            </div>
                        </div>
                    @endfor
                @else
                    <div class="row mb-2 contact-input">
                        @isset($contact)
                            <input type="hidden" name="contact_id[]" value="{{ $contact[$i]->id }}">
                        @endisset
                        <div class="col">
                            <label for="ct_name" class="form-label">
                                ชื่อผู้ติดต่อ
                            </label>
                            <input type="text" name="ct_name[]" id="ct_name" class="form-control"
                                @isset($contact)
                                    value="{{ $contact[$i]->name }}"
                                @endisset
                            >
                        </div>
                        <div class="col">
                            <label for="ct_email" class="form-label">
                                อีเมลผู้ติดต่อ
                            </label>
                            <input type="email" name="ct_email[]" id="ct_email" class="form-control"
                                @isset($contact)
                                    value="{{ $contact[$i]->email }}"
                                @endisset
                            >
                        </div>
                        <div class="col">
                            <label for="ct_tel" class="form-label">
                                เบอร์โทรผู้ติดต่อ
                            </label>
                            <input type="text" name="ct_tel[]" id="ct_tel" class="form-control"
                                @isset($contact)
                                    value="{{ $contact[$i]->phone_no }}"
                                @endisset
                            >
                        </div>
                    </div>
                    <div class="row contact-input mb-3">
                        <div class="col">
                            <label for="ct_remark" class="form-label">หมายเหตุ</label>
                            <textarea name="ct_remark[]" id="ct_remark" class="form-control">@isset($contact){{ $contact[$i]->remark }}@endisset</textarea>
                        </div>
                    </div>
                @endif
            </div>
            <div class="mt-2">
                <input type="submit" value="save" class="form-control btn btn-primary" id="submit">
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('form').change(function () {
                var valid = $('span[valid]');
                $.each(valid, function () {
                    if ($(this).attr('valid') == 'false') {
                       formsubmit_lock($(this).attr('valid'));
                       return false;
                    } else {
                        formsubmit_lock($(this).attr('valid'));
                    }
                });
            });
            $('#customer_trade_no').keyup(function () {
                var eleName = $(this).attr('name');
                if ($('[name="'+eleName+'"]').val().length > 2) {
                    $.ajax({
                        method: 'POST',
                        data: {
                            '_token': $('[name="_token"]').val(),
                            table: 'customers',
                            column: eleName,
                            value: $(this).val()
                        },
                        url: "{{ route('checkDuplicate') }}"
                    }).done(function (response) {
                        valid_error(eleName, !response[0]);
                    });
                }
            });
            $('#customer_name, #customer_no').keyup(function () {
                var eleName = $(this).attr('name');
                if ($('[name="'+eleName+'"]').val().length > 2) {
                    $.ajax({
                        method: 'POST',
                        data: {
                            '_token': $('[name="_token"]').val(),
                            table: 'customers',
                            column: eleName,
                            value: $(this).val()
                        },
                        url: "{{ route('checkDuplicate') }}"
                    }).done(function (response) {
                        valid_error(eleName, !response[0]);
                    });
                }
            });
            $('button.contact-input-add-but').click(function (e) {
                e.preventDefault();
                var cloned = $('div.contact-input').clone();
                $('div.contact-input:last').after(cloned);
            });
            $('[name="customer_address"]').autoComplete({
            resover: 'custom',
            autoFill: true,
            events:{
                search: function (qry, callback) {
                    $.ajax({
                        method: 'POST',
                        url: '{{ route("searchAddress") }}',
                        data: {
                            '_token': $('input[type=hidden][name=_token]').val(),
                            'f': qry
                        }
                    }).done(function (res) {
                        callback(res);
                    });
                },
            }
        });
        $('[name="customer_address"]').on('autocomplete.select', function (env, item) {

            value = item['value'].split(',');
            $('[name=tumbol]').val(value[0]);
            $('[name=amphur]').val(value[1]);
            $('[name=province]').val(value[2]);
        });
        $('[name="isBranch"]').change(function () {
                $('[name="branch_no"]').prop('readonly', !$(this).is(':checked'));
        });
        });
        function validate_customer () {
            return true;
        }
        function ajax_customer(data){
            send(data, function (response) {
                if (response.save_customer) {
                    alertify.notify(response.msg, 'success', 3, function(){
                        window.location.replace(response.redirect_to);
                    });
                } else {
                    alertify.notify(response.msg, 'error', 5);
                }
            });
        }
    </script>
@endsection
