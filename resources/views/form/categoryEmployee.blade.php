@extends('layouts.workpace')

@section('content')
    <div class="container">
        <div class="serach mt-2">
            <div class="row">
                <input type="search" name="search" id="search" placeholder="ค้นหาด้วย ชื่อ นามสกุล" class="form-control">
            </div>
            <div class="row mt-2">
                <div class="col-sm-3">
                    <a href="{{ route('catEmployee') }}" class="btn btn-outline-info">Clear Filter</a>
                </div>
            </div>
        </div>
        <div class="table mt-5">
            <a href="{{ route('addcatEmployee') }}" class="btn btn-warning btn-block mb-3">+ กลุ่มพนักงาน</a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>รหัสกลุ่ม</th>
                        <th>ชื่อตำแหน่ง</th>
                        <th>ลบ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($position as $key => $items)
                        <tr>
                            <td><a class="btn btn-warning" href="{{ route('editcatEmployee', ['id'=>$items->id]) }}"><i class="fas fa-edit"></i></a></td>
                            <td>{{ $items->slug }}</td>
                            <td>{{ $items->position_name }}</td>
                            <td><button type="button" class="btn btn-danger delete"><i class="fas fa-trash"></i></button></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // function delEmpCat(url) {
        //     $.ajax({
        //         method:'GET',
        //         url: url
        //     }).done(function (response) {
        //         if (response.delete) {
        //         alertify.notify(response.msg, 'success', 5, function(){
        //             window.location.replace(response.redirect_to);
        //          });
        //         } else {
        //             alertify.notify(response.msg, 'error', 5);
        //         }
        //     });
        // }
    </script>
@endsection
