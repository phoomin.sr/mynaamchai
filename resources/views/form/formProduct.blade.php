@extends('layouts.workpace')

@section('content')
<div class="container">
    <form name="product" action="{{ $action }}" method="POST">
        @csrf
        <div class="row">
            <div class="col"><h4>ข้อมูลทั่วไป</h4></div>
        </div>
    <hr>
        <div class="row mb-3">
            <div class="col">
                <label for="product_type" class="form-label">ประเภทสินค้า</label>
                <select name="product_type" id="product_type" class="form-control">
                    @foreach ($cat as $id => $item)
                        <option value="{{ $id }}">{{ $item }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <label for="code" class="form-label">รหัสสินค้าคงเหลือ</label>
                <input type="text" name="code" id="code" class="form-control">
            </div>
            <div class="col">
                <label for="name" class="form-label">ชื่อ</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>
            <div class="col">
                <label for="gen" class="form-label">รุ่น</label>
                <input type="text" name="gen" id="gen" class="form-control">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <label for="location" class="form-label">ที่จัดเก็บ</label>
                <input type="text" name="location" class="form-control">
            </div>
            <div class="col">
                <label for="min_stock" class="form-label">จุดสั่งซื้อ</label>
                <input type="number" name="min_stock" class="form-control">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <h4>ตัวเลือก</h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <table class="table table-striped" name='option'>
                    <tbody>
                        @for ($i = 0; $i < 5; $i++)
                            <tr>
                                <td>
                                    <select name="option_type[]" class="form-control selectpicker" onchange="ajax_option({{ $i }})">
                                        <option value="0">--เลือก--</option>
                                        @foreach ($option as $key => $value)
                                            <option value="{{ $value['option_id'] }}">{{ $value['option_name'] }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select name="option_name[{{$i}}][]" class="form-control selectpicker" multiple>
                                    </select>
                                </td>
                                <td>
                                    <button class="btn btn-block btn-danger" type="button" name="delRow">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <button class="btn btn-warning btn-block" onclick="clickClone()">+ เพิ่ม</button>
            </div>
        </div>
        <hr class="mt-5">
        <div class="row">
            <div class="col"><h3>รายการ SKUs</h3></div>
            <div class="col-4"><button class="btn btn-primary btn-block" onclick="generateSkus()">สร้าง</button></div>
            <table class="table table-striped" id="target-bf-save">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>SKU</th>
                        <th>ชื่อสินค้าคงเหลือ</th>
                        <th>ราคาขาย</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="text-right">
            <input type="submit" value="บันทึก" class="btn btn-primary">
        </div>
    </form>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $('[type=submit]').attr('disabled', true);
        $('button').click(function (e) {
            e.preventDefault();
        });
    });
    function del_line() {
        $(document).ready(function () {
            $('button[type=buttton][name=delRow]').click(function () {
                let i = $('button[type=buttton][name=delRow]').index($(this));
            });
        });
    }
    function generateSkus() {
        var prefix = $('#code').val();
        var name = $('#name').val() + ' ' + $('#gen').val();
        var array_sku = [];
        var array_name = [];
        var option = [];

        if (prefix == "") {
            $('#code').addClass('border border-danger');
            return ;
        }
        if (name == "") {
            $('#name').addClass('border border-danger');
            $('#gen').addClass('border border-danger');
            return ;
        }

        for (let i = 0;i < $('[name*="option_name"]').length;i++) {
            if ($('[name*="option_name"]').eq(i).val().length > 0) {
                option.push($('[name*="option_name"]').eq(i).val());
            }
        }
        for (let i = 0;i < option.length;i++) {
            if (array_sku.length == 0) {
                for (ii = 0;ii < option[i].length;ii++) {
                    start = option[i];
                    array_sku.push(prefix + '-' + start[ii].split(',')[0]);
                    array_name.push(name + ' ' + start[ii].split(',')[1]);
                }
            } else {
                var buffer = [];
                var nbuffer = [];
                for (ii = 0;ii < array_sku.length;ii++) {
                    for (x = 0;x < option[i].length;x++) {
                        key = array_sku[ii] + '-' + option[i][x].split(',')[0];
                        nkey = array_name[ii] + ' ' + option[i][x].split(',')[1];
                        buffer.push(key);
                        nbuffer.push(nkey);
                    }
                }
                array_sku = buffer;
                array_name = nbuffer;
            }
        }
        show_arr_table(array_name, array_sku);
    }
    function show_arr_table(name, skus) {
        var str;
        $('table#target-bf-save tbody').html("");
        for (let i = 0;i < skus.length;i++) {
            str += "<tr>"
                + "<td>"+(i+1)+"</td>"
                + "<td><input type='text' class='form-control' name='skus[]' value='"+skus[i]+"'></td>"
                + "<td><input type='text' class='form-control' name='product_name[]' value='"+name[i]+"'></td>"
                + "<td>"
                + "<input type='number' name='product_sale_price[]' class='form-control' step='0.01' value=''>"
                + "</td>"
                +"</tr>";
        }
        $('table#target-bf-save tbody').append(str);
        $('input[type=submit]').attr('disabled', false);
    }
    function clickClone() {
        var tableRow = $('table[name=option] tbody tr').first().clone();
        $('table[name=option] tbody tr').last().after(tableRow);
    }
    function ajax_option(i) {
        $.ajax({
            'method': 'POST',
            'url': "{{ route('get_options') }}",
            'data': {
                '_token': $('input[type=hidden][name=_token]').val(),
                'option_id': $('select[name="option_type[]"]').eq(i).val()
            }
        }).done(function (response) {
            var strOption = "";
            var strLi = "";
            $.each(response, function (key, value) {
                strOption += "<option value='"+key+','+value+"' data-subtext='"+key+"' title='"+value+"'>"+value+"</option>";
                strLi += "<li>"+value+"</li>";
            });
            $('select[name="option_name['+i+'][]"]').html(strOption).selectpicker('refresh');
        });
    }
    function validate_product()
    {
        var err = [];
        if ($('#code').val().length == 0) {
            err.push('code');
        }
        if ($('#name').val().length == 0) {
            err.push('name');
        }
        if ($('#gen').val().length == 0) {
            err.push('gen');
        }
        if ($('#product_type').val() == 0) {
            err.push('product_type');
        }

        if (err.length !== 0) {
            return err;
        }
        return true;
    }
    function ajax_product(data) {
        send(data, function (response) {
            if (response.save) {
                alertify.notify(response.msg, 'success', 3, function(){
                    window.location.replace(response.redirect_to);
                 });
            } else {
                alertify.notify(response.msg, 'error', 5);
            }
        });
    }
</script>
@endsection
