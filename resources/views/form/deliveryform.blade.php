@extends('layouts.workpace')

@section('content')
<form name="deli_form" action="{{ route('deliverysave') }}" method="post">
    <div class="row mb-2">
        <div class="col"></div>
        <div class="col-3">
            <label for="docno" id="docno" class="form-label">เล่มที่</label>
            <input name="docno" type="text" class="form-control">
        </div>
        <div class="col-3">
            <label for="runno" id="runno" class="form-label">เลขที่</label>
            <input name="runno" type="text" class="form-control">
        </div>
    </div>
    <div class="row mb-2">
        <div class="col">
            <label for="doc_date" id="doc_date" class="form-label">วันที่เอกสาร</label>
            <input name="doc_date" type="text" class="form-control" placeholder="วันที่">
        </div>
        <div class="col">
            <label for="delivery_date" id="delivery_date" class="form-label">วันที่ส่ง</label>
            <input name="delivery_date" type="text" class="form-control" placeholder="วันที่">
        </div>
        <div class="col">
            <label for="recieved_date" id="recieved_date" class="form-label">ประมาณการวันที่ถึง</label>
            <input name="recieved_date" type="text" class="form-control" placeholder="วันที่">
        </div>
    </div>
    <div class="row mb-2">
        <div class="col">
            <div class="mb-2">
                <label for="customer" id="customer" class="form-label">ผู้รับน้ำมัน</label>
                <input name="customer" type="search" class="form-control" placeholder="ค้นหาลูกค้า">
            </div>
            <div>
                <label for="customer_address" class="form-label">ที่อยู่ลูกค้า</label>
                <textarea name="customer_address" id="customer_address" class="form-control"></textarea>
            </div>
        </div>
        <div class="col">
            <label for="export_no" class="form-label">เลขที่ใบอนุญาติ (ผู้รับจ้างขนส่งน้ำมันตามมาตรา 12)</label>
            <input type="text" name="export_no" id="export_no" class="form-control">
        </div>
    </div>
    <hr>
    <div class="row mb-3">
        <div class="col">
            <h3>รายละเอียดการเดินทาง</h3>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col">
            <div class="mb-2">
                <label for="car_no" id="car_no" class="form-label">เลขทะเบียนรถ</label>
                <input name="car_no" type="text" class="form-control">
            </div>
        </div>
        <div class="col">
            <label for="seal_no" id="seal_no" class="form-label">หมายเลข Seal</label>
            <input name="seal_no" type="text" class="form-control">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <input type="submit" class="form-control btn btn-primary" value="บันทึก">
        </div>
    </div>
</form>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            dateElem($('[name="doc_date"]'));
            dateElem($('[name="delivery_date"]'));
            dateElem($('[name="recieved_date"]'));
        });
    </script>
@endsection
