@extends('layouts.workpace')

@section('content')
    <div class="container">
        <form action="{{ $action }}" method="post" name="supplier">
            <div class="row mb-3">
                <div class="col-6">
                    <label for="business_type" class="form-label">ประเภทธุรกิจ</label>
                    <select name="business_type" class="form-control" id="business_type">
                        @foreach ($business_type as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-6">
                    <label for="supplier_name" class="form-label">ชื่อผู้ผลิต/คู่ค้า</label>
                    <input type="text" name="supplier_name" id="supplier_name" placeholder="ชื่อผู้ผลิต" class="form-control">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label for="supplier_tax_no" class="form-label">เลขประจำตัวผู้เสียภาษี</label>
                    <input type="text" name="supplier_tax_no" id="supplier_tax_no" placeholder="เลขประจำตัวผู้เสียภาษี 13 หลัก" class="form-control">
                </div>
                <div class="col-6">
                    <label for="supplier_tax_no" class="form-label">เลขประจำตัวผู้เสียภาษี</label>
                    <input type="text" name="supplier_tax_no" id="supplier_tax_no" placeholder="เลขประจำตัวผู้เสียภาษี 13 หลัก" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <input type="submit" value="บันทึก" class="btn btn-block btn-primary">
                </div>
            </div>
        </form>
    </div>
@endsection
