@extends('layouts.workpace')

@section('content')
    <div class='container'>
        <form name="sale_form" id="sale_form" action="{{ route('savesale') }}" method="post">
            @csrf
            <div class="row mb-3">
                <div class="col"></div>
                <div class="col-3">
                    <label for="docno" class="form-label">เลขที่</label>
                    <input type="text" name="docno" id="docno" class="form-control">
                </div>
                <div class="col-3">
                    <label for="doc_date" class="form-label">วันที่เอกสาร</label>
                    <input type="text" name="doc_date" id="doc_date" class="form-control">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col"></div>
                <div class="col-3">
                    <label for="sale" class="form-label">พนักงานขาย</label>
                    <input type="text" name="sale" id="sale" class="form-control">
                </div>
                <div class="col-3">
                    <label for="duedate" class="form-label">วันที่ครบกำหนด</label>
                    <input type="text" name="due_date" id="due_date" class="form-control">
                </div>

            </div>
            <div class="customer-xection">
                <div class="row">
                    <div class="col">
                        <label for="customer_no" class="form-label">รหัสลูกค้า</label>
                        <input type="search" name="customer_no" id="customer_no" class="form-control" placeholder="ค้นหารหัสลูกค้า" autocomplete="off">
                    </div>
                    <div class="col">
                        <label for="customer_name" class="form-label">ชื่อลูกค้า</label>
                        <input type="search" class="form-control" name="customer_name" id='customer_name' placeholder="ค้นหาชื่อลูกค้า..." autocomplete="off">
                        <input type="hidden" name="customer_id" id="customer_id">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="customer_address" class="form-label">ที่อยู่</label>
                        <textarea name="customer_address" id="customer_address" class="form-control"></textarea>
                    </div>
                </div>
                <div class="row tb-sale mt-5">
                    <div class="col">
                        <h3 class='text-muted'>รายการสินค้า</h3>
                        <table class="table table-striped table-hover table-responsive" id="tb-sale">
                            <thead class="table-warning">
                                <tr>
                                    <th width=''>#</th>
                                    <th width=''>สินค้า</th>
                                    <th width=''>หน่วย</th>
                                    <th width=''>จำนวน</th>
                                    <th width=''>ราคา</th>
                                    <th width=''>รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for ($i = 0; $i < 10; $i++)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td class="input-group input-group-sm">
                                            <input type="search" name="product[]" class="form-control" placeholder="รายการขาย..." value="" autocomplete="off">
                                            <input type="hidden" name="product_id[]" value="">
                                        </td>
                                        <td>
                                            <input type="text" name="unit[]" id="unit_{{ $i }}" value="" class='form-control bg-light' readonly>
                                        </td>
                                        <td>
                                            <input type="text" name="txt_qty[]" id="txt_qty_{{ $i }}" class="form-control">
                                            <input type="hidden" name="qty[]" id="qty_{{ $i }}">
                                        </td>
                                        <td>
                                            <input type="text" name="txt_price[]" id="txt_price_{{ $i }}" class="form-control">
                                            <input type="hidden" name="price[]" id="price_{{ $i }}">
                                        </td>
                                        <td>
                                            <input type="text" name="txt_sub_price[]" id="txt_sub_price_{{ $i }}" class="form-control">
                                            <input type="hidden" name="sub_price[]" id="sub_price_{{ $i }}">
                                        </td>
                                    </tr>
                                @endfor
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="fs-6">รวมก่อน vat</td>
                                    <td class="input-group input-group-sm">
                                        <input type="text" name="txt_sub_total" id="txt_sub_total" step="0.01" class="form-control input-sm">
                                        <input type="hidden" name="sub_total" id="sub_total">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="fs-6">ส่วนลดท้ายบิล(บาท)</td>
                                    <td class="input-group input-group-sm">
                                        <input type="text" name="txt_discount_baht" id="txt_discount_baht" step="0.01" class="form-control input-sm">
                                        <input type="hidden" name="discount_baht" id="discount_baht">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="fs-6">VAT
                                        <select name="vattype" id="vattype">
                                            @foreach ($vattype as $key => $item)
                                                @if ($key == 'include_vat')
                                                    <option value="{{ $key }}" selected>{{ $item }}</option>
                                                @else
                                                    <option value="{{ $key }}">{{ $item }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="input-group input-group-sm">
                                        <input type="text" name="txt_vatres" id="txt_vatres" class="form-control" step='0.01'>
                                        <input type="hidden" name="vatres" id="vatres">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="fs-6">ราคารวม VAT</td>
                                    <td class="input-group input-group-sm">
                                        <input type="text" name="txt_sum_vat" id="txt_sum_vat" class="form-control" step='0.01'>
                                        <input type="hidden" name="sum_vat" id="sum_vat">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="fs-6">ราคารวมสุทธิ</td>
                                    <td class="input-group input-group-sm">
                                        <input type="text" name="txt_gtt" id="txt_gtt" class="form-control" step='0.01'>
                                        <input type="hidden" name="gtt" id="gtt">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input type="submit" value="save" class="btn btn-block btn-primary">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script src="{{ asset('asset/js/cal.js') }}"></script>
    <script>
        $(document).ready(function () {
            dateElem($('[name="doc_date"]'));
            dateElem($('[name="due_date"]'));
            $('[name="qty[]"],[name="price[]"],[name="discount_baht"]').change(function () {
                var name = $(this).attr('name');
                var i = $("[name='"+name+"']").index($(this));

                subcal($('[name="price[]"]').eq(i), $('[name="qty[]"]').eq(i), $('[name="sub_price[]"]').eq(i));

                total = 0;
                $.each($('[name="sub_price[]"]'), function () {
                    if ($(this).val() !== "") {
                        total += parseFloat($(this).val());
                    }
                });
                $('#sub_total').val(total);

                cal();
            });
            $('#vattype').change(function (env, item) {
                cal();
            });
            $('#sum_vat').on('change', function (env, item) {
                cal();
            });
            $('[name="customer_name"]').autoComplete({
            resover: 'custom',
            autoFill: true,
            events:{
                search: function (qry, callback) {
                    $.ajax({
                        method: 'POST',
                        url: '{{ $findCustomer }}',
                        data: {
                            '_token': $('input[type=hidden][name=_token]').val(),
                            'f': qry
                        }
                    }).done(function (res) {
                        callback(res);
                    });
                }
            }
        });
        searchbox($('#customer_no'), "{{ route('searchCustomer') }}", (env, item) => {
            var value = jQuery.parseJSON(item.value);
            var credit_term = addDate($('#doc_date').val(), value.customer_credit_term);

            $('#due_date').datepicker('update', credit_term);
            $('#sale').val(value.saletaker);
            $('#customer_no').val(value.customer_no);
            $('#customer_name').val(value.customer_name);
            $('#customer_id').val(value.id);
            $('#customer_address').val(value.customer_address_no + " " + value.customer_address);
        });
        searchbox($('[name="product[]"]'), "{{ route('searchProduct') }}", (env, item) => {
            i = $('[name="product[]"]').index($(env.currentTarget));
            $('[name="product_id[]"]').eq(i).val(item.value);
            sending({
                id: item.value
            }, "{{ route('getExtra_product') }}")
            .then(response => {
                extra = response.extra[0];
                $('[name="unit[]"]').eq(i).val(extra.name_option_dt);
                $('[name="price[]"]').eq(i).val(extra.product_sale_price);
            })
            .catch(err => {
                console.log(err);
            });
        });

    });
    function validate_sale_form () {
        var err = [];
        if ($('[name="customer_name"]').length == 0) {
            err.push("customer_name");
        }
        if ($('[name="customer_address"]').length == 0) {
            err.push("customer_address");
        }

        if (err.length !== 0) {
            console.log(err);
        } else {
            return true;
        }
    }
    function ajax_sale_form(data) {
        send(data, function (response) {
            if (response.save_invoice) {
                alertify.notify(response.msg, 'success', 3, function(){
                    window.location.replace(response.redirect_to);
                });
            } else {
                alertify.notify(response.msg, 'error', 5);
            }
        });
    }
    </script>
@endsection
