@extends('layouts.workpace')

@section('content')
<div class="container">
    <form action="{{ $action }}" method="POST" name="category_stock">
        @csrf
        <input type="hidden" name="id" value="{{ $id ?? '' }}">
        <div class="row mb-3">
            <div class="col">
                <label for="group_main" class="form-label">กลุ่มหลัก</label>
                <select name="group_main" id="group_main" class="form-control">
                    @foreach ($category as $key => $item)
                        <option value="{{ $key }}"
                        @if (isset($info['parent'])&&$key == $info['parent'])
                            selected
                        @endif
                        >{{ $item }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <label for="slug" class="form-label">รหัสกลุ่ม</label>
                <input type="text" class="form-control" name="slug" id="slug" value="{{ $info['slug'] ?? '' }}" maxlength="4">
            </div>
            <div class="col">
                <label for="category_name" class="form-label">ชื่อกลุ่ม</label>
                <input type="text" class="form-control" name="category_name" value="{{ $info['name'] ?? '' }}" id="category_name">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <label for="cost_method" class="form-label">วิธีคำนวนต้นทุน</label>
                <select name="cost_method" class="form-control">
                    @foreach ($cost_method as $key => $item)
                        <option value='{{ $key }}'>{{ $item }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="description" class="form-label">รายละเอียด</label>
                <textarea class="form-control" id="description" name="description" cols="30" rows="10">{{ $info[0]['description'] ?? '' }}</textarea>
            </div>
        </div>
        <div class="m-3">
            <div class="text-right">
                <input type="reset" value="cancel" class="btn btn-secondary">
                <input type="submit" value="save" class="btn btn-primary">
            </div>
        </div>
    </form>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('[name=slug]').change(function () {
                val = $('[name=slug]').val().toUpperCase();
                $('[name=slug]').val(val);
                $.ajax({
                    url: "{{ route('checkDuplicate') }}",
                    method: 'POST',
                    data: {
                        '_token': $('[type=hidden][name="_token"]').val(),
                        'table': 'product_category',
                        'table2': 'category_tax',
                        'first': 'id',
                        'second': 'category_id',
                        'column': 'slug',
                        'expression': '=',
                        'value': val,
                        'scope': 'company'
                    }
                }).done(function (response) {
                    if ($('label[for=slug]').next().is('SPAN')){
                        $('label[for=slug]').next().remove();
                    }
                    if (response[0] == true) {
                        $('label[for=slug]').after("<span class='text-danger'>*รหัสซ้ำ</span>");
                    }
                });
            });
            $('[name=category_name]').change(function () {

            });
        });
        function validate_category_stock () {
            status = [];

            if ($('[name=category_name]').val() == "") {
                status.push('category_name');
            }
            if ($('[name=slug]').val() == "") {
                status.push('slug');
            }

            if (status.length > 0) {
                return status;
            } else {
                return true;
            }
        }
        function ajax_category_stock (data) {
            send(data, function (response) {
                if (response.save) {
                    alertify.notify(response.msg, 'success', 5, function(){
                        window.location.replace(response.redirect_to);
                    });
                } else {
                    alertify.notify(response.msg, 'error', 5);
                }
            });
        }
    </script>
@endsection
