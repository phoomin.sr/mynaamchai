@extends('layouts.workpace')

@section('content')
<div class="container">
    <form action="{{ $action }}" method="POST" name="option">
        @csrf
        <div class="row">
            <div class="col">
                <label for="group_main" class="form-label">กลุ่ม <span class="text-danger">*</span></label>
                <input type="text" name="group_main" id="group_main" class="form-control" value="{{ $info[0]['name'] ?? '' }}">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <label for="option-tb" class="form-label">ตารางเพิ่มตัวเลือก</label>
                <table class="table table-striped">
                    <thead class="text-center">
                        <tr>
                            <th style="width:10rem">ชื่อย่อตัวเลือก</th>
                            <th style="width:20rem">ตัวเลือก</th>
                            <th style="width:5rem">ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i < max(5, isset($infos) ? count($infos):0); $i++)
                            <tr class='program'>
                                <td>
                                    <input type="text" name="abs_option_name[]" class="form-control" maxlength="4" value="{{ $infos[$i]['abs_option_dt'] ?? '' }}">
                                    <input type="hidden" name="option_dt_id[]" value="{{ $infos[$i]['id'] ?? '' }}">
                                </td>
                                <td>
                                    <input type="text" name="option_name[]" class="form-control" value="{{ $infos[$i]['name_option_dt'] ?? '' }}">
                                </td>
                                <td>
                                    <button name="del-row" class="btn btn-danger btn-block"><i class="fas fa-times"></i></button>
                                </td>
                            </tr>
                        @endfor
                        <tr>
                            <td colspan="4">
                                <button name="table-add-row" class="btn btn-block btn-warning">+ เพิ่ม</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col text-right">
                @if (isset($id))
                    <input type='hidden' name='option_id' value='{{ $id }}'>
                @endif
                <input type="submit" value="ยืนยัน" class="btn btn-primary">
            </div>
        </div>
    </form>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('button[name="table-add-row"]').click(function (e) {
                e.preventDefault();
                var elems = $('tr.program').first().clone();
                elems.find('input[type=text]').val('');
                $('tr.program').last().after(elems);
            });
        });
        function UpperCase() {
            $(document).ready(function () {
                $('[name="abs_option_name[]"]').change(function () {
                    var str = $(this).val().toUpperCase();
                    $(this).val(str);
                });
            });
        }
        function delrow() {
            $(document).ready(function () {
                $('button[name=del-row]').click(function () {
                    row = $('button[name=del-row]').index($(this));
                    $('table tr.program').eq(row).remove();
                });
            });
        }
        function validate_option ()
        {
            status = $('[name="abs_option_name[]"]').val() !== "" ? true:false;
            return status;
        }
        function ajax_option (data)
        {
            send(data, function (response) {
                if (response.save) {
                    alertify.notify(response.msg, 'success', 5, function(){
                        window.location.replace(response.redirect_to);
                    });
                } else {
                    alertify.notify(response.msg, 'error', 5);
                }
            });
        }
    </script>
@endsection
