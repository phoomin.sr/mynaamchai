@extends('layouts.workpace')

@section('content')
    <div class="container">
        <form name="updateProduct" action="{{ route('updateProduct') }}" method="post">
            @csrf
            <div class="row mb-3">
                <div class="col">
                    <label for="product_category" class="form-label">กลุ่มสินค้า</label>
                    <select name="category_group" id="category_group" class="form-control">
                        @foreach ($cat as $key => $item)
                            <option value="{{ $key }}" @if ($info[0]->product_cat == $key)
                                selected
                            @endif>{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="product_name" class="form-label">รหัสชื่อสินค้าคงเหลือ</label>
                    <input type="text" name="product_code" id="product_code" class="form-control" value="{{ $info[0]['product_code'] }}">
                </div>
                <div class="col">
                    <label for="product_name" class="form-label">ชื่อสินค้าคงเหลือ</label>
                    <input type="text" class="form-control" name="product_name" id="product_name" value="{{ $info[0]['product_name'] }}">
                </div>
                <div class="col">
                    <label for="generation" class="form-label">รุ่น</label>
                    <input type="text" class="form-control" name="product_generation" id="product_generation" value="{{ $info[0]['product_generation'] }}">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="location" class="form-label">ที่จัดเก็บ</label>
                    <input type="text" name="location" id="location" class="form-control" value="{{ $info[0]['product_location'] }}">
                </div>
                <div class="col">
                    <label for="minstock" class="form-label">จุดสั่งซื้อ</label>
                    <input type="number" class="form-control" name="product_minstock" id="product_minstock" value="{{ $info[0]['product_minstock'] }}">
                </div>
            </div>
            <input type="hidden" name="id" value="{{ $info[0]['id'] }}">
            <div class="text-right">
                <input type="submit" value="บันทึก" class="btn btn-primary mt-3">
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script>
        function validate_updateProduct()
        {
            return true;
        }
        function ajax_updateProduct(data)
        {
            send(data, function (response) {
                if (response.save) {
                alertify.notify(response.msg, 'success', 3, function(){
                    window.location.replace(response.redirect_to);
                 });
                } else {
                    alertify.notify(response.msg, 'error', 5);
                }
            });
        }
    </script>   
@endsection