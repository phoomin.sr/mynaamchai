@extends('layouts.workpace')

@section('content')
    <div class="container">
        <form name='employee' action="{{ route($route) }}" method="POST">
            @csrf
            @if ($action == 'add')
            <div class="row">
                <div class="col">
                    <h3>สมัครสมาชิก</h3><span class="text-muted">(สำหรับสร้างผู้ใช้)</span>
                </div>
                <div class="col">
                    <button class="btn btn-primary float-right" type="button"
                    data-toggle="collapse" data-target="#userInfo" aria-expanded="false" aria-controls="userInfo">
                        แสดง
                    </button>
                </div>
            </div>
            <hr class="divider">
            <div class="userInfo collapse" id="userInfo">
                <div class="mb-5">
                    <div class="mb-3">
                        <label for="email" class="form-label">อีเมล</label>
                        <input type="email" name="email" id="email" value="{{ $user->email ?? '' }}" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="username" class="form-label">ชื่อผู้ใช้</label>
                        <input type="text" name="username" id="username" value="{{ $user->name ?? '' }}" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">รหัสผู้ใช้</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">ยืนยันรหัสผู้ใช้</label>
                        <input type="password" name="confirm-password" id="confirm-password" class="form-control">
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col">
                    <h3>รายละเอียดพนักงาน</h3>
                    <span class="text-muted">(สำหรับเพิ่มพนักงาน)</span>
                </div>
            </div>
            <hr class="divider">
            <div class="row mb-3">
                <div class="col mb-3">
                    <label for="firstname" class="form-label">ชื่อ<span class="text-danger">*</span></label>
                    <input type="text" name="firstname" id="firstname" value="{{ $info->firstname ?? '' }}" class="form-control">
                </div>
                <div class="col mb-3">
                    <label for="lastname" class="form-label">นามสกุล<span class="text-danger">*</span></label>
                    <input type="text" name="lastname" id="lastname" value="{{ $info->lastname ?? '' }}" class="form-control">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col mb-5">
                    <label for="position">ตำแหน่งงาน<span class="text-danger">*</span></label>
                    <select name="position" class="form-control">
                        @foreach ($position as $key => $item)
                            <option value="{{ $key }}" @if ($info->position ?? '' == $key)
                                selected
                            @endif >{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="bithdate" class="form-label">วันเดือนปี เกิด</label>
                    <input type="text" name="bithdate" id="bithdate" class="form-control" value="{{ $user_meta['bithdate'] ?? "" }}">
                </div>
                <div class="col">
                    <label for="phone" class="form-label">เบอร์โทรศัพท์</label>
                    <input type="text" name="phone" id="phone" class="form-control" value="{{ $user_meta['phone'] ?? "" }}">
                </div>
                <div class="col">
                    <label for="salary" class="form-label">เงินเดือน</label>
                    <input type="text" name="salary" id="salary" class="form-control" value="{{ $user_meta['salary'] ?? "" }}">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3">
                    <label for="houseNo" class="form-label">บ้านเลขที่</label>
                    <input type="text" name="houseNo" id="houseNo" class="form-control" value="{{ $user_meta['houseNo'] ?? "" }}">
                </div>
                <div class="col">
                    <label for="address">ที่อยู่</label>
                    <input type="text" name="address" id="address" class="form-control" value="{{ $user_meta['address'] ?? "" }}">
                </div>
            </div>
            @if ($action == 'update')
                <input type="hidden" name="employee_id" value="{{ $info->id }}">
            @endif
            <div class="text-center">
                <input type="hidden" name="grand" value="0">
                <input type="reset" value="ยกเลิก" class="btn btn-secondary">
                <input type="submit" value="บันทึก" class="btn btn-primary">
            </div>
        </form>
    </div>
@endsection
@section('script')
@if ($action == 'add')
    <script>
        $(document).ready(function () {
        // formsubmit_lock(false);
        dateElem($('[name=bithdate]'), {'defaultDate': '-18y'});
        $('#email').change(function () {
            $.ajax({
                url: "{{ route('checkDuplicate') }}",
                method: 'POST',
                data: {
                    '_token': $('[type=hidden][name="_token"]').val(),
                    table: "users",
                    column: 'email',
                    expression: '=',
                    scope: 'company',
                    value: $(this).val()
                }
            }).done(function (response) {
                console.log(response);
                if ($('label[for=email]').next().is('SPAN')) {
                        $('label[for=email]').next().remove();
                    }
                if (response[0]) {
                    $('label[for=email]').after('<span class="text-danger">อีเมลซ้ำ ไม่สามารถใช้งานได้</span>')
                } else {
                    $('label[for=email]').after('<span class="text-success">สามารถใช้งาน email นี้ได้</span>')
                }
            });
        });
        $('#username').change(function () {
            $.ajax({
                url: "{{ route('checkDuplicate') }}",
                method: 'POST',
                data: {
                    '_token': $('[type=hidden][name="_token"]').val(),
                    table: "users",
                    column: 'name',
                    expression: '=',
                    scope: 'company',
                    value: $(this).val()
                }
            }).done(function (response) {
                if ($('label[for=username]').next().is('SPAN')) {
                        $('label[for=username]').next().remove();
                    }
                if (response[0]) {
                    $('label[for=username]').after('<span class="text-danger">อีเมลซ้ำ ไม่สามารถใช้งานได้</span>')
                } else {
                    $('label[for=username]').after('<span class="text-success">สามารถใช้งาน email นี้ได้</span>')
                }
            });
        });
        $('[type="password"]').change(function () {
            if ($('[name=password]').val() !== ""&&$('[name="confirm-password"]').val() !== "") {
                    if ($('label[for=password]').next().is('SPAN')) {
                        $('label[for=password]').next().remove();
                    }
                if ($('[name=password]').val() == $('[name="confirm-password"]').val()) {
                    $('label[for=password]').after('<span class="text-success">รหัสผ่านนี้สามารถใช้งานได้</span>')
                } else {
                    $('label[for=password]').after('<span class="text-danger">รหัสผ่านไม่ตรงกัน</span>')
                }
            }
        });
        // $('form').change(function () {
        //     if($('form').find('span.text-success').length == 4){
        //         formsubmit_lock(true);
        //     }
        //     });
        });
    </script>
@else
    <script>
        $(document).ready(function () {
            dateElem($('[name=bithdate]'));
            $('[type="password"]').change(function () {
                if ($('[name=password]').val() !== ""&&$('[name="confirm-password"]').val() !== "") {
                        if ($('label[for=password]').next().is('SPAN')) {
                            $('label[for=password]').next().remove();
                        }
                    if ($('[name=password]').val() == $('[name="confirm-password"]').val()) {
                        $('label[for=password]').after('<span class="text-success">รหัสผ่านนี้สามารถใช้งานได้</span>')
                    } else {
                        $('label[for=password]').after('<span class="text-danger">รหัสผ่านไม่ตรงกัน</span>')
                    }
                }
            });
        });
    </script>
@endif
<script>
    function validate_employee () {
        var status = [];
        if ($('[name=firstname]').val() == "") {
            status.push("firstname");
        }
        if ($('[name=lastname]').val() == "") {
            status.push("lastname");
        }

        if (status.length > 0) {
            return status;
        } else {
            return true;
        }
    }
    findAddress($('[name=address]'), '{{ $findAddress }}');
    function ajax_employee (data) {
        send(data, function (response) {
            if (response.emp) {
                alertify.notify(response.msg, 'success', 3, function(){
                    window.location.replace(response.redirect_to);
                 });
            } else {
                alertify.notify(response.msg, 'error', 3);
            }
        });
    }
</script>
@endsection
