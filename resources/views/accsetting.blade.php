@extends('layouts.workpace')

@section('content')
        <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    ตั้งค่าการรันเลขเอกสารอัตโนมัติ
                </button>
            </h2>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                comming soon.
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    ตั้งค่าผังบัญชี
                </button>
            </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                comming soon.
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                ยอดยกมา(สำหรับเริ่มต้น)
                </button>
            </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                <div class="product_to_stock">
                    <form action="{{ $stockAction }}" method="post">
                    <table class="table table-striped">
                        <thead class="table-warning text-center">
                            <tr>
                                <th>รหัสสินค้า/ชื่อสินค้า</th>
                                <th>จำนวนยกมา</th>
                                <th>ราคา</th>
                            </tr>
                        </thead>
                        <tbody class="table-hover">
                                @foreach ($product as $key => $item)
                                    <tr>
                                        <td>
                                            {{ $item['product_code'] . "::-" . $item['product_name'] }}
                                            <input type="hidden" name="product_id" value="{{ $item['id'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="qty[]" id="qty-{{ $item['id'] }}" class="form-control" step="0.001">
                                        </td>
                                        <td>
                                            <input type="number" name="unit_price[]" id="unit-price-{{ $item['id'] }}" class="form-control" step="0.001">
                                        </td>
                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                    <input type="submit" value="save" class="btn btn-primary btn-block">
                    </form>
                </div>
            </div>
            </div>
        </div>
        </div>
@endsection