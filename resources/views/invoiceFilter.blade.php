@extends('layouts.workpace')

@section('style')
    <style>
        @media print {
            @page {
                size: A4 landscape;
                font-size: 12px;
                font-family: sans-serif;
                color: black;
                background-color: slategrey;
            }
            header {
                display: none;
            }
            hr {
                display: none;
            }
            .head-no-print {
                display: none;
            }
        }
    </style>
@endsection
@section('content')
    <div class="head-no-print">
        <div class="pb-1">
            <h4>ฟิลเตอร์รายงาน</h4>
        </div>
        <div class="row pb-2">
            <div class="col">
                <input type="search" name="search" id="search" class="form-control" placeholder="ค้นหาจากรหัสสินค้า, ลูกค้า หรือผู้ขาย">
            </div>
        </div>
        <div class="row input-filter pb-2">
            <div class="col">
                <input type="text" name="filter_date_from" id="filter-date-from" placeholder="เริ่มวันที่" value="{{ $default[0] }}" class="form-control filter-date-from">
            </div>
            <div class="col">
                <input type="text" name="filter_date_to" id="filter-date-to" placeholder="ถึงวันที่" value="{{ $default[1] }}" class="form-control filter-date-to">
            </div>
            <div class="col">
                <input type="button" name="clear_filter" class="btn btn-block btn-info" value="clear filter">
            </div>
        </div>
        {{-- <hr> --}}
        <div class="row">
            <div class="col"></div>
            <div class="col-2">
                <input type="button" class="btn btn-block btn-warning text-white" onclick="print()" value="Export by print">
            </div>
            <div class="col-2">
                {{-- <input type="button" class="btn btn-block btn-warning text-white" value="Export to xslx"> --}}
                <a href="{{ route('invoiceExport', ['type' => 'invoice']) }}" id="buttExportxlsx" class="btn btn-block btn-warning text-white">Export to xslx</a>
            </div>
        </div>
        <hr>
    </div>
    <table class='table table-responsive-sm table-striped'>
        <thead class="table-info">
            <th>#</th>
            <th>วันที่</th>
            <th>รหัสใบขาย</th>
            <th>ลูกค้า</th>
            <th>ผู้ขาย</th>
            <th>ประเภทสินค้า</th>
            <th>จำนวน</th>
            <th>ราคาต่อหน่วย</th>
            <th>ก่อน vat</th>
            <th>vat</th>
            <th>รวมภาษีมูลค่าเพิ่ม</th>
        </thead>
        <tbody>
            @foreach ($data as $key => $dt)
                @foreach ($dt as $item)
                    <tr class="text-right parent-of-{{ explode(';', $key)['0'] }}">
                        <td class="text-center">{{ $loop->index + 1 }}</td>
                        <td data-date = "{{ date('d/m/Y', strtotime(explode(';', $key)['1'])) }}">{{ date('d/m/Y', strtotime(explode(';', $key)['1'])) }}</td>
                        <td>{{ explode(';', $key)['2'] }}</td>
                        <td>{{ explode(';', $key)['3'] }}</td>
                        <td>{{ explode(';', $key)['4'] }}</td>
                        <td>{{ $item['0'] }}</td>
                        <td>{{ number_format($item['2'], 2) }}</td>
                        <td>{{ number_format($item['3'], 2) }}</td>
                        <td>{{ number_format($item['4'], 2) }}</td>
                        <td>{{ number_format($item['5'], 2) }}</td>
                        <td>{{ number_format($item['6'], 2) }}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            dateElem($('[name="filter_date_from"]'));
            dateElem($('[name="filter_date_to"]'));
            $('[name="filter_date_from"], [name="filter_date_to"]').change(function () {
                cUrl = window.location.origin + window.location.pathname;
                strFilter = "?firstday=" + $('[name="filter_date_from"]').val().replaceAll('/', '-')
                + "&lastday="
                + $('[name="filter_date_to"]').val().replaceAll('/', '-');

                window.location.replace(cUrl + strFilter);
            });
            $('#buttExportxlsx').click(function (e) {
                e.preventDefault();
                from = $('[name=filter_date_from]').val();
                to = $('[name=filter_date_to]').val();
                href = $(this).attr('href');

                new_link = href + '?datefrom=' + from + '&dateto=' + to;
                $.ajax({
                    'url': new_link,
                    'method': 'GET'
                }).done((res) => {
                    if (res.error !== undefined) {
                        alertify.notify(res.msg, 'error', 5);
                    } else {
                        window.location.replace(new_link);
                    }
                });
            });
        });
    </script>
@endsection
