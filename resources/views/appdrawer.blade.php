@extends('layouts.app')

@section('style')
<style>
    div.icon {
        color: white;
    }
    div.icon:hover {
        background: rgba(255,255,255,0.5);
        color: black;
    }
    a{
        color: #ffffff
    }
    a:hover {
        color: #000;
        text-decoration: none;
    }
    h1 {
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Old versions of Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
        user-select: none; /* Non-prefixed version, currently
                                supported by Chrome, Edge, Opera and Firefox */
    }
    div.appTitle {
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Old versions of Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
        user-select: none; /* Non-prefixed version, currently
                                supported by Chrome, Edge, Opera and Firefox */
        max-height: 25px;
        overflow: hidden;
    }
    div.appdrawer {
        background: rgba(0, 0, 0, 0.5);
    }
</style>
@endsection
@section('content')
    <div class="container-fluid min-vh-100">
        <div class='appdrawer rounded pb-2'>
            <div class="col">
                <div class="text-center text-white mb-5 pt-4">
                    <h1>Application</h1>
                    <input type="search" class="form-control" name='appFIlter' placeholder="ค้นหาแอพพลิเคชั่น">
                </div>
                @foreach ($menu as $looptitle => $apps)
                    <div class="row">
                        <div class="col text-white mr-2 ml-2">
                            <h4>{{ $looptitle }}</h4>
                            <hr class="dropdown-divider">
                        </div>
                    </div>
                    <div class="row row-cols-5 g-4 mb-5 ml-4">
                        @foreach ($apps as $appname => $item)
                            <div class="col-sm-2 mr-3 icon rounded text-center" data-toggle="tooltip" data-placement="bottom" title="{{ $appname }}">
                                <a href="{{ route($item['route_name']) }}" class="link-info">
                                    <div class="appIcon">
                                        <h1><i class="{{ $item['icon'] }}"></i></h1>
                                    </div>
                                    <div class="appTitle">
                                        {{ $appname ?? '' }}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('input[type=search]').keyup(function () {
            var text = $(this).val();
            var ptrn = new RegExp(text, "i");

            $.each($('.icon'), function () {
                if ($(this).data('original-title').search(ptrn) >= 0) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    </script>
@endsection
