@extends('layouts.workpace')
@include('paths.pagination')

@section('content')
    <div class="row mb-2">
        <div class="col">
            <input type="search" name="customer_code" id="customer_code" class="form-control"
            placeholder="ค้นหารหัสลูกค้า รหัสประจำตัวผู้เสียภาษี ชื่อลูกค้า เบอร์โทร"
            value="{{ $default['customer_code'] }}"
            >
        </div>
        <div class="col">
            <input type="search" name="saletacker_id" id="saletacker_id" class="form-control"
            placeholder="ค้นหารหัสพนักงานขาย"
            value="{{ $default['saletacker_id'] }}"
            >
        </div>
        <div class="col">
            <button class="btn btn-primary btn-block" id="search">search <i class="fas fa-search"></i></button>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col">
            <select name="district" id="district" class="form-control" data-live-search="true">
                <option value="">--ตำบล--</option>
                @foreach ($district as $name => $id)
                @if (isset($default['district']))
                    <option value="{{ $name }}"
                    @if ($default['district'] == trim($name))
                        selected
                    @endif
                    >{{ $name }}</option>
                    @else
                        <option value="{{ $name }}">{{ $name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col">
            <select name="amphur" id="amphur" class="form-control" data-live-search="true">
                <option value="">--อำเภอ--</option>
                @foreach ($amphur as $name => $id)
                    @if (isset($default['amphur']))
                        <option value="{{ $name }}"
                        @if ($default['amphur'] == trim($name))
                        selected
                        @endif
                        >{{ $name }}</option>
                    @else
                        <option value="{{ $name }}">{{ $name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col">
            <select name="province" id="province" class="form-control" data-live-search="true">
                <option value="">--จังหวัด--</option>
                @foreach ($province as $name => $id)
                    {{-- @dump($default['province'] == $name) --}}
                    @if ($default['province'] !== null)
                        <option value="{{ $name }}"
                        @if ($default['province'] == trim($name))
                            selected
                        @endif
                        >{{ $name }}</option>
                    @else
                        <option value="{{ $name }}">{{ $name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col-2">
            <a href="{{ route('customerview') }}" class="btn btn-primary btn-block">clear filter</a>
        </div>
    </div>
    <a class="btn btn-block btn-warning" href="{{ $add }}"> + เพิ่มลูกค้า</a>
    <div class="row">
        <div class="col">
            <table class="table table-striped mt-2">
                <thead class="table-warning text-center">
                    <tr>
                        <th>แก้ไข</th>
                        <th>รหัสลูกค้า</th>
                        <th>พนักงานขายผู้รับผิดชอบ</th>
                        <th>รหัสประจำตัวผู้เสียภาษี</th>
                        <th>ชื่อลูกค้า</th>
                        <th>ที่อยู่</th>
                        <th>เบอร์โทรศัทพ์</th>
                        <th>ลบ</th>
                    </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < count($customers_list); $i++)
                        <tr>
                            <td><a href="{{ route('customeredit', ['id' => $customers_list[$i]->id]) }}" class="btn btn-block btn-warning"><i class="fas fa-edit"></i></a></td>
                            <td class="text-center">{{ $customers_list[$i]->customer_no }}</td>
                            <td class="text-right">{{ $customers_list[$i]->saletaker }}</td>
                            <td class="text-right">{{ $customers_list[$i]->customer_taxid }}</td>
                            <td class="text-center">{{ $customers_list[$i]->customer_name }}</td>
                            <td class="text-center">{{ $customers_list[$i]->customer_address_no . ' ' . $customers_list[$i]->customer_address }}</td>
                            <td class="text-center">{{ $customers_list[$i]->phone_no }}</td>
                            <td><a href="{{ route('customerdelete', ['id' => $customers_list[$i]->id]) }}" class="btn btn-block btn-danger delete"><i class="fas fa-trash-alt"></i></a></td>
                        </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#search').click(function () {
                var data = [];
                $('input[type=search],select').each(function () {
                    if($(this).val() !== "") {
                        data.push({'id': $(this).attr('name'), 'value': $(this).val()});
                    }
                });

                hostname = window.location.origin + window.location.pathname + '?';

                for (let i = 0;i < data.length;i++) {
                    if (data.length - 1 == i) {
                        hostname += data[i].id + '=' + data[i].value;
                    } else {
                        hostname += data[i].id + '=' + data[i].value + '&';
                    }
                }
                window.location.replace(hostname);
            });
        })
    </script>
@endsection
