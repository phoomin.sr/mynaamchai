@extends('layouts.workpace')

@section('content')
    <div class="content">
        <div class="filter">

        </div>
        <div class="row">
            <div class="col">
                @dump($list)
                <table class="table table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>สินค้าคงเหลือ</th>
                            <th>จำนวน</th>
                            <th>หน่วย</th>
                            <th>จุดสั่งซื้อ</th>
                            <th>ที่เก็บ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($list as $item)
                            <tr>
                                <td class="text-center">{{ $loop->index + 1 }}</td>
                                <td class="text-left">{{ $item->product_name }}</td>
                                <td class="text-right">{{ number_format($item->qty, 2) }}</td>
                                {{-- <td class="text-right">{{ $item->name_option_dt }}</td> --}}
                                <td></td>
                                <td class="text-right">{{ number_format($item->product_minstock, 2) }}</td>
                                <td class="text-right">{{ $item->product_location }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection
