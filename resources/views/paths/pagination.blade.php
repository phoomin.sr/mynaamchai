@section('pagination')
    <nav aria-label="Page navigation">
        <ul class="pagination pagination justify-content-center">
        <li @if ($pagination->backward == $pagination->page)
            class="page-item disabled"
        @else
            class="page-item"
        @endif>
            <a class="page-link" href="{{ Request::url() . '?page=' . $pagination->backward }}" tabindex="-1">Previous</a>
        </li>
        @foreach ($pagination->menu as $key => $item)
            @if ($key == 'lessthan')
                <li class="page-item disabled">
                    <a href="#" class="page-link">{{ $item }}</a>
                </li>
            @elseif ($key == 'menulink')
                @for ($i = 1; $i <= count($item); $i++)
                    @if ($i == $pagination->page)
                        <li class="page-item">
                            <div class="page-link">
                                <div class="bg-primary rounded-circle text-center" style="width: 20px">
                                    <span class="text-white">{{ $i }}</span>
                                </div>
                            </div>
                        </li>
                    @else
                        <li class="page-item">
                            <a href="{{ $item[$i - 1] }}" class="page-link">{{ $i }}</a>
                        </li>
                    @endif
                @endfor
            @elseif ($key == 'morethan')
                <li class="page-item disabled">
                    <a href="#" class="page-link">{{ $item }}</a>
                </li>
            @endif
        @endforeach
        <li @if ($pagination->forward == $pagination->page)
            class="page-item disabled"
        @else
            class="page-item"
        @endif>
            <a class="page-link" href="{{ Request::url() . '?page=' . $pagination->forward }}">Next</a>
        </li>
        </ul>
    </nav>
@endsection
