@extends('layouts.workpace')

@section('style')
    <style>
        @media print {
            @page {
                size: A4;
                font-size: 12px;
                font-family: sans-serif;
                color: black;
                background-color: slategrey;
            }
            header {
                display: none;
            }
            hr {
                display: none;
            }
            .head-no-print {
                display: none;
            }
        }
    </style>
@endsection

@section('content')
<div class="head-no-print">
    <div class="pb-1">
        <h4>ฟิลเตอร์รายงาน</h4>
    </div>
    <div class="row pb-2">
        <div class="col">
            <input type="search" name="customer_code" value="{{ $default['customer_code'] ?? '' }}" id="customer_code" class="form-control" placeholder="ค้นหาจากรหัสลูกค้า หรือรหัสประจำตัวผู้เสียภาษี หรือชื่อลูกค้า">
        </div>
        <div class="col">
            <input type="search" name="saletacker_id" id="saletacker_id" value="{{ $default['saletacker_id'] ?? '' }}" class="form-control" placeholder="ค้นหาจากรหัสพนักงานขาย">
        </div>
        <div class="col">
            <button class="btn btn-block btn-primary" onclick="search();"><i class="fas fa-search"></i> search</button>
        </div>
    </div>
    <div class="row pb-2">
        <div class="col">
            <select name="district" id="district" class="form-control" data-live-search="true">
                <option value="">--ตำบล--</option>
                @foreach ($district as $name => $id)
                @if (isset($default['district']))
                    <option value="{{ $name }}"
                    @if ($default['district'] == trim($name))
                        selected
                    @endif
                    >{{ $name }}</option>
                    @else
                        <option value="{{ $name }}">{{ $name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col">
            <select name="amphur" id="amphur" class="form-control" data-live-search="true">
                <option value="">--อำเภอ--</option>
                @foreach ($amphur as $name => $id)
                    @if (isset($default['amphur']))
                        <option value="{{ $name }}"
                        @if ($default['amphur'] == trim($name))
                        selected
                        @endif
                        >{{ $name }}</option>
                    @else
                        <option value="{{ $name }}">{{ $name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col">
            <select name="province" id="province" class="form-control" data-live-search="true">
                <option value="">--จังหวัด--</option>
                @foreach ($province as $name => $id)
                    {{-- @dump($default['province'] == $name) --}}
                    @if ($default['province'] !== null)
                        <option value="{{ $name }}"
                        @if ($default['province'] == trim($name))
                            selected
                        @endif
                        >{{ $name }}</option>
                    @else
                        <option value="{{ $name }}">{{ $name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col-2">
            <input type="button" name="clear_filter" class="btn btn-block btn-info" value="clear filter">
        </div>
    </div>
    {{-- <hr> --}}
    <div class="row">
        <div class="col"></div>
        <div class="col-2">
            <input type="button" class="btn btn-block btn-warning text-white" onclick="print()" value="Export by print">
        </div>
        <div class="col-2">
            {{-- <input type="button" class="btn btn-block btn-warning text-white" value="Export to xslx"> --}}
            <a href="{{ route('invoiceExport', ['type' => 'customer']) }}" id="buttExportxlsx" class="btn btn-block btn-warning text-white">Export to xslx</a>
        </div>
    </div>
    <hr>
</div>
<div class="show-table">
    <table class="table table-striped table-responsive-sm">
        <tr class="table-warning text-center">
            <th>รหัสลูกค้า</th>
            <th>รหัสประจำตัวผู้เสียภาษี</th>
            <th>ชื่อลูกค้า</th>
            <th>ที่อยู่</th>
            <th>พนักงานขาย</th>
        </tr>
        @for ($i = 0; $i < count($data); $i++)
            <tr>
                @for ($ii = 0; $ii < count($data[$i]); $ii++)
                    @if ($ii == count($data[$i]) - 1)
                        <td class="text-center">{{ $data[$i][$ii] }}</td>
                    @else
                        <td>{{ $data[$i][$ii] }}</td>
                    @endif
                @endfor
            </tr>
        @endfor
    </table>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            var str = [];
            $('#buttExportxlsx').click(function (e) {
                e.preventDefault();
                hostname = $(this).attr('href');
                $('input[type=search]').each(function () {
                    if ($(this).val() !== "") {
                        str.push($(this).attr('name') + '=' + $(this).val());
                    }
                });
                if (str.length > 0) {
                    hostname = hostname + '?';
                    for (let i = 0;i < str.length;i++) {
                        if (i !== str.length - 1) {
                            hostname += str[i] + '&';
                        } else {
                            hostname += str[i];
                        }
                    }
                }
                $.ajax({
                    'url': hostname,
                    'method': 'GET'
                }).done((res) => {
                    if (res.error !== undefined) {
                        alertify.notify(res.msg, 'error', 5);
                    } else {
                        window.location.replace(hostname);
                    }
                });
            });
            $('select').change(function () {
                str = [];
                $('select').each(function () {
                    if ($(this).val() !== "") {
                        str.push($(this).attr('id') + '=' + $(this).val());
                    }
                });
                if (str.length > 0) {
                    hostname = window.location.origin + window.location.pathname + '?';
                    for (let i = 0;i < str.length;i++) {
                        if (i !== str.length - 1) {
                            hostname += str[i] + '&';
                        } else {
                            hostname += str[i];
                        }
                    }
                    window.location.replace(hostname);
                }
            });
        });
        function search() {
            var str = [];
            var hostname = "";
            $('input[type=search]').each(function () {
                if ($(this).val() !== "") {
                    str.push($(this).attr('name') + '=' + $(this).val());
                }
                if (str.length > 0) {
                    hostname = window.location.origin + window.location.pathname + '?';
                    for (let i = 0;i < str.length;i++) {
                        if (i !== str.length - 1) {
                            hostname += str[i] + '&';
                        } else {
                            hostname += str[i];
                        }
                    }
                    window.location.replace(hostname);
                }
            });
        }
    </script>
@endsection
