@extends('layouts.workpace')

@section('content')
    <div class="container">
        <div class="fillter-panals mb-3">
            <input type="search" name="search" id="search" placeholder="ค้นหาสินค้า" class="form-control">
            <div class="row">
                <div class="col mt-2">
                    <select name="filter-product-group" id="filter-product-group" class="form-control" multiple>
                    </select>
                </div>
                <div class="col mt-2">
                    <select name="filter-product-unit" id="filter-product-unit" class="form-control" multiple>
                    </select>
                </div>
                <div class="col mt-2">
                    <select name="filter-stock" id="filter-stock" class="form-control" multiple></select>
                </div>
                <div class="col mt-2">
                    <button class="btn btn-block btn-secondary">clear filter</button>
                </div>
            </div>
            <div class="row">
                <div class="col mt-2">
                    <button class="btn btn-block btn-primary"><i class="fas fa-search"></i> search</button>
                </div>
            </div>
        </div>
        <div>
            <a href="{{ route('addProduct') }}" class="btn btn-warning btn-block">+ เพิ่ม</a>
        </div>
        <div class="row mt-3">
            <table class="table table-striped">
                <thead>
                    <tr class="text-center">
                        <th>แก้ไข</th>
                        <th>กลุ่ม</th>
                        <th>รหัสสินค้าคงเหลือ</th>
                        <th>ชื่อสินค้าคงเหลือ</th>
                        <th>ที่จัดเก็บ</th>
                        <th>จุดสั่งซื้อ</th>
                        <th>ลบ</th>
                        <th>อัพเดทล่าสุด</th>
                    </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < count($product); $i++)
                        <tr>
                            <td class="text-center">
                                <a class="btn btn-warning" href="{{ route("editProduct", ['id'=>$product[$i]->id]) }}"><i class="fas fa-edit"></i></a>
                            </td>
                            <td>{{ $product[$i]->name }}</td>
                            <td>{{ $product[$i]->product_code }}</td>
                            <td>{{ $product[$i]->product_name }}</td>
                            <td class="text-center">{{ $product[$i]->product_location }}</td>
                            <td class="text-center">{{ $product[$i]->product_minstock }}</td>
                            <td>
                                <a class="btn btn-danger delete" href="{{ route("deleteProduct", ['id'=>$product[$i]->id]) }}"><i class="fas fa-trash"></i></a>
                            </td>
                            <td>
                                {{ $product[$i]->updated_at }}
                            </td>
                        </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
@endsection
