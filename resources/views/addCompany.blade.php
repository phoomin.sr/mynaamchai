@extends('layouts.app')

@section('content')
    <div class='container-fluid'>
        <div class="container">
            <div class="border shadow bg-white p-4 rounded">
                <h3>เพิ่มกิจการและสมัครสมาชิก</h3>
            <hr>
            <form name="register_shotform" action="{{ route('savecompany') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col">
                        <label for="business_name" class="form-label">ชื่อกิจการ</label>
                        <input type="text" class="form-control" id="business_name" name="business_name" placeholder="ชื่อกิจการของคุณ">
                    </div>
                    <div class="col">
                        <label for="business_type" class="form-label">ประเภทกิจการ</label>
                        <select name="business_type" id="business_type" class="form-control">
                            @foreach ($bns as $key => $item)
                                <option value="{{ $key }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="wh_tax1" name="wh_tax" value="1">
                        <label class="form-check-label" for="wh_tax1">จดทะเบียนภาษีมูลค่าเพิ่มแล้ว</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="wh_tax2" name="wh_tax" value="0">
                        <label class="form-check-label" for="wh_tax2">ยังไม่จดทะเบียนภาษีมูลค้าเพิ่ม</label>
                      </div>
                </div>
                <div class="mb-3">
                    <label for="business_trade_no" class='form-label'>รหัสประจำตัวผู้เสียภาษี/ทะเบียนการค้า</label>
                    <input type="text" name="business_trade_no" value="" class="form-control"
                    placeholder="รหัสประจำตัวผู้เสียภาษี/ทะเบียนการค้า ไม่เกิน 15 ตัวอักษรและเป็นตัวเลขเท่านั้น" maxlength="13">
                </div>
                <hr>
                <h3>สร้างบัญชีผู้ใช้งาน</h3>
                <div class="mb-3">
                    <label for="email" class="form-label">อีเมล</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="username" class="form-label">ชื่อผู้ใช้</label>
                    <input type="text" name="username" id="username" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">รหัสผ่าน</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="confirm-password" class="form-label">ยืนยันรหัสผ่าน</label>
                    <input type="password" name="confirm-password" id="confirm-password" class="form-control">
                </div>
                <div class="d-flex justify-content-end">
                    <input type="hidden" name="grand" value="99">
                   <input type="submit" value="save" class="btn btn-outline-primary">
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        // เช็คชื่อกิจการ ajax
        $('[name=business_name]').change(function () {
            var data = {
                '_token': $('input[type=hidden][name="_token"]').val(),
                'table': 'company',
                'column': $(this).attr('name'),
                'value': $(this).val(),
                'expression': '='
            };
            $.ajax({
                'method': 'POST',
                'url': '{{ route('checkDuplicate') }}',
                'data': data
            }).done(function (response) {
                if (response.condition_status) {
                    if (!$('label[for="business_name"]').next().is("SPAN")) {
                        $('label[for="business_name"]').after("<span class='text-danger'> **มีชื่อนี้ในะระบบแล้ว</span>");
                    }
                } else {
                    if ($('label[for="business_name"]').next().is("SPAN")) {
                        $('label[for="business_name"]').next().remove();
                        $('label[for="business_name"]').after("<span class='text-success'> สามารถใช้ชื่อนี้ได้</span>");
                        $('input[name=business_name]').addClass("success");
                    } else {
                        $('label[for="business_name"]').after("<span class='text-success'> สามารถใช้ชื่อนี้ได้</span>");
                        $('input[name=business_name]').addClass("success");
                    }
                }
            });
        });
        // เช็ค email ajax
        $('[name=email]').change(function () {
            var data = {
                '_token': $('input[type=hidden][name="_token"]').val(),
                'table': 'users',
                'column': $(this).attr('name'),
                'value': $(this).val(),
                'expression': '='
            };
            $.ajax({
                'method': 'POST',
                'url': '{{ route('checkDuplicate') }}',
                'data': data
            }).done(function (response) {
                if (response.condition_status) {
                    if (!$('label[for="email"]').next().is("SPAN")) {
                        $('label[for="email"]').after("<span class='text-danger'> **มี email นี้ในะระบบแล้ว</span>");
                    }
                } else {
                    if ($('label[for="email"]').next().is("SPAN")) {
                        $('label[for="email"]').next().remove();
                        $('label[for="email"]').after("<span class='text-success'> สามารถใช้ email นี้ได้</span>");
                        $('input[type=email]').addClass("success");
                    } else {
                        $('label[for="email"]').after("<span class='text-success'> สามารถใช้ email นี้ได้</span>");
                        $('input[type=email]').addClass("success");
                    }
                }
            });
        });
        // เช็ค username ajax
        $('[name=username]').change(function () {
            var data = {
                '_token': $('input[type=hidden][name="_token"]').val(),
                'table': 'users',
                'column': 'name',
                'value': $(this).val(),
                'expression': '='
            };
            $.ajax({
                'method': 'POST',
                'url': '{{ route('checkDuplicate') }}',
                'data': data
            }).done(function (response) {
                if (response.condition_status) {
                    if (!$('label[for="username"]').next().is("SPAN")) {
                        $('label[for="username"]').after("<span class='text-danger'> **มีชื่อผู้ใช้นี้ในะระบบแล้ว</span>");
                    }
                } else {
                    if ($('label[for="username"]').next().is("SPAN")) {
                        $('label[for="username"]').next().remove();
                        $('label[for="username"]').after("<span class='text-success'> สามารถใช้ชื่อผู้ใช้นี้ได้</span>");
                        $('input[name=username]').addClass("success");
                    } else {
                        $('label[for="username"]').after("<span class='text-success'> สามารถใช้ชื่อนี้ได้</span>");
                        $('input[name=username]').addClass("success");
                    }
                }
            });
        });
        // เล็คพาสเวิร์ด ในฟอร์มยืนยัน
        $('[name=password], [name="confirm-password"]').change(function () {
            if ($('[name=password]').val() !== ""&&$('[name="confirm-password"]').val() !== "") {
                let password = $('[name=password]').val();
                let confirm_pass = $('[name="confirm-password"]').val();

                if (password !== confirm_pass) {
                    if (!$('label[for="confirm-password"]').next().is("SPAN")) {
                        $('label[for=confirm-password]').after('<span class="text-danger error-alert">ยืนยันรหัสไม่ถูกต้อง</span>');
                    }
                } else {
                    if ($('label[for=confirm-password]').next().is("SPAN")) {
                        $('span.error-alert').remove();
                        $('label[for=confirm-password]').after('<span class="text-success error-alert">ยืนยันรหัสถูกต้อง</span>');
                        $('input[name=password]').addClass("success");
                        $('input[name=confirm-password]').addClass("success");
                    } else {
                        $('label[for=confirm-password]').after('<span class="text-success error-alert">ยืนยันรหัสถูกต้อง</span>');
                        $('input[name=password]').addClass("success");
                        $('input[name=confirm-password]').addClass("success");
                    }
                }
            }
        });
        // replace trade_number
        $('[name=business_trade_no]').keyup(function () {
            let str = $(this).val();
            nstr = str.replace(/\D+/, '');
            $(this).val(nstr);
        });
    });
    function validate_register_shotform() {
        if ($('input[name=business_trade_no]').val().length == 13) {
            $('input[name=business_trade_no]').addClass('success');
        }

        status1 = $('input[type=text], input[type=email], input[type=password]').length == $('input.success').length;
        status2 = $('input[name=wh_tax]:checked').length > 0;

        return status1 && status2;
    }
    function ajax_register_shotform (data) {
        send(data, function (res) {
            if (res.addCompany) {
                alertify.notify(res.msg, 'success', 5, function(){
                    window.location.replace(res.redirect_to);
                 });
            } else {
                alertify.notify(res.msg, 'error', 5);
            }
        });
        if($('input[type=radio]:checked').length == 0) {
            alert.push($('input[type=radio]').attr('name'));
        }
        if ($('input[type=email]').val() !== "") {
            check_duplicate_equar('users', 'email', $('input[type=email]').val(), function (response) {
                if (response == true) {
                    $('label[for="'+$('input[type=email]').attr('name')+'"]').after(" <span class='text-danger'>*อีเมลนี้ถูกใช้งานแล้ว</span>");
                    $('input[type=email]').addClass('border border-danger');
                } else {
                }
                return false;
            });
        } else {
            alert.push($('input[type=email]').attr('name'));
        }
        if ($('#password').val() == $('#confirm-password').val()) {
            $('label[for="'+$('input[type=email]').attr('name')+'"]').after(" <span class='text-danger'>*อีเมลนี้ถูกใช้งานแล้ว</span>");
            $('input[type=email]').addClass('border border-danger');
        }

        if (alert.length > 0) {
            return alert;
        } else {
            return false;
            // return true;
        }
    }
    function ajax_register_shotform (data) {
        var status = validate_register_shotform();
        if (status == true) {
            send(data, function (res) {
                if (res.addCompany) {
                    alertify.notify(res.msg, 'success', 5, function(){
                        window.location.replace(res.redirect_to);
                    });
                } else {
                    alertify.notify(res.msg, 'error', 5);
                }
            });
        } else {
            form_require_alert(status);
        }
    }
</script>
@endsection
