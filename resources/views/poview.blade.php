@extends('layouts.workpace')

@section('content')
    <div class='table-filter'>
        <a href="{{ route('poadd') }}" class="btn btn-warning btn-block">+ เพิ่มใบสั่งซื้อ</a>
    </div>
    <hr>
    <div class="query-table">
        <table>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table>
    </div>

@endsection
