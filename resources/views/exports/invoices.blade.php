<table>
    <thead>
        <tr>
            <th>#</th>
            <th>วันที่</th>
            <th>รหัสใบขาย</th>
            <th>ลูกค้า</th>
            <th>ผู้ขาย</th>
            <th>ประเภทสินค้า</th>
            <th>จำนวน</th>
            <th>ราคาต่อหน่วย</th>
            <th>ก่อน vat</th>
            <th>vat</th>
            <th>รวมภาษีมูลค่าเพิ่ม</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $key => $dt)
            @foreach ($dt as $item)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ date('d/m/Y', strtotime(explode(';', $key)['1'])) }}</td>
                    <td>{{ explode(';', $key)['2'] }}</td>
                    <td>{{ explode(';', $key)['3'] }}</td>
                    <td>{{ explode(';', $key)['4'] }}</td>
                    <td>{{ $item['0'] }}</td>
                    <td>{{ number_format($item['2'], 2) }}</td>
                    <td>{{ number_format($item['3'], 2) }}</td>
                    <td>{{ number_format($item['4'], 2) }}</td>
                    <td>{{ number_format($item['5'], 2) }}</td>
                    <td>{{ number_format($item['6'], 2) }}</td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
