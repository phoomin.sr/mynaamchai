
    <table class="table table-striped table-responsive-sm">
        <tr class="table-warning text-center">
            <th>รหัสลูกค้า</th>
            <th>รหัสประจำตัวผู้เสียภาษี</th>
            <th>ชื่อลูกค้า</th>
            <th>ที่อยู่</th>
            <th>พนักงานขาย</th>
        </tr>
        @for ($i = 0; $i < count($data); $i++)
            <tr>
                @for ($ii = 0; $ii < count($data[$i]); $ii++)
                    @if ($ii == count($data[$i]) - 1)
                        <td class="text-center">{{ $data[$i][$ii] }}</td>
                    @else
                        <td>{{ $data[$i][$ii] }}</td>
                    @endif
                @endfor
            </tr>
        @endfor
    </table>
