@extends('layouts.workpace')

@section('content')
    <div class="container">
        <div class="w-100 mb-3">
            <div class="mb-2">
                <input type="search" name="search" id="search" class="form-control">
            </div>
            <div class="row">
                <div class="col-3">
                    <select name="filter-method-cal" id="filter-method-cal" class="form-control">
                        <option value="1">เข้าก่อน-ออกก่อน</option>
                        <option value="2">ถั้วเฉลี่ย</option>
                    </select>
                </div>
                <div class="col-3">
                    <a class="btn btn-outline-info" href="{{ route('group') }}">clear filter</a>
                </div>
            </div>
        </div>
        <div class="row">
            <a href="{{ route('addCatstock') }}" class="btn btn-warning btn-block">+ เพิ่ม</a>
        </div>
        <table class="table table-hover table-responsive">
            <thead class='text-center'>
                <tr>
                    <th style="width: 5%">แก้ไข</th>
                    <th style="width: 5%">รหัสกลุ่ม</th>
                    <th style="width: 25%">ชื่อ</th>
                    <th style="width: 15%">คำอธิบาย</th>
                    <th style="width: 5%">ลบ</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($category as $id => $items)
                    <tr>
                        <td class="text-center"><a href="{{ route('updateCatStock', ['id'=>$id]) }}" class="btn btn-warning" data-togle='tooltip' data-placement="bottom" data-original-title='แก้ไข'><i class="fas fa-edit"></i></a></td>
                        <td class="text-center">{{ $items['slug'] }}</td>
                        <td>{{ $items['name'] }}</td>
                        <td>{{ $items['description'] }}</td>
                        <td class="text-center"><button onclick="delrow('{{ route('deleteCatStock', ['id'=>$id]) }}')" class="btn btn-danger" data-togle='tooltip' data-placement="bottom" data-original-title='แก้ไข'><i class="fas fa-trash-alt"></i></button></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('script')
    <script>
        function delrow(url) {
            $.ajax({
                method: 'GET',
                url: url
            }).done(function (response) {
                if (response.delete) {
                alertify.notify(response.msg, 'success', 5, function(){
                    window.location.replace(response.redirect_to);
                 });
                } else {
                    alertify.notify(response.msg, 'error', 5);
                }
            });
        }
    </script>
@endsection
