@extends('layouts.workpace')

@section('content')
    <div class="container">
        <div class="filter mb-3">
            <div class="col">
                <input type="search" class="form-control" name="filter-supplier" id="filter-supplier" placeholder="ค้นหาคู่ค้า 3 ตัวอักษร ชื่อ, รหัส, เลขทะเบียนภาษีมูลค่าเพิ่ม">
            </div>
        </div>
        <div class="table mb-3">
            <div class="col mb-3">
                <a class="btn btn-block btn-warning" href="{{ route('addsupplier') }}">+ เพิ่ม</a>
            </div>
            <div class="col">
                <table class="table table-hover table-warning">
                    <thead class="text-center">
                        <tr>
                            <th>ลำดับ</th>
                            <th>ชื่อ</th>
                            <th>ประเภท</th>
                            <th>ที่อยู่</th>
                            <th>ผู้ติดต่อ</th>
                            <th>สถานะ</th>
                            <th>สร้างเมื่อ</th>
                            <th>อัพเดทล่าสุด</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
