@extends('layouts.workpace')

@section('content')
    <div class="container">
        <div class="row">
            <div class='col'>
                <a href="{{ route('addsale') }}" class="btn btn-warning btn-block">+ เพิ่ม</a>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col">
                <table class="table table-responsive table-striped">
                    <thead class="table-warning text-center">
                        <tr>
                            <th width='5%'>แก้ไข</th>
                            <th width='5%'>พิมพ์</th>
                            <th width='10%'>รหัสใบขาย</th>
                            <th width='30%'>ลูกค้า</th>
                            <th width='10%'>วันที่ส่ง</th>
                            <th width='10%'>ประเภทภาษี</th>
                            <th width='10%'>ราคารวม</th>
                            <th width='10%'>วันที่สร้าง</th>
                            <th width='5%'>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($view as $item)
                            <tr>
                                <td width='5%'>
                                    <a class="btn btn-block btn-warning" href="{{ route('editsale', ['id' => $item->id]) }}"><i class="fas fa-edit"></i></a>
                                </td>
                                <td width='5%'>
                                    <a class="btn btn-block btn-warning" href="{{ route('printsale', ['id' => $item->id]) }}"><i class="fas fa-print"></i></a>
                                </td>
                                <td width='10%'>{{ $item->docno }}</td>
                                <td width='30%'>{{ $item->customer_name }}</td>
                                <td width='10%'>{{ date_format(date_create($item->duedate), 'd-M-Y') }}</td>
                                <td width='20%'>{{ $vattype[$item->vattype] }}</td>
                                <td width='10%'>{{ number_format($item->novat, 2) }}</td>
                                <td width='10%'>{{ date_format(date_create($item->docdate), 'd-M-Y') }}</td>
                                <td width='5%'>
                                    <a href="{{ route('deletesale', ['id' => $item->id]) }}" class="btn btn-block btn-danger delete"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
