@extends('layouts.workpace')

@section('content')
    <div class="filter-panels mb-3">

    </div>
    <div class="mb-3">
        <a href="{{ route('formWidthdrawmn') }}" class="btn btn-block btn-warning">สร้างใบเบิก</a>
    </div>
<hr>
    <div class="mt-3">
        <table class="table table-responsive-sm table-striped">
            <tr class="table-warning text-center">
                <th>รหัสใบเบิก</th>
                <th>ประเภทการเบิก</th>
                <th>รายการเบิก</th>
                <th>วันที่เบิก</th>
            </tr>
            @for ($i = 0; $i < count($record); $i++)
                <tr class="text-center">
                    <td>{{ $record[$i]->doc_no }}</td>
                    <td>{{ $withdraw_for[$record[$i]->type] }}</td>
                    <td class="text-left">{{ $record[$i]->programs }}</td>
                    <td>{{ $record[$i]->doc_date }}</td>
                </tr>
            @endfor
        </table>
    </div>

@endsection
