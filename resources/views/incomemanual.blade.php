@extends('layouts.workpace')

@section('content')
    <div class="container">
        <div class="filter">
           <input type="search" name="s-name" id="s-name" class="form-control">
        </div>
        <div class="table-show-all">
            <a href="{{ $add }}" class="btn btn-block btn-warning mt-3">+ เพิ่ม</a>
            <table class="table table-hover table-striped mt-3">
                <thead class="text-center">
                    <tr>
                        <th>ลำดับ</th>
                        <th>รหัสใบรับ</th>
                        <th>หมายเหตุ</th>
                        <th>วันที่รับ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recs ?? [] as $rec)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $rec->no }}</td>
                            <td>{{ $rec->product_name . ' จำนวน ' . number_format($rec->unit_qty) .'-' }}</td>
                            <td>{{ date_create($rec->docdate)->format('d M Y') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
