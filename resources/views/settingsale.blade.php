@extends('layouts.workpace')

@section('content')
    <div class="container">
        <h3>ตัวช่วยสร้างรหัส</h3>
        <p class="text-muted">
            กำหนดรูปแบบการรันรหัสสำหรับใบขาย
        </p>
        <table class="table table-hover table-response table-striped table-sm">
            <thead class="table-warning text-center">
                <tr>
                    <th>ลำดับ</th>
                    <th>ประเภทเอกสาร</th>
                    <th>คำนำหน้า</th>
                    <th>รูปแบบรหัส</th>
                    <th>จำนวนหลักที่ต้องการ</th>
                    <th>วันที่</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>#</td>
                    <td>
                        <select name="docname" id="docname" class="form-control">
                            @foreach ($doctype as $item => $name)
                                <option value="{{ $item }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="text" name="prefix" id="prefix" class="form-control">
                    </td>
                    <td>
                        <select name="option_doc" id="option_doc" class="form-control" multiple>
                            @foreach ($dateop as $item => $name)
                                <option value="{{ $item }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="number" name="digit" id="digit" class="form-control">
                    </td>
                    <td>
                        <input type="button" value="สร้าง" class="btn btn-primary btn-sm btn-block">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection