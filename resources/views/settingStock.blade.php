@extends('layouts.workpace')

@section('content')
    <div class="container">
        <form action="{{ route('stocksavesetting') }}" name="settingStock" method="post">
            @csrf
            <h4>วิธีการรันเอกสาร</h4>
            <hr>
            <div class="row mb-3">
                <div class="col-6">
                    <p>เจ้าหนี้การค้า</p>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <label for="prefix_trade" class="form-label">คำนำหน้า</label>
                            <input type="text" name="prefix_trade" id="prefix_trade" value="{{ $docname['stock_trade']['prefix'] ?? "" }}" class="form-control">
                            <span class="text-muted" style="font-size: 9pt">**คำนำหน้ารหัสใบรับ เช่น RR เป็นต้น ถ้าไม่มีสามารถเว้นว่างไว้</span>
                        </div>
                        <div class="col">
                            <label for="runno_pattrn_trade">วิธีรัน</label>
                            <select name="runno_pattrn_trade[]" class="form-control selectpicker" multiple>
                                <option value="0">--เลือก--</option>
                                @foreach ($runno as $key => $value)
                                    @if (isset($docname['stock_trade']['runno']))
                                        @if (in_array($key, $docname['stock_trade']['runno']))
                                            <option value="{{ $key }}" selected>{{ $value }}</option>
                                        @endif
                                    @endif
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="trade_no_number">จำนวน digit</label>
                            <input type="text" name="trade_no_number" id="trade_no_number" class="form-control" value="{{ $docname['stock_trade']['digit'] ?? "" }}">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <p>เจ้าหนี้อื่น</p>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <label for="prefix_other" class="form-label">คำนำหน้า</label>
                            <input type="text" name="prefix_other" id="prefix_other" class="form-control" value="{{ $docname['stock_other']['prefix'] ?? "" }}">
                            <span class="text-muted" style="font-size: 9pt">**คำนำหน้ารหัสใบรับ เช่น RR เป็นต้น ถ้าไม่มีสามารถเว้นว่างไว้</span>
                        </div>
                        <div class="col">
                            <label for="runno_pattrn_other">วิธีรัน</label>
                            <select name="runno_pattrn_other[]" class="form-control selectpicker" multiple>
                                <option value="0">--เลือก--</option>
                                @foreach ($runno as $key => $value)
                                    @if (isset($docname['stock_other']['runno']))
                                        @if (in_array($key, $docname['stock_other']['runno']))
                                            <option value="{{ $key }}" selected>{{ $value }}</option>
                                        @endif
                                    @endif
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="other_no_number">จำนวน digit</label>
                            <input type="text" name="other_no_number" id="other_no_number" class="form-control" value="{{ $docname['stock_other']['digit'] ?? "" }}">
                        </div>
                    </div>
                </div>
            </div>
            <h4 class="mt-4">เพิ่มคลังสินค้า</h4>
            <hr>
            <div class="row mb-3">
                <div class="col">
                    <label for="stock_name" class="form-label">คลังสินค้าคงเหลือ</label>
                    @if (count($stock_name) > 0)
                        <table class="table table-responsive-sm table-striped">
                            <tr class="table-success text-center">
                                <th>ชื่อคลัง</th>
                                <th scope="col">ลบคลัง</th>
                            </tr>
                            @for ($i = 0; $i < max(count($stock_name),1); $i++)
                                <tr>
                                    <td>
                                        {{ $stock_name[$i]->name }}
                                    </td>
                                    <td>
                                        <a class="btn btn-block btn-danger" href="{{ route('deleteEmployee', ['id' => $stock_name[$i]->id]) }}">ลบ</a>
                                    </td>
                                </tr>
                            @endfor
                        </table>
                    @endif
                        <input type='text' name='stock_name[]' class="form-control" value="">
                    <button class="btn btn-block btn-warning mt-2" id='addRow'>+ แถว</button>
                </div>
            </div>
            <input type="submit" value="บันทึก" class="btn btn-block btn-primary">
        </form>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#addRow').click(function (e) {
                e.preventDefault();
                addRow();
            });
        });
        function addRow() {
            let clone = $('[name="stock_name[]"]').first().clone();
            clone.find('input').val('');

            $('[name="stock_name[]"]').last().after(clone);
        }
        function validate_settingStock(){
            return true
        }
        function ajax_settingStock(data){
            send(data, function (response) {
                if (response.update) {
                    alertify.notify(response.msg, 'success', 5);
                } else {
                    alertify.notify(response.msg, 'error', 5);
                }
            });
        }
    </script>
@endsection
