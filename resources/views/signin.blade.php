@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row row-cols-1 row-cols-md-3 g-3">
    @if ($company !== '')
        @for ($i = 0; $i < count($company); $i++)
            <div class="col">
                <div class="card bg-white shadow text-dark mx-auto d-flex justify-content-center" style="min-height: 24rem">
                    <div class="card-img text-center pt-3 pb-3 bg-light">
                            <h3 class="mt-2">{{ $company[$i]->business_name }}</h3>
                            <h6 class="card-subtitle mb-2 text-muted">sign in to {{ $company[$i]->business_name }}</h6>
                    </div>
                    <div class="card-body">
                        <div class="card-title text-center">
                            <form name="login" action="{{ route('login') }}" method="POST" class="mt-4">
                                @csrf
                                <input type="hidden" name="company_id" value="{{ $company[$i]->id }}">
                                <div class="mb-3">
                                    <label for="username" class="form-label">username</label>
                                    <input type="text" name="username" id="username" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label">password</label>
                                    <input type="password" name="password" id="password" class="form-control">
                                </div>
                                <div class="mb-2 mt-3">
                                    <input type="submit" value="Sign In" class="btn btn-outline-primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    @endif
        <div class="col">
            <a href='{{ url('shotform-company') }}'>
                <div class="card bg-white shadow text-dark mx-auto d-flex justify-content-center" style="min-height: 24rem">
                    <div class="img-card mx-auto p-5 rounded-circle" style="width: 8rem;height: 8rem;border-style: dashed;">
                        <i class="fas fa-plus h2"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function validate_login () {
        return true;
    }
    function ajax_login (data) {
        send(data, function (res) {
            if (res.login) {
                alertify.notify(res.msg, 'success', 2, function(){
                    window.location.replace(res.redirect_to);
                 });
            } else {
                alertify.notify(res.msg, 'error', 2);
            }
        });
    }
</script>
@endsection
