@extends('layouts.workpace')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <a class="btn btn-warning btn-block" href="{{ route('addoption') }}">+ เพิ่ม</a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-striped mt-5">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>แก้ไข</th>
                            <th>ชื่อตัวเลือก</th>
                            <th>ตัวเลือก</th>
                            <th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i < count($option); $i++)
                            <tr>
                                <td class="text-center">{{ $i + 1 }}</td>
                                <td class="text-center">
                                    <a class="btn btn-warning" href="{{ route('updateformoption', ['id'=>$option[$i]['option_id']]) }}"><i class="fas fa-edit"></i></a>
                                </td>
                                <td>{{ $option[$i]['option_name'] }}</td>
                                <td>
                                    <ul>
                                        @foreach ($option[$i]['option_inner'] as $item)
                                            <li>{{ $item }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-danger delete" href="{{ route('deleteoption', ['id'=>$option[$i]['option_id']]) }}"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
